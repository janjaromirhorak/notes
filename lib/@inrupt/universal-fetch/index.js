// The original @inrupt/universal-fetch does have some issues when bundled with Vite
// Namely, the environment detection is wrong, and for this reason the app did not work in production mode.
// Since this app is always run only in the browser environment, there is no need for server-side fetch, and the package
// can be locally resolved by just reexporting global defaults.
export default globalThis.fetch;
const fetchProxy = (...params) => fetch(...params);
export const { Response, Request, Headers } = globalThis;
export { fetchProxy as fetch };
