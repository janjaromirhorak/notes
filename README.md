# Notes

A note taking application built with [Solid Project](https://solidproject.org/) that runs in your browser.

The application is written using [React](https://reactjs.org/),
and it is based on the [Create React App](https://create-react-app.dev/) framework.

## The live application and documentation

You can try the application for yourself by visiting [https://notes.jenda.dev](https://notes.jenda.dev).

The developer documentation is hosted [here](https://notes.jenda.dev/docs).

## Local development

To run the application locally, the [Yarn](https://yarnpkg.com/) package management tool is required.
To build the documentation, [Parcel](https://parceljs.org/) is required as well.
Node `^18.0.0` is required to build the package.

### Installing dependencies

To install the dependencies, run the following command in the project directory:

```bash
yarn install
```

### Starting the development server

Since the application is built with Create React App, the application contains
a development environment with a development server, supporting linting and hot reloads.

To start the development server, run the following command:

```bash
yarn start
```

### Building the application and the documentation

To build the application, run:

```bash
yarn build
```

To build the documentation, run:

```bash
yarn docs
```

## Testing

The app has e2e tests implemented in [Playwright](https://playwright.dev/).

Before running the tests, you will have to prepare a Solid pod with correct information and then set up the user information for the test runner.

### Prepare the Solid Pod

The tests expect a Solid Pod with the followin setup:

- The main user collection contains at least these three collection:
  - `public`
  - `private`
  - `profile`
- The `public` collection must be readable by unauthenticated users and the test user must have full access to it (`read`, `write`, `append`, `control`).
- The `public` collection must contain a collection named `e2e-testing-directory`, that contains a note named `public-testing-note.ttl`.
- The `public-testing-note.ttl` note must contain the string "Testing note" in its title.

### Create `.env.test`

After setting up the Solid Pod of the test user, create an `.env.test` file containing information needed by the tests:

```bash
# Base URL of the app - this is the default for `yarn build && yarn serve`
PLAYWRIGHT_APP_URL="http://localhost:4173"
# WebID of the testing user
PLAYWRIGHT_TESTING_USER="https://.../profile/card#me"
# Username of the testing user
PLAYWRIGHT_TESTING_USER_USERNAME="..."
# Password of the testing user
PLAYWRIGHT_TESTING_USER_PASSWORD="..."
# Route to the public directory of the testing user
PLAYWRIGHT_PUBLIC_DIRECTORY="https://.../public/"
```

### Run the tests

You can run the test suite using the following command:

```bash
yarn test

# to run the tests in a "headed" mode (visible browser)
yarn test --headed

# to run the tests in a "debug" mode (visible browser with a debugger)
yarn test --debug
```

When running the tests for the first time, you may by prompted by Playwright to install the browser binaries. In that case just follow the directions.

## Deploying to production

### Using GitLab pipelines

To build and deploy the application using GitLab, create a fork of
the application's repository and configure your web server to allow FTP uploads.
Then configure the following CI/CD variables in GitLab:

- `DEPLOY_FTP_HOST` - host for the FTP connection
- `DEPLOY_FTP_USER` - username for the FTP connection
- `DEPLOY_FTP_PASSWORD` - password for the FTP connection

Then rename the `.gitlab-ci.yml.disabled` file to `.gitlab-ci.yml`.

After that, every push to the GitLab repository will be automatically built,
and every new or modified tag on the `master` branch will be deployed
to the web server using the provided FTP credentials.

The documentation will be deployed to the `/docs` route.

### Deploy to Vercel

[Vercel](https://vercel.com) is a hosting platform, that allows simple deployments of frontend web applications and simple Node.js based backends directly from their Git repository. Since the application developed in this thesis runs entirely in the browser, and is built using a standardized build process and dependency management, it can be easily deployed to Vercel.

To deploy the application to Vercel, connect your fork of this repository to your Vercel account and choose `Vite` as the framework and `yarn build` as the build command.
In the current state of Vercel and the application there is no need to change any other settings, as the defaults are sufficient for this application.

After that, each push to the repository will automatically create a deployment, accessible via a unique URL. The head of the `main` branch will be deployed to the main application domain configured in the Vercel project settings.

To tweak redirects and other app-level configuration, you can modify the `vercel.json` file.
