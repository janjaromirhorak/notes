import React, { KeyboardEventHandler, useCallback, useEffect } from "react";
import ContentSpinner from "../../components/ContentSpinner";
import FileBrowser from "../../components/FileBrowser";
import { useWebId } from "../../components/SessionProvider";
import { useSafeState } from "../../hooks/useSafeState";
import { getPodUriFromWebId } from "../../services";

import { useTranslation } from "react-i18next";
import Button from "../../components/Button";
import PageHeading from "../../components/PageHeading";
import useSetCustomTitle from "../../components/SessionProvider/useSetCustomTitle";
import TextInput from "../../components/TextInput";

/**
 * A container for the note browser page
 *
 * @category Containers
 */
const OpenNote: React.FC<{}> = () => {
  const { t } = useTranslation();
  const webId = useWebId();
  const [podUri, setPodUri] = useSafeState<string>(
    getPodUriFromWebId(webId) ?? ""
  );
  const [podUriInputValue, setPodUriInputValue] = useSafeState<string>("");
  const setCustomTitle = useSetCustomTitle();

  useEffect(() => {
    setCustomTitle(t("fileBrowser.pageTitle"));
  }, [t, setCustomTitle]);

  const onKeyUp: KeyboardEventHandler = useCallback(
    (e) => {
      if (e.key === "Enter") {
        setPodUri(podUriInputValue);
      }
    },
    [podUriInputValue, setPodUri]
  );

  if (!webId && !podUri) {
    return (
      <div className="mx-auto max-w-screen-md">
        <PageHeading>{t("fileBrowser.notLoggedIn.title")}</PageHeading>

        <p className="mb-6">{t("fileBrowser.notLoggedIn.text1")}</p>

        <div className="flex gap-4">
          <TextInput
            value={podUriInputValue}
            onChange={(e) => setPodUriInputValue(e.currentTarget.value)}
            placeholder={"https://example.com/"}
            onKeyUp={onKeyUp}
          />
          <Button onClick={() => setPodUri(podUriInputValue)}>
            {t("fileBrowser.notLoggedIn.browse")}
          </Button>
        </div>
      </div>
    );
  }

  if (webId && !podUri) {
    return <ContentSpinner />;
  }

  return <FileBrowser defaultUri={podUri} />;
};

export default OpenNote;
