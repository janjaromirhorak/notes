import useCustomTitle from "../../components/SessionProvider/useCustomTitle";

const ResponsivePageTitle = () => {
  const customTitle = useCustomTitle();
  return <>{customTitle || import.meta.env.VITE_TITLE_BASE}</>;
};

export default ResponsivePageTitle;
