import React, { useEffect } from "react";

import { useTranslation } from "react-i18next";
import LoginForm from "../../components/LoginForm";
import useSetCustomTitle from "../../components/SessionProvider/useSetCustomTitle";

/**
 * A container for the login page
 *
 * @category Containers
 */
const Login: React.FC<{}> = () => {
  const setCustomTitle = useSetCustomTitle();
  const { t } = useTranslation();
  useEffect(() => {
    setCustomTitle(t("loginForm.title"));
  }, [t, setCustomTitle]);

  return (
    <div className="mx-auto max-w-screen-md">
      <LoginForm />
    </div>
  );
};

export default Login;
