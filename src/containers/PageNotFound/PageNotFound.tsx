import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { ButtonLink } from "../../components/Button";
import PageHeading from "../../components/PageHeading";
import useSetCustomTitle from "../../components/SessionProvider/useSetCustomTitle";

/**
 * A React component page that is displayed when there's no valid route. Users can click the button
 * to get back to the home/welcome page.
 *
 * @category Containers
 */
const PageNotFound: React.FC<{}> = () => {
  const { t } = useTranslation();
  const setCustomTitle = useSetCustomTitle();
  useEffect(() => {
    setCustomTitle(t("pageNotFound.title"));
  }, [t, setCustomTitle]);

  return (
    <div className="mx-auto max-w-screen-md">
      <PageHeading>{t("pageNotFound.title")}</PageHeading>

      <p className="mb-8">{t("pageNotFound.text1")}</p>

      <div className="flex items-center justify-center gap-4">
        <ButtonLink to="/"> {t("pageNotFound.back")}</ButtonLink>
        <ButtonLink
          href="https://gitlab.com/janjaromirhorak/notes/-/issues"
          isExternal
          theme="secondary"
        >
          {t("pageNotFound.issue")}
        </ButtonLink>
      </div>
    </div>
  );
};

export default PageNotFound;
