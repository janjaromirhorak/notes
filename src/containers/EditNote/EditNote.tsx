import React, { useEffect } from "react";
import { useSafeState } from "../../hooks/useSafeState";

import { RemoteNote, uidToUri } from "../../services";

import { useNavigate, useParams } from "react-router-dom";
import ContentSpinner from "../../components/ContentSpinner";
import NoteEditor from "../../components/NoteEditor";
import NoteLoadingError from "../../components/NoteEditor/components/NoteLoadingError";
import { useIsLoggedIn } from "../../components/SessionProvider";
import useRemoteResourceFactory from "../../components/SessionProvider/useRemoteResourceFactory";

/** Props for the EditNote component */
type EditNoteProps = {
  /** Route match from react-router */
  match?: { params: { uid: string } };
};

/**
 * A container for the note editor
 *
 * @category Containers
 */
const EditNote: React.FC<EditNoteProps> = () => {
  const { uid } = useParams();
  const uri = uid ? uidToUri(uid) : "";
  const factory = useRemoteResourceFactory();

  const [note, setNote] = useSafeState<RemoteNote | undefined>(undefined);
  const [loadingError, setLoadingError] = useSafeState<boolean>(false);
  const [isLoading, setIsLoading] = useSafeState<boolean>(true);
  const isLoggedIn = useIsLoggedIn();
  const navigate = useNavigate();

  useEffect(() => {
    if (uri) {
      try {
        setIsLoading(true);
        const note = factory.getNote(uri);
        note
          .isNote()
          .then((isNote) => {
            if (isNote) {
              setNote(note);
            } else {
              navigate("/");
            }
          })
          .finally(() => {
            setIsLoading(false);
          });
      } catch (e) {
        setLoadingError(true);
      }
    }
  }, [isLoggedIn, uri, navigate, setIsLoading, setNote, setLoadingError]);

  if (loadingError) {
    return <NoteLoadingError noteUri={uri} />;
  }

  if (isLoading) {
    return <ContentSpinner />;
  }

  if (!note) {
    return <NoteLoadingError noteUri={uri} />;
  }

  return <NoteEditor note={note} />;
};

export default EditNote;
