import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import HomePageContent from "../../components/HomePage";
import useSetCustomTitle from "../../components/SessionProvider/useSetCustomTitle";

/**
 * A container for the home page
 *
 * @category Containerst("loginForm.title")
 */
const HomePage: React.FC<{}> = () => {
  const setCustomTitle = useSetCustomTitle();
  const { t } = useTranslation();
  useEffect(() => {
    setCustomTitle();
  }, [t, setCustomTitle]);

  return (
    <div className="mx-auto max-w-screen-sm">
      <HomePageContent />
    </div>
  );
};

export default HomePage;
