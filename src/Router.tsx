import React from "react";
import { RouterProvider, createBrowserRouter } from "react-router-dom";

import {
  EditNote,
  HomePage,
  Login,
  OpenNote,
  PageNotFound,
} from "./containers";
import Layout from "./containers/Layout";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      { index: true, Component: HomePage },
      { path: "open", Component: OpenNote },
      { path: "login", Component: Login },
      { path: "note/:uid", Component: EditNote },
      { path: "404", Component: PageNotFound },
    ],
  },
]);

const Router: React.FC<{}> = () => <RouterProvider router={router} />;

export default Router;
