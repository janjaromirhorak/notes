import React, { Suspense, useEffect } from "react";
import Router from "./Router";
import ToastProvider from "./components/Toast/ToastProvider";
import { Loader } from "./components/Utils";

const App: React.FC<{}> = () => {
  useEffect(() => {
    console.log(`Notes v${import.meta.env.VITE_VERSION ?? "???"}`);
  });

  return (
    <Suspense fallback={<Loader />}>
      <ToastProvider>
        <Router />
      </ToastProvider>
    </Suspense>
  );
};

export default App;
