const formatDateTime = (date: Date): string => {
  let formatterConfig: Intl.DateTimeFormatOptions;
  if (new Date().toDateString() === date.toDateString()) {
    formatterConfig = {
      dateStyle: undefined,
      timeStyle: "short",
    };
  } else {
    formatterConfig = {
      dateStyle: "short",
      timeStyle: "short",
    };
  }

  const formatter = new Intl.DateTimeFormat(undefined, formatterConfig);
  return formatter.format(date);
};

export default formatDateTime;
