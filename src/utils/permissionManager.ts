import {
  Access,
  createAcl,
  getAgentAccessAll,
  getPublicAccess,
  getResourceAcl,
  getResourceInfoWithAcl,
  getSolidDatasetWithAcl,
  hasAccessibleAcl,
  hasResourceAcl,
  saveAclFor,
  setAgentDefaultAccess,
  setAgentResourceAccess,
  setPublicDefaultAccess,
  setPublicResourceAccess,
} from "@inrupt/solid-client";
import * as authClient from "@inrupt/solid-client-authn-browser";
import {
  PermissionContextItem,
  noAccess,
} from "../components/NoteEditor/NotePermissionsContext";
import type { RemoteResource } from "../services";

class PermissionManager {
  async getPermissionsInMaps(
    resource: RemoteResource
  ): Promise<PermissionContextItem[]> {
    const resourceInfo = await getResourceInfoWithAcl(resource.getUri(), {
      fetch: authClient.fetch.bind(authClient),
    });
    const agentAccess = getAgentAccessAll(resourceInfo) ?? {};

    let returnValue: PermissionContextItem[] = [];
    for (const agentWebId of Object.keys(agentAccess)) {
      returnValue.push({
        webId: agentWebId,
        permissions: agentAccess[agentWebId] ?? noAccess,
      });
    }

    returnValue.push({
      webId: undefined,
      permissions: getPublicAccess(resourceInfo) ?? noAccess,
    });

    return returnValue;
  }

  async getSolidDataset(resourceUrl: string): Promise<any> {
    return await getSolidDatasetWithAcl(resourceUrl, {
      fetch: authClient.fetch.bind(authClient),
    });
  }

  async getOrCreateResourceAcl(dataset: any): Promise<any> {
    // Obtain the SolidDataset's own ACL, if available,
    // or initialise a new one, if possible:
    if (!hasResourceAcl(dataset) && !hasAccessibleAcl(dataset)) {
      return;
    }

    return hasResourceAcl(dataset)
      ? getResourceAcl(dataset)
      : createAcl(dataset);
  }

  updateResourceAclAccess(
    resourceAcl: any,
    permissionMap: Access,
    webId?: string
  ): any {
    // Give someone Control access to the given Resource:
    if (webId === undefined || webId === "public") {
      resourceAcl = setPublicDefaultAccess(resourceAcl, permissionMap);
      resourceAcl = setPublicResourceAccess(resourceAcl, permissionMap);
    } else {
      resourceAcl = setAgentDefaultAccess(resourceAcl, webId, permissionMap);
      resourceAcl = setAgentResourceAccess(resourceAcl, webId, permissionMap);
    }

    return resourceAcl;
  }

  async saveResourceAcl(resourceAcl: any, dataset: any): Promise<void> {
    await saveAclFor(dataset, resourceAcl, {
      fetch: authClient.fetch.bind(authClient),
    });
  }

  async setPermissionsForResource(
    resourceUrl: string,
    items: PermissionContextItem[]
  ) {
    let dataset = await this.getSolidDataset(resourceUrl);
    let resourceAcl = await this.getOrCreateResourceAcl(dataset);

    if (!resourceAcl) {
      return;
    }

    for (const item of items) {
      resourceAcl = this.updateResourceAclAccess(
        resourceAcl,
        item.permissions,
        item.webId
      );
    }

    await this.saveResourceAcl(resourceAcl, dataset);
  }
}

const permissionManager: PermissionManager = new PermissionManager();

export default permissionManager;
