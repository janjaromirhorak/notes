import { PropsWithChildren } from "react";
import { Link as RouterLink } from "react-router-dom";

/**
 * Represents the props for the Link component
 */
type LinkProps = {
  /** The URL of the external link */
  to: string;
  /** The target attribute of the anchor element */
  target?: string;
  /** The rel attribute of the anchor element */
  rel?: string;
  /** Additional classnames */
  className?: string;
  /** Whether the link is external */
  isExternal?: boolean;
  /** Theme of the link */
  theme?: "primary" | "danger";
};

/**
 * A component representing a link
 *
 * @param {PropsWithChildren<LinkProps>} props
 * @category Components
 */
const Link: React.FC<PropsWithChildren<LinkProps>> = ({
  children,
  isExternal,
  target,
  rel,
  to,
  theme,
  className = "",
}) => {
  const classes = `font-semibold hover:underline ${
    theme === "danger" ? "text-red-600" : "text-indigo-600"
  } ${className}`;
  if (isExternal) {
    return (
      <a className={classes} href={to} target={target} rel={rel}>
        {children}
      </a>
    );
  }

  return (
    <RouterLink to={to} className={classes}>
      {children}
    </RouterLink>
  );
};

export default Link;
