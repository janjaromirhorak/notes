import React from "react";

/**
 * A full page loading animation
 *
 * @category Components
 */
const Loader: React.FC<{}> = () => {
  return (
    <div className="flex h-screen items-center justify-center">
      <div className="h-16 w-16 animate-spin rounded-full border-4 border-indigo-600 border-r-indigo-600/30"></div>
    </div>
  );
};

export default Loader;
