import { Switch } from "@headlessui/react";

/**
 * Represents the props for the Toggle component
 */
type ToggleProps = {
  /** Indicates whether the toggle is enabled or disabled */
  enabled: boolean;
  /** The callback function to be called when the toggle is changed */
  onChange: () => void;
};

/**
 * A component representing a toggle switch
 *
 * @param {ToggleProps} props
 * @category Components
 */
const Toggle: React.FC<ToggleProps> = ({ enabled, onChange }) => (
  <Switch
    checked={enabled}
    onChange={onChange}
    className={`relative inline-flex h-6 w-11 flex-shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus:ring-2 focus:ring-indigo-600 focus:ring-offset-2 ${
      enabled ? "bg-indigo-600" : "bg-gray-200"
    }`}
  >
    <span className="sr-only">Use setting</span>
    <span
      aria-hidden="true"
      className={`pointer-events-none inline-block h-5 w-5 transform rounded-full bg-white shadow ring-0 transition duration-200 ease-in-out ${
        enabled ? "translate-x-5" : "translate-x-0"
      }`}
    />
  </Switch>
);

export default Toggle;
