import React from "react";

/**
 * A component representing a text input field
 *
 * @prop {React.HTMLProps<HTMLInputElement>}
 * @category Components
 */
const TextInput: React.FC<React.HTMLProps<HTMLInputElement>> = ({
  className,
  ...props
}) => {
  const mergedClasses = `block w-full rounded-md border-0 py-1.5 px-4 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 outline-none focus:outline-none transition-all ${className}`;
  return <input type="text" className={mergedClasses} {...props} />;
};

export default TextInput;
