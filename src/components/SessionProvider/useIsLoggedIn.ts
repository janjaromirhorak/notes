import useSession from "./useSession";

/**
 * A hook returning true, if the user is logged in.
 */
const useIsLoggedIn = (): boolean => {
  const session = useSession();
  return !!session?.isLoggedIn;
};

export default useIsLoggedIn;
