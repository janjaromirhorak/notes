import { useContext } from "react";
import SessionContext from "./SessionContext";

/**
 * A hook returning the custom page title
 */
const useCustomTitle = () => useContext(SessionContext)?.customTitle;

export default useCustomTitle;
