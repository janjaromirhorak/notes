import { ISessionInfo } from "@inrupt/solid-client-authn-browser";
import { createContext } from "react";
import { RemoteNoteList, RemoteResourceFactory } from "../../services";

export type NoteMetadata = {
  title: string;
  uri: string;
};

export type SessionContextType = {
  session?: ISessionInfo;
  setSession?: (session: ISessionInfo | undefined) => void;
  noteList?: RemoteNoteList;
  refetchNoteList?: () => Promise<void>;
  remoteResourceFactory?: RemoteResourceFactory;
  customTitle?: string;
  setCustomTitle?: (title?: string) => void;
  noteMetadata?: NoteMetadata[];
};

/**
 * A context containing the current user session and a setter
 */
const SessionContext = createContext<SessionContextType>({
  session: undefined,
  setSession: undefined,
});

export default SessionContext;
