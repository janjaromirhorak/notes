import useSession from "./useSession";

/**
 * A hook returning the WebID of the currently logged in user
 */
const useWebId = (): string | undefined => {
  const session = useSession();
  return session?.webId ? session.webId : undefined;
};

export default useWebId;
