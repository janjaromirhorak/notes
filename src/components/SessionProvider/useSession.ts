import { useContext } from "react";

import { ISessionInfo } from "@inrupt/solid-client-authn-browser";
import SessionContext from "./SessionContext";

/**
 * A hook returning the current user session
 */
const useSession = (): ISessionInfo | undefined => {
  const sessionData = useContext(SessionContext);
  if (sessionData?.session) {
    return sessionData.session;
  }
  return undefined;
};

export default useSession;
