import { useCallback, useContext } from "react";
import { SCHEMAORG } from "../../services/namespaces";
import SessionContext from "./SessionContext";

/**
 * A hook returning note list related helpers
 */
const useNoteListHelpers = () => {
  const { noteList, refetchNoteList, noteMetadata } =
    useContext(SessionContext);

  const addNoteToList = useCallback(
    async (noteUri: string) => {
      if (noteList) {
        const isAccessible = await noteList.isAccessible();
        if (!isAccessible) {
          await noteList.createRemoteResource(SCHEMAORG("ItemList"));
        }
        await noteList.addNoteToList(noteUri);
        if (refetchNoteList) {
          await refetchNoteList();
        }
      }
    },
    [noteList, refetchNoteList]
  );

  const removeNoteFromList = useCallback(
    async (noteUri: string) => {
      if (noteList) {
        await noteList.removeNoteFromList(noteUri);
        if (refetchNoteList) {
          await refetchNoteList();
        }
      }
    },
    [noteList, refetchNoteList]
  );

  return {
    noteList,
    refetchNoteList: refetchNoteList ?? (() => new Promise((r) => r())),
    noteMetadata,
    addNoteToList,
    removeNoteFromList,
  };
};

export default useNoteListHelpers;
