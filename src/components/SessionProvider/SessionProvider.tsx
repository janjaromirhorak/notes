import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useSafeState } from "../../hooks/useSafeState";

import {
  ISessionInfo,
  handleIncomingRedirect,
  onSessionRestore,
} from "@inrupt/solid-client-authn-browser";
import { useNavigate } from "react-router-dom";
import {
  RemoteNoteList,
  RemoteResourceFactory,
  getCombinedTitle,
} from "../../services";
import getNoteListUriFromWebId from "../../services/getNoteListUriFromWebId";
import SessionContext, { NoteMetadata } from "./SessionContext";

type SessionProviderProps = {
  children?: React.ReactNode;
};

/**
 * Context provider for the SessionContext
 *
 * Initializes the user session and passes the context using a provider.
 *
 * @category Components
 * @category Components
 * @see SessionContext
 */
const SessionProvider: React.FC<SessionProviderProps> = ({
  children = null,
}: SessionProviderProps) => {
  const [session, setSession] = useSafeState<ISessionInfo | undefined>(
    undefined
  );
  const [noteList, setNoteList] = useSafeState<RemoteNoteList | undefined>(
    undefined
  );
  const [customTitle, setCustomTitle] = useState<string>();
  const [noteMetadata, setNoteMetadata] = useState<NoteMetadata[] | undefined>(
    undefined
  );

  const remoteResourceFactory = useMemo(() => new RemoteResourceFactory(), []);

  const navigate = useNavigate();

  // Hook for handling incoming redirects from auth providers
  useEffect(() => {
    // Redirect the user to their original URL after session restoration
    onSessionRestore((url) => {
      const parsedUrl = new URL(url);
      if (
        parsedUrl.origin === new URL(window.location.href).origin &&
        parsedUrl.pathname &&
        history
      ) {
        navigate(parsedUrl.pathname);
      }
    });

    handleIncomingRedirect({
      restorePreviousSession: true,
    })
      .then((sessionInfo) => {
        setSession(sessionInfo);
      })
      .catch((e) => {
        setSession(undefined);
      })
      .finally(() => {
        remoteResourceFactory.clearCache();
      });
  }, [history, setSession]);

  const refetchNoteList = useCallback(async () => {
    if (noteList) {
      const notes = await noteList.getNotes(true);
      setNoteMetadata(
        await Promise.all(
          notes.map(
            async (note): Promise<NoteMetadata> => ({
              title: await note.getTitle(true),
              uri: note.getUri(),
            })
          )
        )
      );
    } else {
      setNoteMetadata(undefined);
    }
  }, [noteList, setNoteMetadata, navigate]);

  // Hook for handling additional data derived from session information
  useEffect(() => {
    const noteListUri = session?.webId
      ? getNoteListUriFromWebId(session.webId)
      : undefined;

    if (noteListUri) {
      setNoteList(remoteResourceFactory.getNoteList(noteListUri));
      refetchNoteList();
    }
  }, [session?.webId, setNoteList, refetchNoteList]);

  useEffect(() => {
    document.title = getCombinedTitle(customTitle);
  }, [customTitle]);

  return (
    <SessionContext.Provider
      value={{
        session,
        setSession,
        noteList,
        refetchNoteList,
        remoteResourceFactory,
        customTitle,
        setCustomTitle,
        noteMetadata,
      }}
    >
      {children}
    </SessionContext.Provider>
  );
};

export default SessionProvider;
