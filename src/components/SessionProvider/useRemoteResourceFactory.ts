import { useContext } from "react";
import { RemoteResourceFactory } from "../../services";
import SessionContext from "./SessionContext";

/**
 * A hook returning an instance of RemoteResourceFactory
 */
const useRemoteResourceFactory = () =>
  useContext(SessionContext).remoteResourceFactory ??
  // fallback, should never happen
  new RemoteResourceFactory();

export default useRemoteResourceFactory;
