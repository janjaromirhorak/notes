import { useContext } from "react";
import SessionContext from "./SessionContext";

/**
 * A hook returning the setter for the custom page title
 */
const useSetCustomTitle = () =>
  useContext(SessionContext)?.setCustomTitle ?? (() => {});

export default useSetCustomTitle;
