import React from "react";

import { useTranslation } from "react-i18next";
import useReloadPage from "../../hooks/useReloadPage";
import Button, { ButtonLink } from "../Button";
import ErrorBox from "../ErrorBox";

/**
 * A component to indicate a critical error to the user.
 * The component is rendered from ErrorBoundary
 *
 * @category Components
 */
const GlobalError: React.FC<{}> = () => {
  const { t } = useTranslation();
  const reloadPage = useReloadPage();

  return (
    <ErrorBox title={t("globalError.title")}>
      <p className="mb-8 max-w-[30rem]">{t("globalError.text")}</p>
      <div className="flex gap-3">
        <Button onClick={reloadPage} theme="primary">
          {t("globalError.reload")}
        </Button>
        <ButtonLink
          href={"https://gitlab.com/janjaromirhorak/notes/-/issues"}
          isExternal
          theme="secondary"
        >
          {t("globalError.issue")}
        </ButtonLink>
      </div>
    </ErrorBox>
  );
};

export default GlobalError;
