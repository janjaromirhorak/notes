import React from "react";
import { useSafeState } from "../../hooks/useSafeState";

/**
 * Represents a single tab page
 */
type TabPage = {
  /** The title of the tab */
  title: string;
  /** The content of the tab */
  content: React.ReactNode;
};

/**
 * Represents the props for the Tabs component
 */
type TabProps = {
  /** The array of tab pages */
  tabs: TabPage[];
};
const tabButtonClasses = {
  active: "text-indigo-500 border-b-2 border-indigo-500",
  inactive:
    "text-slate-500 border-slate-200 hover:text-slate-700 hover:border-slate-400 focus-visible:text-slate-700 focus-visible:border-slate-400",
};

/**
 * A component representing a set of tabs
 *
 * @param {TabProps} props
 * @category Components
 */
const Tabs: React.FC<TabProps> = ({ tabs }) => {
  const [activeTab, setActiveTab] = useSafeState<number>(0);

  return (
    <div className="flex flex-col gap-4">
      <div className="flex">
        {tabs.map(({ title }, index) => (
          <button
            key={index}
            onClick={() => setActiveTab(index)}
            className={`block border-b px-6 py-3 text-sm font-medium ${
              tabButtonClasses[activeTab === index ? "active" : "inactive"]
            }`}
          >
            {title}
          </button>
        ))}
        <div className="flex-1 border-b border-slate-200" />
      </div>
      <div>{tabs[activeTab ?? 0]?.content}</div>
    </div>
  );
};

export default Tabs;
