import { ChevronRightIcon } from "@heroicons/react/24/outline";
import React, { ChangeEventHandler, FormEventHandler } from "react";
import Button from "../Button";
import TextInput from "../TextInput";

/**
 * Represents the props for the AddressBar component
 */
type AddressBarProps = {
  /** Event handler for form submission */
  onSubmit: FormEventHandler<HTMLFormElement>;

  /** Event handler for input value change   */
  onChange: ChangeEventHandler<HTMLInputElement>;

  /** The current value of the input */
  value: string;
};

/**
 * A component representing an address bar
 *
 * @prop {AddressBarProps}
 * @category Components
 */
const AddressBar: React.FC<AddressBarProps> = ({
  onSubmit,
  onChange,
  value,
}: AddressBarProps) => {
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        onSubmit(e);
      }}
    >
      <div className="flex gap-3 px-3">
        <TextInput
          placeholder="https://..."
          value={value}
          onChange={onChange}
        />
        <Button theme="secondary" aria-label="Submit">
          <ChevronRightIcon className="w-4 stroke-2" />
        </Button>
      </div>
    </form>
  );
};

export default AddressBar;
