import { FolderIcon } from "@heroicons/react/24/solid";
import React from "react";

import FileBrowserItem from "./FileBrowserItem";

/** Props for the FileBrowserFolderItem component */
type FileBrowserFolderItemProps = {
  /** The name of the folder */
  name: string;
  /** The callback function to be called when the folder item is clicked (optional) */
  onClick?: () => void;
};

/**
 * A component representing an item in the file browser for a folder
 *
 * @param {FileBrowserFolderItemProps} props
 * @category Components
 */
const FileBrowserFolderItem: React.FC<FileBrowserFolderItemProps> = ({
  name,
  onClick,
}) => {
  return (
    <FileBrowserItem name={name} onClick={onClick} icon={<FolderIcon />} />
  );
};

export default FileBrowserFolderItem;
