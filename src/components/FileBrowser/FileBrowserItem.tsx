import { DocumentIcon } from "@heroicons/react/24/solid";
import React from "react";

/** Props for the FileBrowserItem component */
type FileBrowserItemProps = {
  /** The name of the item */
  name: string;
  /** The icon for the item (optional) */
  icon?: any;
  /** The callback function to be called when the item is clicked (optional) */
  onClick?: () => void | undefined;
  /** Additional CSS class names for the item (optional) */
  className?: string;
};

/**
 * A component representing an item in the file browser
 *
 * @param {FileBrowserItemProps} props
 * @category Components
 */
const FileBrowserItem: React.FC<FileBrowserItemProps> = ({
  name,
  icon,
  onClick,
  className = "",
}: FileBrowserItemProps) => {
  return (
    <div
      className={`flex items-center gap-2 rounded-md p-2 ${
        onClick ? "cursor-pointer hover:bg-slate-200" : "cursor-auto"
      } ${className}`}
      onClick={onClick}
    >
      <div className="flex w-6 justify-center pt-0.5">
        {icon ?? <DocumentIcon />}
      </div>
      <div>{name}</div>
    </div>
  );
};

export default FileBrowserItem;
