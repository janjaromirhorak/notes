import { XCircleIcon } from "@heroicons/react/20/solid";
import React, { useCallback, useEffect, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useSafeState } from "../../hooks/useSafeState";
import { uriToUid } from "../../services";
import RemoteDirectory from "../../services/RemoteDirectory";
import RemoteResource from "../../services/RemoteResource";
import useRemoteResourceFactory from "../SessionProvider/useRemoteResourceFactory";
import AddressBar from "./AddressBar";
import FileBrowserContent from "./FileBrowserContent";

/** Props for the FileBrowser component */
type FileBrowserProps = {
  /** default file browser URI */
  defaultUri?: string;
};

/**
 * Implementation of the Pod Browser. Allows the user to explore Collections on Solid Pods.
 *
 * @prop {FileBrowserProps}
 * @category Components
 */
const FileBrowser: React.FC<FileBrowserProps> = ({ defaultUri }) => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const factory = useRemoteResourceFactory();

  const [firstLoad, setFirstLoad] = useSafeState<boolean>(true);
  const [loadingDirectory, setLoadingDirectory] = useSafeState<boolean>(true);
  const [loadingError, setLoadingError] = useSafeState<boolean>(false);
  const [error, setError] = useSafeState<any>(undefined);
  const [addressBarValue, setAddressBarValue] = useSafeState<string>("");
  const [currentDirectory, setCurrentDirectory] = useSafeState<
    RemoteDirectory | undefined
  >(undefined);
  const [currentListedFiles, setCurrentListedFiles] = useSafeState<
    RemoteResource[]
  >([]);
  const [currentListedDirectories, setCurrentListedDirectiories] = useSafeState<
    RemoteDirectory[]
  >([]);

  const loadNewDirectory = useCallback(
    async (uri: string, parentUri?: string) => {
      if (uri.slice(-1) !== "/") {
        uri = `${uri}/`;
      }

      setAddressBarValue(uri);
      setLoadingDirectory(true);

      let newDirectory = factory.getDirectory(uri);

      if (parentUri) {
        newDirectory.setParentUri(parentUri);
      } else {
        newDirectory.setParentUri(undefined);
      }

      try {
        const { files, folders } = await newDirectory.getDirectoryListing();

        setLoadingDirectory(false);
        setLoadingError(false);
        setCurrentDirectory(newDirectory);
        setCurrentListedFiles(files);
        setCurrentListedDirectiories(folders);
      } catch (e) {
        // @ts-expect-error incomplete error typing
        if (e.status && e.statusText) {
          setLoadingDirectory(false);
          setLoadingError(true);
          setError(e);
        } else {
          throw e;
        }
      }
    },
    [
      setAddressBarValue,
      setCurrentListedDirectiories,
      setCurrentListedFiles,
      setError,
      setLoadingDirectory,
      setLoadingError,
      setCurrentDirectory,
    ]
  );

  const onAddressBarChange = useCallback(
    ({ target }: { target: { value: string } }) => {
      setAddressBarValue(target.value);
    },
    [setAddressBarValue]
  );

  const onAddressBarSubmit = useCallback(
    async (valueOverride?: string): Promise<void> => {
      let location = valueOverride || addressBarValue;
      if (!location) {
        return;
      }

      if (location.slice(-1) !== "/") {
        location = `${location}/`;
      }

      let remoteDocument = factory.getResource(location);

      // check if the location is a valid directory
      const isDirectory = await remoteDocument.isDirectory();
      if (isDirectory) {
        let tempDir = factory.getDirectory(location);
        const guessedParentUri = await tempDir.guessParentUri();
        await loadNewDirectory(location, guessedParentUri);
      } else {
        // check if the location contains a note
        const hasNote = await remoteDocument.hasNote();
        if (hasNote) {
          remoteDocument = remoteDocument.getNoteEquivalent();
          navigate(`/note/${uriToUid(remoteDocument.getUri())}`);
        }
      }
    },
    [addressBarValue, loadNewDirectory, history]
  );

  useEffect(() => {
    if (defaultUri && firstLoad) {
      setAddressBarValue(defaultUri);
      onAddressBarSubmit(defaultUri).then(() => {
        setFirstLoad(false);
        setLoadingDirectory(false);
      });
    }
  }, [
    defaultUri,
    onAddressBarSubmit,
    firstLoad,
    setAddressBarValue,
    setFirstLoad,
    setLoadingDirectory,
  ]);

  const parentDirectory = useMemo<RemoteDirectory | undefined>(() => {
    if (currentDirectory) {
      const parentUri = currentDirectory.getParentUri();
      if (parentUri) {
        return factory.getDirectory(parentUri);
      }
    }

    return undefined;
  }, [currentDirectory]);

  return (
    <>
      <AddressBar
        onSubmit={() => onAddressBarSubmit()}
        onChange={onAddressBarChange}
        value={addressBarValue ?? ""}
      />
      {loadingDirectory ||
      !currentDirectory ||
      currentListedDirectories === undefined ||
      currentListedFiles === undefined ? (
        <div className="m-3 flex animate-pulse items-center justify-center rounded-md bg-slate-100 py-24 text-sm font-semibold text-slate-600">
          Loading data from Solid...
        </div>
      ) : loadingError ? (
        <div className="mt-5 rounded-md bg-red-50 p-4">
          <div className="flex">
            <div className="flex-shrink-0">
              <XCircleIcon
                className="h-5 w-5 text-red-400"
                aria-hidden="true"
              />
            </div>
            <div className="ml-3">
              <p className="text-sm text-red-800">
                {error ? error.message : t("fileBrowser.genericError")}
              </p>
            </div>
          </div>
        </div>
      ) : (
        <>
          <FileBrowserContent
            directory={currentDirectory}
            parentDirectory={parentDirectory}
            listedDirectories={currentListedDirectories}
            listedFiles={currentListedFiles}
            openDirectory={loadNewDirectory}
          />
        </>
      )}
    </>
  );
};

export default FileBrowser;
