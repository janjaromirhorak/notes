import React, { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { RiAddFill } from "react-icons/ri";
import { useSafeState } from "../../hooks/useSafeState";

import { CheckIcon, DocumentPlusIcon } from "@heroicons/react/24/outline";
import { useNavigate } from "react-router-dom";
import { useWebId } from "../../components/SessionProvider";
import { RemoteDirectory, uriToUid } from "../../services";
import Button from "../Button";
import Modal from "../Modal";
import useNoteListHelpers from "../SessionProvider/useNoteListHelpers";
import useRemoteResourceFactory from "../SessionProvider/useRemoteResourceFactory";
import TextInput from "../TextInput";

const ValidatedInputStatus: {
  NONE: symbol;
  VALID: symbol;
  WARNING: symbol;
  INVALID: symbol;
} = Object.freeze({
  NONE: Symbol("none"),
  VALID: Symbol("valid"),
  WARNING: Symbol("warning"),
  INVALID: Symbol("invalid"),
});

/**
 * Represents the props for the CreateNoteButton component
 */
type CreateNoteButtonProps = {
  /** The current directory for creating a new note */
  currentDirectory: RemoteDirectory;
};

/**
 * A component representing a button to create a new note
 *
 * @prop {CreateNoteButtonProps}
 * @category Components
 */
const CreateNoteButton: React.FC<CreateNoteButtonProps> = ({
  currentDirectory,
}) => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const webId = useWebId();
  const factory = useRemoteResourceFactory();

  const [isModalOpen, setIsModalOpen] = useSafeState<boolean>(false);
  const [isModalOpening, setIsModalOpening] = useSafeState<boolean>(false);
  const [isCreatingNote, setIsCreatingNote] = useSafeState<boolean>(false);
  const [newNoteName, setNewNoteName] = useSafeState<string>("");
  const [newNoteNameStatus, setNewNoteNameStatus] = useSafeState<symbol>(
    ValidatedInputStatus.NONE
  );
  const [newNoteNameStatusMessage, setNewNoteNameStatusMessage] = useSafeState<
    string | undefined
  >(undefined);
  const [isValidatingNoteName, setIsValidatingNoteName] =
    useSafeState<boolean>(false);
  const { addNoteToList } = useNoteListHelpers();

  const getNewNoteFileName = useCallback(async () => {
    if (currentDirectory) {
      let suffix = 0;
      let maxSuffix = 100;
      while (suffix < maxSuffix) {
        const fileName =
          currentDirectory.getUri() +
          t("createNewNote.newFilePrefix") +
          (suffix ? suffix : "") +
          ".ttl";
        const potentialNewNote = factory.getResource(fileName);
        const canBeCreated = await potentialNewNote.newFileCanBeCreatedHere();

        if (canBeCreated) {
          return fileName;
        }

        suffix++;
      }
    }

    return "";
  }, [currentDirectory, t]);

  const validateNoteName = useCallback(
    async (name: string): Promise<{ status: symbol; message?: string }> => {
      if (name.length === 0) {
        return {
          status: ValidatedInputStatus.INVALID,
          message: t("createNewNote.pathMustNotBeEmpty"),
        };
      }
      setIsValidatingNoteName(true);
      const potentialNewNote = factory.getResource(name);
      const fileCanBeCreated = await potentialNewNote.newFileCanBeCreatedHere();
      setIsValidatingNoteName(false);
      if (fileCanBeCreated) {
        return {
          status: ValidatedInputStatus.VALID,
          message: undefined,
        };
      } else {
        return {
          status: ValidatedInputStatus.INVALID,
          message: t("createNewNote.fileCannotBeCreated"),
        };
      }
    },
    [t, setIsValidatingNoteName]
  );

  const closeModal = useCallback(() => {
    setIsModalOpen(false);
  }, [setIsModalOpen]);

  const openModal = useCallback(() => {
    setIsModalOpening(true);
    setIsCreatingNote(false);
    setNewNoteNameStatus(ValidatedInputStatus.NONE);
    setNewNoteNameStatusMessage(undefined);
    getNewNoteFileName()
      .then((name) => {
        setNewNoteName(name);
        return validateNoteName(name);
      })
      .then(({ status, message }) => {
        setNewNoteNameStatus(status);
        setNewNoteNameStatusMessage(message);
        setIsModalOpen(true);
      })
      .finally(() => {
        setIsModalOpening(false);
      });
  }, [
    getNewNoteFileName,
    validateNoteName,
    setIsModalOpening,
    setIsCreatingNote,
    setNewNoteNameStatus,
    setNewNoteNameStatusMessage,
    setIsModalOpen,
    setNewNoteName,
  ]);

  const onNewNoteNameChange = useCallback(
    ({ currentTarget }: { currentTarget: HTMLInputElement }) => {
      setNewNoteName(currentTarget.value);
      validateNoteName(currentTarget.value).then(({ status, message }) => {
        setNewNoteNameStatus(status);
        setNewNoteNameStatusMessage(message);
      });
    },
    [
      validateNoteName,
      setNewNoteName,
      setNewNoteNameStatus,
      setNewNoteNameStatusMessage,
    ]
  );

  const onCreateNote = useCallback(() => {
    setIsCreatingNote(true);
    const newNoteUri = newNoteName ?? "";
    validateNoteName(newNoteUri).then(({ status, message }) => {
      if (status === ValidatedInputStatus.VALID) {
        // create a new note at this location
        let newNote = factory.getNote(newNoteUri);
        newNote = newNote.getNoteEquivalent();

        newNote
          .createRemoteNote(t("createNewNote.newFileName"))
          .then(() => {
            // refresh the directory listing
            const newNoteDir = factory.getDirectory(newNote.getClosestDirUri());
            newNoteDir.clearListingCache();

            const newNoteUri = newNote.getUri();
            addNoteToList(newNoteUri);
            setIsCreatingNote(false);
            navigate(`/note/${uriToUid(newNoteUri)}`);
          })
          .catch((error) => {
            setNewNoteNameStatus(ValidatedInputStatus.INVALID);
            setNewNoteNameStatusMessage(
              error.message
                ? error.message
                : t("createNewNote.fileCannotBeCreated")
            );
            setIsCreatingNote(false);
          });
      } else {
        setNewNoteNameStatus(status);
        setNewNoteNameStatusMessage(message);
        setIsCreatingNote(false);
      }
    });
  }, [
    newNoteName,
    validateNoteName,
    navigate,
    t,
    webId,
    setIsCreatingNote,
    setNewNoteNameStatus,
    setNewNoteNameStatusMessage,
  ]);

  return (
    <>
      <Modal
        title="Create a new note"
        submitButtonText={t("createNewNote.submit")}
        Icon={DocumentPlusIcon}
        isOpen={!!isModalOpen}
        close={closeModal}
        onSubmit={onCreateNote}
        isSubmitting={isCreatingNote}
        isSubmitDisabled={!!newNoteNameStatusMessage}
      >
        <label className="relative block">
          <span className="mb-2 block">
            {t("createNewNote.enterNoteLocation")}
          </span>
          <TextInput
            value={newNoteName}
            onChange={onNewNoteNameChange}
            className="pr-8"
          />
          {isValidatingNoteName ? (
            <div className="absolute bottom-2.5 right-2 block h-4 w-4 animate-spin rounded-full border-2 border-indigo-600 border-r-indigo-600/30" />
          ) : newNoteNameStatus === ValidatedInputStatus.VALID ? (
            <CheckIcon className="absolute bottom-2.5 right-2 w-4 stroke-2 text-indigo-700" />
          ) : null}
        </label>
        {newNoteNameStatusMessage && (
          <div className="mt-1 text-sm text-red-500">
            {newNoteNameStatusMessage}
          </div>
        )}
      </Modal>
      <div className="ml-10 mt-2">
        <Button
          theme="primary"
          onClick={openModal}
          isLoading={isModalOpening}
          leftIcon={<RiAddFill />}
        >
          {t("createNewNote.buttonText")}
        </Button>
      </div>
    </>
  );
};

export default CreateNoteButton;
