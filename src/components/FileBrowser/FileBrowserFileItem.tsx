import {
  DocumentIcon,
  DocumentTextIcon,
  ExclamationTriangleIcon,
  QueueListIcon,
} from "@heroicons/react/24/solid";
import React, { useEffect } from "react";
import { useSafeState } from "../../hooks/useSafeState";

import { useNavigate } from "react-router-dom";
import type { RemoteResource } from "../../services";
import { uriToUid } from "../../services";
import getNoteListUriFromWebId from "../../services/getNoteListUriFromWebId";
import { useWebId } from "../SessionProvider";
import FileBrowserItem from "./FileBrowserItem";

/** Props for the FileBrowserFileItem component */
type FileBrowserFileItemProps = {
  /** The name of the file */
  name: string;
  /** The file resource */
  file: RemoteResource;
};

/**
 * A component representing an item in the file browser for a file
 *
 * @param {FileBrowserFileItemProps} props
 * @category Components
 */
const FileBrowserFileItem: React.FC<FileBrowserFileItemProps> = ({
  name,
  file,
}) => {
  const navigate = useNavigate();
  const webId = useWebId();
  const [isLoaded, setIsLoaded] = useSafeState<boolean>(false);
  const [isForbidden, setIsForbidden] = useSafeState<boolean>(false);
  const [type, setType] = useSafeState<"unknown" | "note" | "noteList">(
    "unknown"
  );

  useEffect(() => {
    if (
      file.getFileUri() ===
      (getNoteListUriFromWebId(webId) ?? "").replace(/#this$/, "")
    ) {
      setIsLoaded(true);
      setType("noteList");
    } else {
      file
        .hasNote()
        .then((isNote) => {
          setIsLoaded(true);
          setType(isNote ? "note" : "unknown");
        })
        .catch(() => {
          setIsLoaded(true);
          setIsForbidden(true);
        });
    }
  }, [file, setIsForbidden, setIsLoaded]);

  return (
    <FileBrowserItem
      name={`${type === "note" ? name.replace(/#this$/, "") : name}${
        type === "noteList" ? " (list of your notes)" : ""
      }`}
      icon={
        isLoaded ? (
          isForbidden ? (
            <ExclamationTriangleIcon />
          ) : type === "note" ? (
            <DocumentTextIcon className="text-indigo-600" />
          ) : type === "noteList" ? (
            <QueueListIcon />
          ) : (
            <div className="relative">
              <DocumentIcon className="h-6 w-6" />
              <div className="pointer-events-none absolute bottom-0 left-0.5 flex h-5 w-5 items-center justify-center text-xs font-black text-white [user-select:none]">
                ?
              </div>
            </div>
          )
        ) : (
          <div className="h-4 w-4 animate-spin rounded-full border-2 border-slate-600 border-t-transparent"></div>
        )
      }
      onClick={
        type === "note"
          ? () => {
              navigate("/note/" + uriToUid(file.getUri()));
            }
          : undefined
      }
    />
  );
};

export default FileBrowserFileItem;
