import React, { useCallback } from "react";

import { useIsLoggedIn } from "../../components/SessionProvider";
import { RemoteDirectory, RemoteResource } from "../../services";
import CreateNoteButton from "./CreateNoteButton";
import FileBrowserFileItem from "./FileBrowserFileItem";
import FileBrowserFolderItem from "./FileBrowserFolderItem";

/** Props for the FileBrowserContent component */
type FileBrowserContentProps = {
  /** The current directory */
  directory: RemoteDirectory;
  /** The parent directory (optional) */
  parentDirectory?: RemoteDirectory;
  /** The array of listed files */
  listedFiles: RemoteResource[];
  /** The array of listed directories */
  listedDirectories: RemoteDirectory[];
  /**
   * Function to open a directory
   *
   * @param {string} uri - The URI of the directory to open
   * @param {string} [parentUri] - The URI of the parent directory (optional)
   */
  openDirectory: (uri: string, parentUri?: string) => Promise<void>;
};

/**
 * A component representing the content of a file browser
 *
 * @prop {FileBrowserContentProps} props - The props for the FileBrowserContent component
 * @category Components
 */
const FileBrowserContent: React.FC<FileBrowserContentProps> = ({
  directory,
  parentDirectory,
  listedFiles = [],
  listedDirectories = [],
  openDirectory,
}) => {
  const isLoggedIn = useIsLoggedIn();

  const getRelativeUri = useCallback(
    (uri: string) => {
      if (
        directory &&
        uri.substring(0, directory.getUri().length) === directory.getUri()
      ) {
        return uri.substring(directory.getUri().length);
      }

      return uri;
    },
    [directory]
  );

  return (
    <div className="flex flex-col pt-3">
      {parentDirectory ? (
        <FileBrowserFolderItem
          name={".."}
          onClick={() => openDirectory(parentDirectory.getUri())}
        />
      ) : null}
      {listedDirectories.map((dir) => {
        return (
          <FileBrowserFolderItem
            key={dir.getUri()}
            name={getRelativeUri(dir.getUri())}
            onClick={() => openDirectory(dir.getUri(), directory.getUri())}
          />
        );
      })}
      {listedFiles.map((file) => {
        return (
          <FileBrowserFileItem
            key={file.getUri()}
            file={file}
            name={getRelativeUri(file.getUri())}
          />
        );
      })}
      {isLoggedIn ? <CreateNoteButton currentDirectory={directory} /> : null}
    </div>
  );
};

export default FileBrowserContent;
