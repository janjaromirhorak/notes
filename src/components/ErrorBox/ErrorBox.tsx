import React from "react";

import { ExclamationCircleIcon } from "@heroicons/react/20/solid";

/**
 * Represents the props for the ErrorBox component
 */
type ErrorBoxProps = {
  /** The title of the error box */
  title: React.ReactNode;
  /** The content of the error box */
  children?: React.ReactNode;
};

/**
 * A component representing an error box
 *
 * @param {ErrorBoxProps} props
 * @category Components
 */
const ErrorBox: React.FC<ErrorBoxProps> = ({ title, children }) => (
  <div className="mx-auto my-8 flex max-w-screen-lg flex-col items-center rounded-md bg-red-100 p-12 text-center">
    <ExclamationCircleIcon className="mb-4 w-12 text-red-600" />
    <h1 className="mb-6 text-center text-2xl font-bold">{title}</h1>
    {children}
  </div>
);
export default ErrorBox;
