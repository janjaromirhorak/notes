import { Dialog, Transition } from "@headlessui/react";
import { Fragment, PropsWithChildren, useRef } from "react";
import Button from "../Button";

type ModalProps = {
  title: string;
  hideCancelButton?: boolean;
  cancelButtonText?: string;
  submitButtonText?: string;
  theme?: "primary" | "danger";
  Icon?: React.ComponentType;
  isOpen: boolean;
  close: () => void;
  onSubmit?: () => void;
  onCancel?: () => void;
  isSubmitting?: boolean;
  isSubmitDisabled?: boolean;
};

const Modal: React.FC<PropsWithChildren<ModalProps>> = ({
  title,
  children,
  hideCancelButton,
  submitButtonText = "Submit",
  cancelButtonText = "Cancel",
  theme = "primary",
  Icon,
  isOpen,
  close,
  onSubmit,
  onCancel,
  isSubmitting,
  isSubmitDisabled,
}) => {
  const cancelButtonRef = useRef(null);

  return (
    <Transition.Root show={isOpen} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-[100]"
        initialFocus={cancelButtonRef}
        onClose={close}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative transform overflow-visible rounded-lg bg-white text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg">
                <div className="rounded-lg bg-white px-4 pb-4 pt-5 sm:p-6 sm:pb-4">
                  <div className="sm:flex sm:items-start">
                    {Icon && (
                      <div
                        className={`mx-auto flex h-12 w-12 flex-shrink-0 items-center justify-center rounded-full sm:mx-0 sm:h-10 sm:w-10 ${
                          theme === "danger" ? "bg-red-100" : "bg-indigo-100"
                        }`}
                      >
                        <Icon
                          /* @ts-expect-error ignore missing props */
                          className={`h-6 w-6 ${
                            theme === "danger"
                              ? "text-red-600"
                              : "text-indigo-600"
                          }`}
                          aria-hidden="true"
                        />
                      </div>
                    )}

                    <div className="mt-3 text-center sm:ml-4 sm:mt-0 sm:flex-1 sm:text-left">
                      <Dialog.Title
                        as="h3"
                        className="text-base font-semibold leading-6 text-gray-900"
                      >
                        {title}
                      </Dialog.Title>
                      <div className="mt-2">{children}</div>
                    </div>
                  </div>
                </div>
                {onSubmit && (
                  <div className="gap-2 rounded-b-lg bg-gray-50 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6">
                    <Button
                      onClick={onSubmit}
                      theme={theme}
                      isLoading={isSubmitting}
                      isDisabled={isSubmitDisabled}
                    >
                      {submitButtonText}
                    </Button>
                    {!hideCancelButton && (
                      <Button
                        onClick={onCancel ?? close}
                        ref={cancelButtonRef}
                        theme="secondary"
                      >
                        {cancelButtonText}
                      </Button>
                    )}
                  </div>
                )}
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

export default Modal;
