import { BellIcon } from "@heroicons/react/24/outline";
import React, { Fragment, useCallback, useEffect } from "react";
import { createPortal } from "react-dom";
import { useSafeState } from "../../hooks/useSafeState";

import * as authClient from "@inrupt/solid-client-authn-browser";
import { WebsocketNotification } from "@inrupt/solid-client-notifications";
import { RemoteNotification } from "../../services";

import { Popover, Transition } from "@headlessui/react";
import { useTranslation } from "react-i18next";
import useRemoteResourceFactory from "../SessionProvider/useRemoteResourceFactory";
import NotificationListItem from "./NotificationListItem";

type NotificationsContentProps = {
  inboxUrl: string;
};

/**
 * Notification wrapper for the Bell Icon and the Notifications Panel
 *
 * @category Components
 * @memberOf Notification
 */
const NotificationsContent: React.FC<NotificationsContentProps> = ({
  inboxUrl,
}) => {
  const { t } = useTranslation();
  const factory = useRemoteResourceFactory();
  const [isInitialized, setIsInitialized] = useSafeState(false);
  // The component tries to use WebSocket notifications supported by some Solid Pod implementations.
  // When the WebSocket apporach fails, polling is used instead;
  const [usePolling, setUsePolling] = useSafeState(false);

  useEffect(() => {
    const websocket = new WebsocketNotification(inboxUrl, {
      fetch: authClient.fetch.bind(authClient),
    });

    websocket.on("message", () => {
      // The message format is not consistent across Solid Pods yet.
      // For now, use the messages just as a change indication.
      loadNotifications();
    });

    websocket.connect().catch(() => {
      // notifications are not supported - switch to polling
      setUsePolling(true);
    });

    return () => {
      websocket.disconnect();
    };
  }, [inboxUrl]);

  const [notifications, setNotifications] = useSafeState<RemoteNotification[]>(
    []
  );
  const [hasUnreadNotifications, setHasUnreadNotifications] =
    useSafeState<boolean>(false);
  const loadNotifications = useCallback(async () => {
    const notifications = await factory
      .getInbox(inboxUrl)
      .getNotifications(true);

    // filter notifications to get only those, that link to an accessible note
    let noteNotifications = (
      await Promise.all(
        notifications.map(async (notification) => {
          const objectUri = await notification.getObjectUri();
          if (objectUri) {
            const note = factory.getNote(objectUri);
            const isNote = await note.isNote();
            if (isNote) {
              return notification;
            }
          }
          return undefined;
        })
      )
    ).filter((notification) => !!notification) as RemoteNotification[];

    setNotifications(noteNotifications);
    setIsInitialized(true);
  }, [inboxUrl, setIsInitialized, setNotifications]);

  useEffect(() => {
    loadNotifications();
  }, [loadNotifications]);

  useEffect(() => {
    // update unread count
    Promise.all(
      (notifications ?? []).map((notification) =>
        notification.getReadStatus(true)
      )
    ).then((readStatuses) => {
      setHasUnreadNotifications(readStatuses.some((read) => !read));
    });
  }, [notifications, setHasUnreadNotifications]);

  // periodically reload notifications
  useEffect(() => {
    if (usePolling) {
      const interval = window.setInterval(() => {
        loadNotifications();
      }, 30 * 1000);

      return () => {
        window.clearInterval(interval);
      };
    }
  }, [loadNotifications, usePolling]);

  return (
    <Popover className="relative flex  p-0">
      <Popover.Button className="relative mx-4 outline-none focus:outline-none active:outline-none">
        <BellIcon className="h-auto w-6 text-slate-600" />
        {hasUnreadNotifications ? (
          <span className="absolute right-0.5 top-0.5 h-2 w-2 rounded-md bg-orange-600 py-0.5"></span>
        ) : null}
      </Popover.Button>
      {createPortal(
        <Transition
          as={Fragment}
          enter="transition ease-out duration-200"
          enterFrom="opacity-0 translate-y-1"
          enterTo="opacity-100 translate-y-0"
          leave="transition ease-in duration-150"
          leaveFrom="opacity-100 translate-y-0"
          leaveTo="opacity-0 translate-y-1"
        >
          <Popover.Panel className="absolute right-0 top-12 z-[60] mt-5 flex w-screen max-w-max px-4 lg:left-32 ">
            <div className="max-h-[30rem] w-screen max-w-md flex-auto overflow-hidden overflow-y-auto rounded-3xl bg-white text-sm leading-6 shadow-lg ring-1 ring-gray-900/5">
              {!isInitialized ? (
                <div className="animate-pulse bg-slate-200 px-8 py-12 text-center text-slate-800">
                  {t("editor.notifications.loadingNotifications")}
                </div>
              ) : (
                <>
                  {notifications?.length ? (
                    <div className="flex flex-col">
                      {(notifications ?? []).map((notification) => (
                        <NotificationListItem
                          key={notification.getUri()}
                          notification={notification}
                          reloadNotifications={loadNotifications}
                        />
                      ))}
                    </div>
                  ) : (
                    <p className="p-4 text-slate-700">
                      {t("editor.notifications.noNotifications")}
                    </p>
                  )}
                </>
              )}
            </div>
          </Popover.Panel>
        </Transition>,
        document.body
      )}
    </Popover>
  );
};

export default NotificationsContent;
