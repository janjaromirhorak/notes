import React, {
  MouseEventHandler,
  useCallback,
  useEffect,
  useMemo,
} from "react";
import { Link } from "react-router-dom";
import { useSafeState } from "../../hooks/useSafeState";

import { TrashIcon } from "@heroicons/react/24/outline";
import { useTranslation } from "react-i18next";
import { RemoteNotification, uriToUid } from "../../services";
import formatDateTime from "../../utils/formatDateTime";
import ProfilePicture from "../ControlPanel/TopBar/ProfilePicture";
import useRemoteResourceFactory from "../SessionProvider/useRemoteResourceFactory";
import Tooltip from "../Tooltip";

/** Props for the NotificationListItem component */
type NotificationListItemProps = {
  /** The notification */
  notification: RemoteNotification;
  /** Function to reload notifications */
  reloadNotifications: () => Promise<void>;
};

type NotificationData = {
  actorUri?: string;
  actorName?: string;
  noteName?: string;
  noteUri?: string;
  readStatus?: boolean;
  summary?: string;
  title?: string;
  published?: Date;
};

const ConditionalLink = ({
  children,
  className,
  to,
}: {
  children: React.ReactNode;
  className: string;
  to?: string;
}) =>
  to ? (
    <Link to={to} className={className}>
      {children}
    </Link>
  ) : (
    <div className={className}>{children}</div>
  );

/**
 * A component representing a notification item in the notification list
 *
 * @param {NotificationListItemProps} props
 * @category Components
 */
const NotificationListItem: React.FC<NotificationListItemProps> = ({
  notification,
  reloadNotifications,
}: NotificationListItemProps) => {
  const { t } = useTranslation();
  const factory = useRemoteResourceFactory();
  const [isLoading, setIsLoading] = useSafeState<boolean>(true);
  const [isDeleting, setIsDeleting] = useSafeState<boolean>(false);
  const [notificationData, setNotificationData] =
    useSafeState<NotificationData>();

  const loadNotification = useCallback(async () => {
    setIsLoading(true);
    const [
      [actorUri, actorName],
      [noteUri, noteName],
      readStatus,
      summary,
      title,
      published,
    ] = await Promise.all([
      (async () => {
        const actorUri = await notification.getActorUri();
        const actorName = actorUri
          ? await factory.getProfile(actorUri).getName()
          : undefined;
        return [actorUri, actorName ?? "Unknown user"];
      })(),
      (async () => {
        const objectUri = await notification.getObjectUri();
        let noteName: string | undefined = undefined;
        if (objectUri) {
          try {
            noteName = await factory.getNote(objectUri).getTitle();
          } catch (e) {}
        }
        return [objectUri, noteName || "Unknown note"];
      })(),
      notification.getReadStatus(),
      notification.getSummary(),
      notification.getTitle(),
      notification.getPublished(),
    ]);

    setNotificationData({
      actorUri,
      actorName,
      noteUri,
      noteName,
      summary,
      title,
      readStatus,
      published,
    });
    setIsLoading(false);
  }, [notification, setNotificationData, setIsLoading]);

  // mark notification as read
  useEffect(() => {
    if (!notificationData?.readStatus) {
      notification.markAsRead();
    }
  }, [notificationData?.readStatus, notification]);

  useEffect(() => {
    loadNotification();
  }, [notification, loadNotification]);

  const deleteNotification: MouseEventHandler<HTMLButtonElement> = useCallback(
    async (e) => {
      setIsDeleting(true);
      e.preventDefault();
      factory.deleteFromCache(
        factory.getIdentifier(notification.getUri(), "RemoteNotification")
      );
      await notification.deleteRemoteFile();
      await reloadNotifications();
      setIsDeleting(false);
    },
    [notification, reloadNotifications]
  );

  const notificationText: string = useMemo(() => {
    if (notificationData?.summary) {
      return notificationData.summary;
    }

    return t("editor.notifications.defaultSummary", {
      name: notificationData?.actorName,
      note: notificationData?.noteName,
    });
  }, [
    notificationData?.summary,
    notificationData?.actorName,
    notificationData?.noteName,
    t,
  ]);

  if (!isLoading && !notificationData?.noteUri) {
    return null;
  }

  return (
    <ConditionalLink
      to={
        notificationData?.noteUri
          ? `/note/${uriToUid(notificationData.noteUri)}`
          : undefined
      }
      className="block p-4 hover:bg-slate-100"
    >
      <div className={`flex gap-3 text-sm ${isLoading ? "animate-pulse" : ""}`}>
        {isLoading ? (
          <div className="h-8 w-8 rounded-full bg-slate-300"></div>
        ) : (
          <ProfilePicture
            webId={notificationData?.actorUri}
            className="shrink-0"
          />
        )}
        <div className="flex flex-1 flex-col gap-1">
          {isLoading ? (
            <>
              <div className="w-32 bg-slate-300">&nbsp;</div>
              <div className="w-72 bg-slate-300">&nbsp;</div>
            </>
          ) : (
            <>
              <div className="flex items-baseline gap-3">
                {notificationData?.title && (
                  <div
                    className={`font-semibold ${
                      notificationData?.readStatus
                        ? "text-slate-700"
                        : "text-slate-950"
                    }`}
                  >
                    {notificationData.title}
                  </div>
                )}
                {notificationData?.published && (
                  <div
                    className={`text-xs ${
                      notificationData?.readStatus
                        ? "text-slate-500"
                        : "text-slate-700"
                    }`}
                  >
                    {formatDateTime(notificationData.published)}
                  </div>
                )}
              </div>
              <div
                className={
                  notificationData?.readStatus
                    ? "text-slate-500"
                    : "text-slate-700"
                }
              >
                {notificationText}
              </div>
            </>
          )}
        </div>
        <Tooltip
          content={t("editor.notifications.deleteNotification")}
          className="block"
        >
          <button
            onClick={deleteNotification}
            className={`block h-7 w-7 rounded-md text-red-600 transition-opacity hover:bg-red-200 ${
              isDeleting ? "p-1.5" : "p-1"
            } ${isLoading ? "opacity-0" : "opacity-100"}`}
            disabled={isDeleting}
          >
            {isDeleting ? (
              <div className="h-full w-full animate-spin rounded-full border-2 border-red-700 border-t-transparent" />
            ) : (
              <TrashIcon className="h-5 w-5" />
            )}
          </button>
        </Tooltip>
      </div>
    </ConditionalLink>
  );
};

export default NotificationListItem;
