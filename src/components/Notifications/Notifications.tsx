import React from "react";
import NotificationsContent from "./NotificationsContent";

type NotificationsProps = {
  inboxUrl: string;
};

/**
 * A notification button with a badge and list of notifications
 *
 * @category Components
 */
const Notifications: React.FC<NotificationsProps> = ({
  inboxUrl,
}: NotificationsProps) => {
  return inboxUrl ? <NotificationsContent inboxUrl={inboxUrl} /> : null;
};

export default Notifications;
