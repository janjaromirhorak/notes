import React, { MouseEventHandler, PropsWithChildren, forwardRef } from "react";

/** Common props for both Button and ButtonLink component */
export type BaseButtonProps = {
  /**
   * additional classnames
   */
  className?: string;
  /** button theme */
  theme?: "primary" | "danger" | "secondary" | "secondaryDanger";
  /** icon prefix */
  leftIcon?: React.ReactNode;
  /** button title - will be rendered in a `title` attribute */
  title?: string;
  ref?: any;
  "aria-label"?: string;
};

/**
 * Button props
 * @extends {BaseButtonProps}
 */
type ButtonProps = BaseButtonProps & {
  /** onClick callback */
  onClick?: MouseEventHandler<HTMLButtonElement>;
  /** when the button is disabled, it is rendered differently and cannot be clicked */
  isDisabled?: boolean;
  /** when the button is loading, it is rendered differently and cannot be clicked */
  isLoading?: boolean;
};

const colorThemes: Record<NonNullable<ButtonProps["theme"]>, string> = {
  primary:
    "bg-indigo-600 text-white hover:bg-indigo-500 disabled:hover:bg-indigo-600",
  danger:
    "bg-red-600 text-white hover:bg-red-500 ring-red-100 disabled:hover:bg-red-600",
  secondary:
    "bg-white text-gray-900 ring-gray-300 hover:bg-gray-50 disabled:hover:bg-white",
  secondaryDanger:
    "bg-white text-red-600 ring-gray-300 hover:bg-gray-50 disabled:hover:bg-white",
};

const loadingColorThemes: Record<NonNullable<ButtonProps["theme"]>, string> = {
  primary: "bg-indigo-600 text-white",
  danger: "bg-red-600 text-white  ring-red-100",
  secondary: "bg-white text-gray-900 ring-gray-300 ",
  secondaryDanger: "bg-white text-red-600 ring-gray-300 ",
};

const loaderColorThemes: Record<NonNullable<ButtonProps["theme"]>, string> = {
  primary: "border-white border-r-white/30",
  danger: "border-white border-r-white/30",
  secondary: "border-indigo-600 border-r-indigo-600/30",
  secondaryDanger: "border-red-600 border-r-red-600/30",
};

export const getClassNames = (theme: ButtonProps["theme"] = "primary") =>
  `flex relative gap-2 rounded-md px-2.5 py-1.5 text-sm font-semibold shadow-sm ring-1 ring-inset focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 transition-transform active:scale-[0.99] active:translate-y-0.5 disabled:opacity-70 disabled:active:scale-1 disabled:active:translate-y-0 ${colorThemes[theme]}`;

/**
 * Styled native button component
 *
 * @param {ButtonProps}
 * @category Components
 */
const Button: React.FC<PropsWithChildren<ButtonProps>> = forwardRef(
  (
    {
      children,
      leftIcon,
      className = "",
      theme = "primary",
      isLoading,
      isDisabled,
      title,
      onClick,
      "aria-label": ariaLabel,
    },
    ref
  ) => {
    return (
      <button
        className={`${getClassNames(theme)} ${className}`}
        title={title}
        onClick={onClick}
        disabled={isDisabled}
        // @ts-expect-error
        ref={ref}
        aria-label={ariaLabel}
      >
        <span className={`flex w-full items-center justify-center gap-2`}>
          {leftIcon ? <span className="w-4">{leftIcon}</span> : null}
          {children}
        </span>
        <span
          className={`absolute bottom-0 left-0 right-0 top-0 flex items-center justify-center rounded-md ring-1 ring-inset transition-opacity ${
            loadingColorThemes[theme]
          } ${isLoading ? "opacity-100" : "opacity-0"}`}
        >
          <span
            className={`block h-4 w-4 origin-center animate-spin rounded-full border-2 ${loaderColorThemes[theme]}`}
          />
        </span>
      </button>
    );
  }
);

export default Button;
