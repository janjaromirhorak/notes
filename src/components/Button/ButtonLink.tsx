import React, { PropsWithChildren } from "react";
import { Link } from "react-router-dom";
import { BaseButtonProps, getClassNames } from "./Button";

/**
 * Props of an external link rendered as a native anchor element
 * @extends {BaseButtonProps}
 */
type ExternalButtonLinkProps = BaseButtonProps & {
  /** target URL */
  href: string;
  /** native `rel` attribute */
  rel?: string;
  /** native `target` attribute */
  target?: string;
  /** false for internal link, true for external link */
  isExternal: true;
};

/**
 * Props of an internal link handled by react-router
 * @extends {BaseButtonProps}
 */
type InternalButtonLinkProps = BaseButtonProps & {
  /** target route */
  to: string;
  /** false for internal link, true for external link */
  isExternal?: false;
};

/** Button props
 * @extends {ExternalButtonLinkProps}
 * @extends {InternalButtonLinkProps}
 */
type ButtonLinkProps = ExternalButtonLinkProps | InternalButtonLinkProps;

/**
 * Button serving as a link. Based on the isExternal property either react-router Link component, or a native anchor component is used.
 *
 * @param {ButtonLinkProps}
 * @category Components
 */
const ButtonLink: React.FC<PropsWithChildren<ButtonLinkProps>> = ({
  children,
  leftIcon,
  className = "",
  theme = "primary",
  isExternal,
  ...props
}) => {
  if (isExternal) {
    return (
      <a className={`${getClassNames(theme)} ${className}`} {...props}>
        {leftIcon ? <span className="w-4">{leftIcon}</span> : null}
        {children}
      </a>
    );
  }
  return (
    <Link
      className={`${getClassNames(theme)} ${className}`}
      {...(props as InternalButtonLinkProps)}
    >
      {leftIcon ? <span className="w-4">{leftIcon}</span> : null}
      {children}
    </Link>
  );
};

export default ButtonLink;
