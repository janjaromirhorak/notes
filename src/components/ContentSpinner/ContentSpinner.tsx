import React from "react";

/**
 * Renders a large spinner, indicating a content is being loaded
 *
 * @category Components
 */
const ContentSpinner: React.FC<{}> = () => {
  return (
    <div className="flex items-center justify-center py-52">
      <div className="h-10 w-10 animate-spin rounded-full border-4 border-indigo-600 border-r-indigo-600/30"></div>
    </div>
  );
};

export default ContentSpinner;
