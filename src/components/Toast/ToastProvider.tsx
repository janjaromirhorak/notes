import React, { PropsWithChildren, useCallback, useEffect } from "react";
import { useSafeState } from "../../hooks/useSafeState";
import Toast, { ToastMessage } from "./Toast";
import ToastContext, { ToastContextType } from "./ToastContext";

type ToastMessageWithMetadata = ToastMessage & { id: number; display: boolean };

const defaultTimeout = 5000;
const transitionTime = 300;

const ToastProvider: React.FC<PropsWithChildren<{}>> = ({ children }) => {
  const [_, setNextToastId] = useSafeState<number>(0);
  const [toastMessages, setToastMessages] = useSafeState<
    ToastMessageWithMetadata[]
  >([]);
  const [timeouts, setTimeouts] = useSafeState<number[]>([]);

  const addTimeout = useCallback(
    (handle: () => void, timeout: number) =>
      setTimeouts((timeouts) => [
        ...(timeouts ?? []),
        window.setTimeout(handle, timeout),
      ]),
    [setTimeouts]
  );

  const closeToastMessage = useCallback(
    (id: number) => {
      // schedule display:false
      setToastMessages((messages) =>
        (messages ?? []).map((message) =>
          message.id === id ? { ...message, display: false } : message
        )
      );

      // schedule unmount
      addTimeout(() => {
        setToastMessages((messages) =>
          (messages ?? []).filter((message) => message.id !== id)
        );
      }, transitionTime + 100);
    },
    [setToastMessages]
  );

  const addToastMessage = useCallback<ToastContextType["addToastMessage"]>(
    (message, timeout) => {
      setNextToastId((id) => {
        const currentId = id ?? 0;

        // create the toast message
        setToastMessages((messages) => [
          ...(messages ?? []),
          {
            ...message,
            display: false,
            id: currentId,
          },
        ]);

        // add timeout for showing the toast message
        addTimeout(() => {
          // set display:true
          setToastMessages((messages) =>
            (messages ?? []).map((message) =>
              message.id === currentId ? { ...message, display: true } : message
            )
          );

          // schedule display:false
          addTimeout(() => {
            closeToastMessage(currentId);
          }, (timeout ?? defaultTimeout) + transitionTime + 10);
        }, 1);

        return currentId + 1;
      });
    },
    [setNextToastId, setToastMessages, setTimeouts]
  );

  useEffect(() => {
    return () => {
      (timeouts ?? []).map(window.clearTimeout);
    };
  }, []);

  return (
    <>
      <ToastContext.Provider value={{ addToastMessage }}>
        {children}
      </ToastContext.Provider>
      {(toastMessages ?? []).length > 0 && (
        <div
          aria-live="assertive"
          className="pointer-events-none fixed inset-0 z-[200] flex flex-col items-end justify-end gap-4 px-4 py-6 sm:items-start sm:p-6"
        >
          {(toastMessages ?? []).map(({ id, ...props }) => (
            <Toast key={id} {...props} close={() => closeToastMessage(id)} />
          ))}
        </div>
      )}
    </>
  );
};

export default ToastProvider;
