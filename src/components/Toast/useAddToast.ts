import { useContext } from "react";
import ToastContext from "./ToastContext";

const useAddToast = () => useContext(ToastContext).addToastMessage;
export default useAddToast;
