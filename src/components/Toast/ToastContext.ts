import { createContext } from "react";
import { ToastMessage } from "./Toast";

export type ToastContextType = {
  addToastMessage: (message: ToastMessage, timeout?: number) => void;
};

const ToastContext = createContext<ToastContextType>({
  addToastMessage: () => {},
});

export default ToastContext;
