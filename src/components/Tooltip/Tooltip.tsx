import React, { PropsWithChildren, useMemo } from "react";
import { Tooltip as ReactTooltip } from "react-tooltip";

type TooltipProps = {
  className?: string;
  content?: React.ReactNode;
  placement?: "top" | "right" | "bottom" | "left";
};

const Tooltip: React.FC<PropsWithChildren<TooltipProps>> = ({
  children,
  className,
  content,
  placement = "top",
}) => {
  const id = useMemo(() => {
    return Math.round(Math.random() * 1000000).toString();
  }, []);

  if (!content) {
    return <span className={className}>{children}</span>;
  }

  return (
    <>
      <span
        className={className}
        data-tooltip-id={id}
        data-tooltip-place={placement}
      >
        {children}
      </span>
      <ReactTooltip id={id} children={content} />
    </>
  );
};

export default Tooltip;
