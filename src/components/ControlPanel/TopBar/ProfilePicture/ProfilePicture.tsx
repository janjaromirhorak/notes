import React, { useEffect } from "react";
import { useSafeState } from "../../../../hooks/useSafeState";
import useRemoteResourceFactory from "../../../SessionProvider/useRemoteResourceFactory";
import Tooltip from "../../../Tooltip";

/**
 * Props for the ProfilePicture component
 */
type ProfilePictureProps = {
  /** WebID of the user */
  webId?: string;
  /** additional classNames */
  className?: string;
};

/**
 * Profile picture component
 * @param {ProfilePictureProps}
 * @category Components
 */
const ProfilePicture: React.FC<ProfilePictureProps> = ({
  webId,
  className = "",
}: ProfilePictureProps) => {
  const [name, setName] = useSafeState<string | undefined>(undefined);
  const [src, setSrc] = useSafeState<string | undefined>(undefined);
  const [isLoading, setIsLoading] = useSafeState<boolean>(true);
  const factory = useRemoteResourceFactory();

  useEffect(() => {
    setIsLoading(true);
    if (webId) {
      const profile = factory.getProfile(webId);
      Promise.all([profile.getName(), profile.getPicture()]).then(
        ([name, picture]) => {
          setName(name);
          setSrc(picture);
          setIsLoading(false);
        }
      );
    } else {
      setName(undefined);
      setSrc(undefined);
      setIsLoading(false);
    }
  }, [webId, setName, setSrc]);

  const initials = (name ?? "?")
    .split(" ")
    .map((word) => word.trim().substring(0, 1))
    .filter((word) => word.length)
    .slice(0, 2)
    .join("");

  return (
    <Tooltip
      className={`relative flex h-8 w-8 items-center justify-center overflow-hidden rounded-full text-sm font-semibold text-black ${
        isLoading
          ? "animate-pulse bg-slate-300"
          : webId
          ? "bg-indigo-300"
          : "bg-slate-300"
      } ${className}`}
      content={name}
    >
      {isLoading ? null : src ? (
        <img
          src={src}
          alt={name}
          className="absolute bottom-0 left-0 right-0 top-0 object-cover"
        />
      ) : (
        initials
      )}
    </Tooltip>
  );
};

export default ProfilePicture;
