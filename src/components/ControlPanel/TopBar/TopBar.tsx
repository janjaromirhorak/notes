import React, { useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useSafeState } from "../../../hooks/useSafeState";

import LogOutButton from "../../LogOutButton";
import Notifications from "../../Notifications";

import { ButtonLink } from "../../Button";
import { useWebId } from "../../SessionProvider";
import useRemoteResourceFactory from "../../SessionProvider/useRemoteResourceFactory";
import useAddToast from "../../Toast/useAddToast";
import ProfilePicture from "./ProfilePicture";

/**
 * Top application bar component. Renders notifications and other user-specific info.
 *
 * @category Components
 */
const TopBar: React.FC<{}> = () => {
  const { t } = useTranslation();
  const addToast = useAddToast();
  const factory = useRemoteResourceFactory();

  const webId: string | undefined = useWebId();
  const [inboxUrl, setInboxUrl] = useSafeState<string | undefined>(undefined);

  /**
   * Looks for all of the inbox containers in the pod and sets inboxes state
   */
  const discoverInbox = useCallback(async () => {
    if (webId) {
      try {
        /**
         * Get user's global inbox path from pod.
         */
        const newInboxUrl = await factory.getProfile(webId).getInbox();

        /**
         * If user doesn't have an inbox in the pod, show an error and link to
         * know how fix it.
         */
        if (!newInboxUrl) {
          addToast(
            {
              message: t("noInboxUser.message"),
              kind: "danger",
            },
            9000
          );
        }

        setInboxUrl(newInboxUrl);
      } catch (error) {
        /**
         * Show general errors
         */
        addToast(
          {
            message: t("navBar.notifications.fetchingError"),
            description:
              error &&
              typeof error === "object" &&
              "message" in error &&
              typeof error.message === "string"
                ? error.message
                : undefined,
            kind: "danger",
          },
          9000
        );
      }
    }
  }, [webId, t, addToast, setInboxUrl]);

  useEffect(() => {
    if (webId) {
      discoverInbox();
    }
  }, [webId, discoverInbox]);

  return (
    <div className="flex w-full items-center gap-3">
      <ProfilePicture webId={webId} className="mr-auto" />
      {inboxUrl ? <Notifications inboxUrl={inboxUrl} /> : null}
      {webId ? (
        <LogOutButton />
      ) : (
        <ButtonLink to={"/login"}>{t("logInButton.text")}</ButtonLink>
      )}
    </div>
  );
};

export default TopBar;
