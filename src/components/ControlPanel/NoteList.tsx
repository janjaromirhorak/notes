import { PlusIcon } from "@heroicons/react/20/solid";
import React, { useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { useParams } from "react-router-dom";

import { uidToUri, uriToUid } from "../../services";

import { useSafeState } from "../../hooks/useSafeState";
import Button from "../Button";
import useNoteListHelpers from "../SessionProvider/useNoteListHelpers";
import NoteListItem from "./NoteListItem";

/**
 * Renders a list of users notes
 *
 * @category Components
 */
const NoteList: React.FC<{}> = () => {
  const { t } = useTranslation();
  const [isAddingToList, setIsAddingToList] = useSafeState<boolean>(false);

  const { uid } = useParams<{ uid?: string }>();

  const { refetchNoteList, noteList, noteMetadata } = useNoteListHelpers();

  const onAddNoteToList = useCallback(
    async (uri: string) => {
      if (noteList) {
        try {
          setIsAddingToList(true);
          await noteList.addNoteToList(uri);
          await refetchNoteList();
        } finally {
          setIsAddingToList(false);
        }
      }
    },
    [noteList, refetchNoteList]
  );

  const removeNoteFromList = useCallback(
    async (uri: string) => {
      if (noteList) {
        await noteList.removeNoteFromList(uri);
        await refetchNoteList();
      }
    },
    [noteList, refetchNoteList]
  );

  const isCurrentNoteInTheList: boolean = useMemo(() => {
    return !!(
      uid &&
      noteMetadata &&
      noteMetadata.some(({ uri }) => uriToUid(uri) === uid)
    );
  }, [noteMetadata, uid]);

  if (noteMetadata === undefined) {
    return (
      <div className="mb-4 flex w-full animate-pulse items-center justify-center rounded-md bg-slate-200 p-3 py-12 text-sm font-semibold text-slate-600">
        Loading data from Solid...
      </div>
    );
  }

  return (
    <div className="space-y-3">
      {uid && !isCurrentNoteInTheList && noteList ? (
        <div className="rounded-md bg-slate-200 p-3">
          <p className="mb-3 text-sm">{t("noteList.notInYourNotes")}</p>
          <Button
            isLoading={isAddingToList}
            leftIcon={<PlusIcon />}
            onClick={() => onAddNoteToList(uidToUri(uid))}
            className={
              import.meta.env.VITE_IUM_CLASS_NAMES
                ? "testing-add-current-note-to-list mx-auto"
                : "mx-auto"
            }
          >
            {t("noteList.addCurrentNoteToList")}
          </Button>
        </div>
      ) : null}
      {noteMetadata.map((noteItem) => (
        <NoteListItem
          key={noteItem.uri}
          noteMetadata={noteItem}
          isActive={uriToUid(noteItem.uri) === uid}
          removeNoteFromList={async () => removeNoteFromList(noteItem.uri)}
        />
      ))}
    </div>
  );
};

export default NoteList;
