import { createContext } from "react";

export type ControlPanelContextType = {
  active: boolean;
  setActive: (active: boolean) => void;
};

const ControlPanelContext = createContext<ControlPanelContextType>({
  active: false,
  setActive: () => {},
});

export default ControlPanelContext;
