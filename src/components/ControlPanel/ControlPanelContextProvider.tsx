import React, { memo } from "react";
import { useSafeState } from "../../hooks/useSafeState";
import ControlPanelContext from "./ControlPanelContext";

type Props = {
  children?: React.ReactNode;
};

const ControlPanelContextProvider: React.FC<Props> = memo<Props>(
  ({ children }: Props) => {
    const [active, setActive] = useSafeState<boolean>(false);

    return (
      <ControlPanelContext.Provider
        value={{
          active: !!active,
          setActive,
        }}
      >
        {children}
      </ControlPanelContext.Provider>
    );
  }
);

export default ControlPanelContextProvider;
