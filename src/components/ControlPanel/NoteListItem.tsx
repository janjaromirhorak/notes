import { XMarkIcon } from "@heroicons/react/24/outline";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { useSafeState } from "../../hooks/useSafeState";
import { uriToUid } from "../../services";
import { NoteMetadata } from "../SessionProvider/SessionContext";
import Tooltip from "../Tooltip";

type NoteListItemProps = {
  noteMetadata: NoteMetadata;
  removeNoteFromList: () => Promise<void>;
  isActive?: boolean;
};

const backgroundColors = {
  active: "bg-slate-200 hover:bg-slate-300 focus-visible:bg-slate-300",
  inactive: "bg-white hover:bg-slate-200 focus-visible:bg-slate-200",
};

const closeButtonBackgroundColors = {
  active: "hover:bg-slate-400 focus-visible:bg-slate-400",
  inactive: "hover:bg-slate-300 focus-visible:bg-slate-300",
};

const NoteListItem: React.FC<NoteListItemProps> = ({
  noteMetadata,
  removeNoteFromList,
  isActive = false,
}) => {
  const { t } = useTranslation();
  const [isRemovingFromNoteList, setIsRemovingFromNoteList] =
    useSafeState<boolean>(false);
  const linkUid = uriToUid(noteMetadata.uri);
  return (
    <Link
      to={`/note/${linkUid}`}
      className={`group flex items-start rounded-md p-3 text-sm ${
        backgroundColors[isActive ? "active" : "inactive"]
      }`}
    >
      <div className="w-full space-y-2">
        {noteMetadata.title ? (
          <h4 className="font-bold">{noteMetadata.title}</h4>
        ) : (
          <h4 className="font-bold italic">Untitled</h4>
        )}

        <p className="text-xs text-slate-600">{noteMetadata.uri}</p>
      </div>
      <button
        className="block"
        onClick={async (e) => {
          e.preventDefault();
          e.stopPropagation();
          setIsRemovingFromNoteList(true);
          try {
            await removeNoteFromList();
          } catch (e) {}
          setIsRemovingFromNoteList(false);
        }}
      >
        <Tooltip
          content={t("noteList.removeFromList")}
          className={`block rounded-sm p-1 ${
            closeButtonBackgroundColors[isActive ? "active" : "inactive"]
          }`}
        >
          {isRemovingFromNoteList ? (
            <div className="block h-4 w-4 animate-spin rounded-full border-2 border-slate-700 border-t-slate-700/40" />
          ) : (
            <XMarkIcon className="block h-4 w-4 stroke-2" />
          )}
        </Tooltip>
      </button>
    </Link>
  );
};

export default NoteListItem;
