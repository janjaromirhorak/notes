import {
  MagnifyingGlassIcon,
  MagnifyingGlassPlusIcon,
} from "@heroicons/react/20/solid";
import React, { memo } from "react";
import { useTranslation } from "react-i18next";
import { useIsLoggedIn } from "../SessionProvider";

import NoteList from "./NoteList";
import NotLoggedInNoteList from "./NotLoggedInNoteList";

import { ButtonLink } from "../Button";

/**
 * Renders a control panel, used for navigating the app
 *
 * @category Components
 */
const ControlPanel: React.FC<{}> = memo<{}>(() => {
  const isLoggedIn = useIsLoggedIn();
  const { t } = useTranslation();

  return (
    <div className="flex flex-col items-center space-y-3 pb-6">
      {isLoggedIn ? <NoteList /> : <NotLoggedInNoteList />}
      <ButtonLink
        to="/open"
        className={
          import.meta.env.VITE_IUM_CLASS_NAMES
            ? "testing-open-pod-browser-button"
            : ""
        }
        leftIcon={
          isLoggedIn ? (
            <MagnifyingGlassPlusIcon className="pt-[0.15rem]" />
          ) : (
            <MagnifyingGlassIcon className="pt-[0.15rem]" />
          )
        }
      >
        {isLoggedIn
          ? t("controlPanel.findOrCreateNote")
          : t("controlPanel.findNote")}
      </ButtonLink>
    </div>
  );
});

export default ControlPanel;
