import React from "react";
import { useTranslation } from "react-i18next";
import Link from "../Link";

/**
 * Renders a placeholder for the note list for an unauthenticated user
 *
 * @category Components
 */
const NotLoggedInNoteList: React.FC<{}> = () => {
  const { t } = useTranslation();

  return (
    <p className="w-full text-sm">
      {t("notLoggedInNoteList.loginText1")}{" "}
      <Link
        to={"/login"}
        className="font-semibold text-indigo-600 hover:underline"
      >
        {t("notLoggedInNoteList.loginButtonText")}
      </Link>{" "}
      {t("notLoggedInNoteList.loginText2")}
    </p>
  );
};

export default NotLoggedInNoteList;
