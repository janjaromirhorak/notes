import React from "react";
import { Trans, useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

/**
 * A component representing the content for a logged-in user on the home page
 * @category Components
 */
const LoggedInContent: React.FC<{}> = () => {
  const { t } = useTranslation();

  return (
    <p className="mb-4">
      <Trans
        t={t}
        i18nKey="homePage.loggedInText"
        components={{
          l: <Link to={"/open"} className="text-indigo-700 hover:underline" />,
        }}
      />
    </p>
  );
};

export default LoggedInContent;
