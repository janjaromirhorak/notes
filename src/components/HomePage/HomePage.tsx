import React from "react";

import { ReactComponent as GitlabIcon } from "../../assets/icons/gitlab-icon.svg";

import { Trans, useTranslation } from "react-i18next";
import { ButtonLink } from "../Button";
import PageHeading from "../PageHeading";
import { useIsLoggedIn } from "../SessionProvider";
import LoggedInContent from "./LoggedInContent";
import NotLoggedInContent from "./NotLoggedInContent";
import SolidIcon from "./SolidIcon";

/**
 * Content of the Home page
 *
 * @category Components
 */
const HomePage: React.FC<{}> = () => {
  const { t } = useTranslation();
  const isLoggedIn = useIsLoggedIn();

  return (
    <>
      <PageHeading>{t("homePage.title")}</PageHeading>

      <p className="mb-4">
        <Trans
          t={t}
          i18nKey="homePage.perex"
          components={{
            l: (
              <a
                href={"https://solidproject.org/"}
                className="text-indigo-700 hover:underline"
              />
            ),
          }}
        />
      </p>

      {isLoggedIn ? <LoggedInContent /> : <NotLoggedInContent />}

      <div className="mt-6 flex items-center justify-center gap-6">
        <ButtonLink
          theme="secondary"
          href="https://gitlab.com/janjaromirhorak/notes"
          isExternal
          leftIcon={<GitlabIcon className="mt-[0.07rem] w-[18px]" />}
        >
          {t("homePage.sourceCodeButton")}
        </ButtonLink>

        <ButtonLink
          theme="secondary"
          className="flex items-stretch"
          href="https://solidproject.org/"
          isExternal
          leftIcon={<SolidIcon className="mt-[0.1rem]" />}
        >
          {t("homePage.solidButton")}
        </ButtonLink>
      </div>
    </>
  );
};

export default HomePage;
