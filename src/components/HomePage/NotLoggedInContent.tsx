import React from "react";
import { Trans, useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

/**
 * A component representing the content for a not logged-in user on the home page
 *
 * @category Components
 */
const NotLoggedInContent: React.FC<{}> = () => {
  const { t } = useTranslation();

  return (
    <p className="mb-4">
      <Trans
        t={t}
        i18nKey="homePage.loggedOutText"
        components={{
          l1: (
            <Link
              to={"/login"}
              color="teal.500"
              className="text-indigo-700 hover:underline"
            />
          ),
          l2: (
            <Link
              to={"/open"}
              color="teal.500"
              className="text-indigo-700 hover:underline"
            />
          ),
        }}
      />
    </p>
  );
};

export default NotLoggedInContent;
