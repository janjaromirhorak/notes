import solidIcon from "../../assets/icons/solid-logo.png";

const SolidIcon: React.FC<{ className?: string }> = ({ className }) => {
  return <img src={solidIcon} className={className} alt="" />;
};

export default SolidIcon;
