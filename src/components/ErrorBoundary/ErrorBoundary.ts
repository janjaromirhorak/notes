import React from "react";

type Props = {
  children: React.ReactNode;
  component: (error: String, info: Object) => React.ReactNode;
};

type State = {
  hasError: boolean;
  error: any;
  info: any;
};

/**
 * ErrorBoundary component to catch React component errors
 * You can use you own markup to show Error in your components
 *
 * @category Components
 */
class ErrorBoundary extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      hasError: false,
      error: null,
      info: null,
    };
  }

  // Catch error and update state to render custom error component
  componentDidCatch(error: any, info: any) {
    this.setState({
      hasError: true,
      error,
      info,
    });
  }

  render() {
    const { hasError, error, info } = this.state;
    const { component, children } = this.props;
    return hasError ? component(error, info) : children;
  }
}

export default ErrorBoundary;
