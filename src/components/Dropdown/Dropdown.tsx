import { Menu, Transition } from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/20/solid";
import { Fragment, useMemo } from "react";

/**
 * Represents an option in the dropdown
 */
export type DropdownOption = {
  /** Label of the option */
  label: string;
  /** Value of the option */
  value: string;
  /** The callback function to be called when the option is selected */
  onSelect?: () => void;
};

/**
 * Represents the props for the Dropdown component
 */
type DropdownProps = {
  /** The array of options for the dropdown */
  options: DropdownOption[];
  /** The currently active option */
  activeOption?: string;
  /** The default text to display in the dropdown */
  defaultText: string;
  /** The optional CSS class name for the dropdown */
  className?: string;
};

/**
 * A generic dropdown component
 *
 * @category Components
 */
const Dropdown: React.FC<DropdownProps> = ({
  options,
  activeOption,
  defaultText,
  className = "",
}) => {
  const menuButtonText = useMemo<string>(() => {
    const activeOptionItem = options.find(
      ({ value }) => value === activeOption
    );
    return activeOptionItem ? activeOptionItem.label : defaultText;
  }, [activeOption, defaultText]);

  return (
    <Menu as="div" className={`relative inline-block text-left ${className}`}>
      <div>
        <Menu.Button className="inline-flex w-full justify-between gap-x-1.5 rounded-md bg-white px-3 py-2 text-left text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50">
          <span className="overflow-hidden text-ellipsis whitespace-nowrap">
            {menuButtonText}
          </span>
          <ChevronDownIcon
            className="-mr-1 h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
        </Menu.Button>
      </div>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute left-0 z-10 mt-2 w-72 origin-top-left rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
          <div className="py-1">
            {options.map(({ label, value, onSelect }) => (
              <Menu.Item key={value}>
                <button
                  className={`block w-full px-4 py-2 text-left text-sm ${
                    value === activeOption
                      ? "bg-gray-100 text-gray-900"
                      : "text-gray-700"
                  }`}
                  onClick={onSelect}
                >
                  {label}
                </button>
              </Menu.Item>
            ))}
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export default Dropdown;
