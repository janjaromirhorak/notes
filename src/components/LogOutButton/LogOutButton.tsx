import { logout } from "@inrupt/solid-client-authn-browser";
import React, { useCallback, useContext } from "react";

import { useTranslation } from "react-i18next";
import Button from "../Button";
import SessionContext from "../SessionProvider/SessionContext";
import useAddToast from "../Toast/useAddToast";

/**
 * Logs out the currently logged in user. Expects the user to be logged in.
 *
 * @category Components
 */
const LogOutButton: React.FC<{}> = () => {
  const addToast = useAddToast();
  const { setSession } = useContext(SessionContext);
  const { t } = useTranslation();

  const logOut: () => Promise<void> = useCallback(async () => {
    try {
      if (localStorage) {
        localStorage.clear();
      }
      await logout();
      if (setSession) {
        setSession(undefined);
      }
    } catch (error) {
      addToast(
        {
          message:
            error &&
            typeof error === "object" &&
            "message" in error &&
            typeof error.message === "string"
              ? error.message
              : t("logoutButton.unexpectedError"),
          kind: "danger",
        },
        9000
      );
    }
  }, [setSession, addToast, t]);

  return (
    <Button onClick={logOut} theme="secondary">
      {t("logoutButton.text")}
    </Button>
  );
};

export default LogOutButton;
