import { getDefaultSession, login } from "@inrupt/solid-client-authn-browser";
import React, { useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useIsLoggedIn } from "../../components/SessionProvider";
import { useSafeState } from "../../hooks/useSafeState";
import Button, { ButtonLink } from "../Button";
import PageHeading from "../PageHeading";
import TextInput from "../TextInput";
import AuthorizeScreenshot from "./AuthorizeScreenshot";

/**
 * A form for the user to log in with
 *
 * @category Components
 */
const LoginForm: React.FC<{}> = () => {
  const navigate = useNavigate();
  const isLoggedIn = useIsLoggedIn();
  const { t } = useTranslation();

  const [buttonsDisabled, setButtonsDisabled] = useSafeState<boolean>(false);
  const [isWebIdButtonLoading, setIsWebIdButtonLoading] =
    useSafeState<boolean>(false);
  const [isInruptButtonLoading, setIsInruptButtonLoading] =
    useSafeState<boolean>(false);
  const [isSolidCommunityButtonLoading, setIsSolidCommunityButtonLoading] =
    useSafeState<boolean>(false);

  const [loginWebId, setLoginWebId] = useSafeState<string>("");
  const onLoginWebIdChange = useCallback(
    (e: unknown) => {
      // @ts-expect-error incomplete error typing
      setLoginWebId(e.target?.value);
    },
    [setLoginWebId]
  );

  useEffect(() => {
    if (isLoggedIn) {
      navigate("/");
    }
  }, [isLoggedIn, history]);

  const loginWithIssuer = useCallback(
    async (issuer: string) => {
      setButtonsDisabled(true);
      if (!getDefaultSession().info.isLoggedIn) {
        try {
          await login({
            oidcIssuer: issuer,
            redirectUrl: new URL("/login", window.location.href).toString(),
            clientName: "Notes",
          });
        } catch (e) {
          console.error(e);
        }
      }
      setButtonsDisabled(false);
    },
    [setButtonsDisabled]
  );

  const onWebIdLogin = useCallback(async () => {
    setIsWebIdButtonLoading(true);
    const issuer = new URL(loginWebId ?? "").origin;
    if (issuer) {
      await loginWithIssuer(issuer);
    }
    setIsWebIdButtonLoading(false);
  }, [loginWebId, loginWithIssuer, setIsWebIdButtonLoading]);

  const onKeyUp = useCallback(
    // @ts-expect-error incomplete error typing
    (e) => {
      if (e.key === "Enter") {
        onWebIdLogin();
      }
    },
    [onWebIdLogin]
  );

  const onInruptLogin = useCallback(async () => {
    setIsInruptButtonLoading(true);
    await loginWithIssuer("https://inrupt.net/");
    setIsInruptButtonLoading(false);
  }, [loginWithIssuer, setIsInruptButtonLoading]);

  const onSolidCommunityLogin = useCallback(async () => {
    setIsSolidCommunityButtonLoading(true);
    await loginWithIssuer("https://solidcommunity.net/");
    setIsSolidCommunityButtonLoading(false);
  }, [loginWithIssuer, setIsSolidCommunityButtonLoading]);

  return (
    <>
      <PageHeading>{t("loginForm.title")}</PageHeading>

      <div className="flex gap-2">
        <TextInput
          value={loginWebId}
          onChange={onLoginWebIdChange}
          onKeyUp={onKeyUp}
          placeholder={"https://example.com/card#me"}
        />
        <Button
          onClick={onWebIdLogin}
          isDisabled={buttonsDisabled || !loginWebId}
          isLoading={isWebIdButtonLoading}
          className="w-24"
        >
          {t("loginForm.submit")}
        </Button>
      </div>

      <h2 className="mb-4 mt-8 text-center text-xl font-semibold">
        {t("loginForm.selectProvider")}
      </h2>

      <div className="flex flex-wrap justify-center gap-3">
        <Button
          theme="secondary"
          onClick={onInruptLogin}
          isDisabled={buttonsDisabled}
          isLoading={isInruptButtonLoading}
        >
          inrupt.net
        </Button>
        <Button
          theme="secondary"
          onClick={onSolidCommunityLogin}
          isDisabled={buttonsDisabled}
          isLoading={isSolidCommunityButtonLoading}
        >
          solidcommunity.net
        </Button>
      </div>

      <h2 className="mb-4 mt-12 text-center text-2xl font-semibold">
        {t("loginForm.firstLogin.title")}
      </h2>

      <p className="mb-6">{t("loginForm.firstLogin.text1")}</p>

      <AuthorizeScreenshot />

      <h2 className="mb-4 mt-8 text-center text-2xl font-semibold">
        {t("loginForm.register.title")}
      </h2>

      <div className="flex flex-wrap justify-center gap-3">
        <ButtonLink
          theme="secondary"
          href={"https://inrupt.net/register"}
          isExternal
        >
          {t("loginForm.register.registerWith", {
            name: "inrupt.net",
          })}
        </ButtonLink>

        <ButtonLink
          theme="secondary"
          href={"https://solidcommunity.net/register"}
          isExternal
        >
          {t("loginForm.register.registerWith", {
            name: "solidcommunity.net",
          })}
        </ButtonLink>
      </div>
    </>
  );
};

export default LoginForm;
