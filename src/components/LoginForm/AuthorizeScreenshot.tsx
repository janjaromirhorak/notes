import React, { useMemo } from "react";
import { useTranslation } from "react-i18next";
import useAppUrl from "../../hooks/useAppUrl";

/**
 * A recreation of an authorization dialog used on Solid Pods to authorize an application
 *
 * @category Components
 */
const AuthorizeScreenshot: React.FC<{}> = () => {
  const { t } = useTranslation();
  const appUrl: string = useAppUrl();

  const permissions = useMemo(
    () => [
      t("loginForm.firstLogin.authorizeScreenshot.label1"),
      t("loginForm.firstLogin.authorizeScreenshot.label2"),
      t("loginForm.firstLogin.authorizeScreenshot.label3"),
      t("loginForm.firstLogin.authorizeScreenshot.label4"),
    ],
    [t]
  );

  return (
    <div className="relative">
      <div className="pointer-events-none space-y-2 rounded-md border border-[#bfbfbf] bg-white p-4 text-[0.9rem] shadow-lg [user-select:none]">
        <div>
          {t("loginForm.firstLogin.authorizeScreenshot.text1", {
            appUrl,
          })}
        </div>
        {permissions.map((permission, key) => (
          <div key={key}>
            <input type={"checkbox"} checked readOnly />
            <strong style={{ marginLeft: "0.3em" }}>{permission}</strong>
          </div>
        ))}
        <div className="flex gap-1">
          <div className="rounded-[4px] bg-[#337ab7] px-2 py-1 text-white">
            {t("loginForm.firstLogin.authorizeScreenshot.authorize")}
          </div>
          <div className="rounded-[4px] border border-[#bfbfbf] px-2 py-1">
            {t("loginForm.firstLogin.authorizeScreenshot.cancel")}
          </div>
        </div>
      </div>
      <div className="absolute bottom-0 left-0 right-0 top-0 flex flex-col items-center justify-center gap-2 rounded-md bg-slate-200 px-6 text-sm text-slate-900 opacity-0 transition-opacity duration-500 hover:opacity-95">
        <p>{t("loginForm.firstLogin.authorizeScreenshot.hoverMessage1")}</p>
        <p className="font-semibold">
          {t("loginForm.firstLogin.authorizeScreenshot.hoverMessage2")}
        </p>
      </div>
    </div>
  );
};

export default AuthorizeScreenshot;
