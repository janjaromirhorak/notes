import React, { PropsWithChildren } from "react";

/**
 * A component representing the page heading
 *
 * @param {React.PropsWithChildren<{}>} props
 * @category Components
 */
const PageHeading: React.FC<PropsWithChildren<{}>> = ({ children }) => (
  <h1 className="mb-6 mt-3 text-center text-3xl font-bold">{children}</h1>
);

export default PageHeading;
