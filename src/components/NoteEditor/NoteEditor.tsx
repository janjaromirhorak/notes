import React, { useCallback, useEffect, useMemo } from "react";
import { useWebId } from "../../components/SessionProvider";
import { useSafeState } from "../../hooks/useSafeState";

import { RemoteNote } from "../../services";
import { InfoBar, SavingIndicator } from "./components";

import ContentSpinner from "../../components/ContentSpinner";
import permissionManager from "../../utils/permissionManager";
import useSendNoteNotification, {
  NOTIFICATION_TYPE,
} from "./components/useSendNoteNotification";
import EditorSavingStatus from "./EditorSavingStatus";
import { noAccess, PermissionContextItem } from "./NotePermissionsContext";
import NotePermissionsContextProvider from "./NotePermissionsContextProvider";

import { Access } from "@inrupt/solid-client";
import { useTranslation } from "react-i18next";
import { BatchUpdate } from "../../services/RemoteResource/BatchUpdate";
import useNoteListHelpers from "../SessionProvider/useNoteListHelpers";
import useSetCustomTitle from "../SessionProvider/useSetCustomTitle";
import TextInput from "../TextInput";
import PermissionInfo from "./components/PermissionInfo";
import RichTextEditor from "./components/RichTextEditor/RichTextEditor";

type Props = {
  note: RemoteNote;
};

const savingCooldownSeconds = 1;

const noPermissions: Access = {
  read: false,
  write: false,
  append: false,
  control: false,
};

/**
 * The React Component of the note editor
 *
 * @category Components
 * @category Components
 */
const NoteEditor: React.FC<Props> = ({ note }: Props) => {
  const { t } = useTranslation();

  const webId = useWebId();
  const setCustomTitle = useSetCustomTitle();

  const uri = useMemo(() => {
    return note.getUri();
  }, [note]);
  const [noteFetched, setNoteFetched] = useSafeState(false);
  const [title, setTitle] = useSafeState("");
  const [content, setContent] = useSafeState("");
  const [initialContent, setInitialContent] = useSafeState<
    string | undefined
  >();
  const [richTextEdited, setRichTextEdited] = useSafeState<boolean>(false);
  const [savingStatus, setSavingStatus] = useSafeState(
    EditorSavingStatus.Loaded
  );
  const [notePermissions, setNotePermissions] = useSafeState<
    PermissionContextItem[] | undefined
  >(undefined);
  const [userPermissions, setUserPermissions] =
    useSafeState<Access>(noPermissions);
  const [publicPermissions, setPublicPermissions] =
    useSafeState<Access>(noPermissions);
  const [permissionsLoaded, setPermissionsLoaded] =
    useSafeState<boolean>(false);
  const [lastSavedDate, setLastSavedDate] = useSafeState<Date | undefined>();
  const { refetchNoteList } = useNoteListHelpers();
  const [saveData, setSaveData] = useSafeState<{
    content: string;
    title: string;
  }>();
  const [prevSaveData, setPrevSaveData] = useSafeState<{
    content: string;
    title: string;
  }>();

  const sendNoteNotification = useSendNoteNotification();

  const setAndSortPermissions = useCallback(
    (permissions: PermissionContextItem[] | undefined) => {
      if (permissions) {
        const newPermissions = permissions.sort((a, b) => {
          if (a?.webId && b?.webId) {
            if (a?.webId < b?.webId) {
              return -1;
            } else if (a?.webId > b?.webId) {
              return 1;
            }
            return 0;
          } else if (a?.webId) {
            return -1;
          } else if (b?.webId) {
            return 1;
          }

          return 0;
        });

        setNotePermissions(
          newPermissions.filter(
            (permission) =>
              permission.permissions.read ||
              permission.permissions.write ||
              permission.permissions.append ||
              permission.permissions.control
          )
        );
      } else {
        setNotePermissions(permissions);
      }
    },
    [setNotePermissions]
  );

  const isNotePublic: boolean = useMemo(() => {
    return !!publicPermissions?.read;
  }, [publicPermissions?.read]);

  useEffect(() => {
    if (note) {
      if (webId) {
        note.getUserPermissions(webId).then(setUserPermissions);
        note.getPublicPermissions().then(setPublicPermissions);
      } else {
        note.getPublicPermissions().then((permissions) => {
          setUserPermissions(permissions);
          setPublicPermissions(permissions);
        });
      }
    }
  }, [note, webId, setUserPermissions, setPublicPermissions]);

  const editingAllowed: boolean = useMemo(() => {
    return !!userPermissions?.write;
  }, [userPermissions]);

  // load the note from the Solid pod
  useEffect(() => {
    if (note) {
      note.getTitle().then(setTitle);
      note.getModified().then(setLastSavedDate);
      note.getContent().then((content) => {
        setContent(content);
        setNoteFetched(true);
        setInitialContent(content);
      });
    }
  }, [uri, webId, note, setTitle, setContent, setNoteFetched]);

  /**
   * Changes the title of the note.
   * Manages the state change and issues an update of the Solid Pod.
   */
  const onTitleChange = useCallback(
    ({ currentTarget }: { currentTarget: HTMLInputElement }) => {
      const title = currentTarget.value;

      setSaveData({ content: content ?? "", title });

      setTitle(title);
    },
    [setSaveData, content, setTitle]
  );

  // update page title
  useEffect(() => {
    setCustomTitle(title);
  }, [title, setCustomTitle]);

  const loadPermissions = useCallback(async () => {
    if (note) {
      try {
        const permissions = await permissionManager.getPermissionsInMaps(note);
        setAndSortPermissions(permissions);
      } catch (e) {
        // ignore
      }
    }
  }, [setAndSortPermissions, webId, note]);

  // debounced save
  useEffect(() => {
    if (
      savingStatus !== EditorSavingStatus.Saving &&
      saveData &&
      (saveData.content !== prevSaveData?.content ||
        saveData.title !== prevSaveData.title)
    ) {
      setSavingStatus(EditorSavingStatus.Changed);
      const timeout = window.setTimeout(async () => {
        try {
          setSavingStatus(EditorSavingStatus.Saving);

          const batch = new BatchUpdate();

          await note.setTitle(saveData.title ?? "", batch);
          await note.setContent(saveData.content ?? "", batch);
          const now = new Date();
          await note.setModified(now, batch);
          await note.runBatchUpdate(batch);

          setPrevSaveData(saveData);
          setSavingStatus(EditorSavingStatus.Saved);
          setLastSavedDate(now);
          await refetchNoteList();
        } catch (e) {
          // retry later (this might be a temporary error or a 409 conflict response)
          setSavingStatus(EditorSavingStatus.Changed);
        }
      }, savingCooldownSeconds * 1000);
      return () => {
        window.clearTimeout(timeout);
      };
    }
  }, [saveData, savingStatus, prevSaveData, setPrevSaveData]);

  const updateNotePermissions = useCallback(
    async (permissions: PermissionContextItem[]): Promise<void> => {
      if (note) {
        let changedPermissions = [];
        for (const permission of permissions) {
          let found = false;
          for (const oldPermission of notePermissions ?? []) {
            if (permission.webId === oldPermission.webId) {
              if (
                oldPermission.permissions.read !==
                  permission.permissions.read ||
                oldPermission.permissions.write !==
                  permission.permissions.write ||
                oldPermission.permissions.append !==
                  permission.permissions.append ||
                oldPermission.permissions.control !==
                  permission.permissions.control
              ) {
                changedPermissions.push(permission);
              }
              found = true;
              break;
            }
          }
          if (!found) {
            changedPermissions.push(permission);
          }
        }

        setAndSortPermissions(permissions);

        if (changedPermissions.length) {
          await permissionManager.setPermissionsForResource(
            note.getFileUri(),
            permissions
          );

          for (const changedPermission of changedPermissions) {
            if (changedPermission.webId) {
              await sendNoteNotification(
                changedPermission.webId,
                note.getFileUri(),
                NOTIFICATION_TYPE.CHANGED_PERMISSIONS
              );
            }
          }
        }
      }
    },
    [note, setAndSortPermissions, notePermissions, sendNoteNotification]
  );

  const updateCurrentUserPermissions = useCallback(
    async (permissions: Access): Promise<void> => {
      setUserPermissions(permissions);
    },
    [setUserPermissions]
  );

  // load the note permissions from the Solid pod
  useEffect(() => {
    if (note && webId && !permissionsLoaded && userPermissions?.control) {
      loadPermissions();
      setPermissionsLoaded(true);
    }
  }, [
    loadPermissions,
    permissionsLoaded,
    webId,
    note,
    userPermissions?.control,
    setPermissionsLoaded,
  ]);

  const onContentChange = useCallback(
    (content: string) => {
      if (noteFetched && editingAllowed) {
        if (richTextEdited) {
          setSaveData({ content, title: title ?? "" });
        } else {
          setRichTextEdited(true);
        }
      }
    },
    [
      noteFetched,
      richTextEdited,
      setRichTextEdited,
      setSaveData,
      title,
      editingAllowed,
    ]
  );

  return !note ? (
    <ContentSpinner />
  ) : (
    <>
      <NotePermissionsContextProvider
        value={{
          permissions: notePermissions,
          updatePermissions: updateNotePermissions,
          currentUserPermissions: userPermissions ?? noAccess,
          updateCurrentUserPermissions: updateCurrentUserPermissions,
        }}
      >
        <div>
          <div className="flex flex-col gap-4">
            <div className="flex flex-col-reverse gap-4 md:flex-row">
              <div className="w-full flex-1">
                <TextInput
                  value={title}
                  onChange={onTitleChange}
                  readOnly={!editingAllowed}
                  placeholder={t("editor.titlePlaceholder")}
                  className="!text-3xl !font-semibold !ring-gray-100 focus:!ring-indigo-600"
                />
              </div>
              <div className="flex grow-0 items-center justify-end gap-4">
                <PermissionInfo permissions={userPermissions ?? noAccess} />
                {userPermissions?.write ? (
                  <SavingIndicator
                    savingStatus={savingStatus ?? EditorSavingStatus.Loaded}
                    lastSavedDate={lastSavedDate}
                  />
                ) : null}
                <InfoBar note={note} isPublic={isNotePublic} />
              </div>
            </div>

            {noteFetched ? (
              <RichTextEditor
                onChange={onContentChange}
                initialValue={initialContent ?? ""}
                editable={editingAllowed}
              />
            ) : (
              <div className="m-3 flex animate-pulse items-center justify-center rounded-md bg-slate-100 py-24 text-sm font-semibold text-slate-600">
                Loading data from Solid...
              </div>
            )}
          </div>
        </div>
      </NotePermissionsContextProvider>
    </>
  );
};

export default NoteEditor;
