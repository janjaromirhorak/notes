import React, { useContext, useMemo } from "react";

import { ArrowTopRightOnSquareIcon } from "@heroicons/react/20/solid";

import { useTranslation } from "react-i18next";
import { RemoteNote } from "../../../services";
import { ButtonLink } from "../../Button";
import type { NotePermissionsContextType } from "../NotePermissionsContext";
import NotePermissionsContext from "../NotePermissionsContext";
import DeleteNoteButton from "./DeleteNoteButton";
import PermissionManager from "./PermissionManager";

/** Props for the InfoBar component */
type InfoBarProps = {
  /** Specifies if the note is public */
  isPublic: boolean;
  /** The remote note */
  note: RemoteNote;
};

/**
 * A component representing an info bar
 *
 * @param {InfoBarProps} props
 * @category Components
 */
const InfoBar: React.FC<InfoBarProps> = ({ note }) => {
  const { t } = useTranslation();

  const { currentUserPermissions: userPermissions } =
    useContext<NotePermissionsContextType>(NotePermissionsContext);

  const userCanDelete = useMemo(() => {
    if (userPermissions) {
      return userPermissions.control;
    }

    return false;
  }, [userPermissions]);

  return (
    <>
      <PermissionManager note={note} />
      {userCanDelete ? <DeleteNoteButton note={note} /> : null}
      <ButtonLink
        leftIcon={<ArrowTopRightOnSquareIcon className="pt-[0.1rem]" />}
        theme="secondary"
        href={note.getFileUri()}
        target="_blank"
        rel="noopener noreferrer"
        title={t("editor.openSource")}
        isExternal
      >
        {t("editor.sourceButton")}
      </ButtonLink>
    </>
  );
};

export default InfoBar;
