import React from "react";
import { useTranslation } from "react-i18next";

/**
 * A placeholder component for editor content
 *
 * @category Components
 */
const ContentPlaceholder: React.FC = () => {
  const { t } = useTranslation();
  return (
    <div className="pointer-events-none absolute left-0 top-0 inline-block overflow-hidden overflow-ellipsis text-slate-600 [user-select:none]">
      {t("editor.contentPlaceholder")}
    </div>
  );
};

export default ContentPlaceholder;
