import { PropsWithChildren } from "react";

type ToolbarButtonProps = {
  onClick: () => void;
  isActive: boolean;
  label: string;
  className?: string;
};

const ToolbarButton: React.FC<PropsWithChildren<ToolbarButtonProps>> = ({
  onClick,
  isActive,
  label,
  children,
  className = "",
}) => {
  return (
    <button
      onClick={onClick}
      title={label}
      aria-label={label}
      className={`flex w-8 items-center justify-center rounded-md p-1.5 hover:bg-slate-200 ${
        isActive ? "bg-slate-300" : ""
      } ${className}`}
    >
      {children}
    </button>
  );
};

export default ToolbarButton;
