import { $isCodeNode } from "@lexical/code";
import { $convertToMarkdownString } from "@lexical/markdown";
import { OnChangePlugin } from "@lexical/react/LexicalOnChangePlugin";
import type { EditorState } from "lexical";
import { $getRoot } from "lexical";
import type { Dispatch, SetStateAction } from "react";
import { useMemo } from "react";
import { useSafeState } from "../../../../../hooks/useSafeState";

export type OnChangeMarkdownType =
  | Dispatch<SetStateAction<string>>
  | ((value: string) => void);

const OnChangeMarkdown: React.FC<{
  transformers: any;
  onChange: OnChangeMarkdownType;
}> = ({ onChange, transformers }) => {
  const [debounceTimeout, setDebounceTimeout] = useSafeState<
    number | undefined
  >();

  const OnChangeMarkdown = useMemo(() => {
    return (state: EditorState): void => {
      window.clearTimeout(debounceTimeout);
      setDebounceTimeout(
        window.setTimeout(() => {
          transformState(state, onChange, transformers);
        }, 200)
      );
    };
  }, [onChange]);

  return <OnChangePlugin onChange={OnChangeMarkdown} ignoreSelectionChange />;
};

export default OnChangeMarkdown;

function transformState(
  editorState: EditorState,
  onChange: OnChangeMarkdownType,
  transformers: any
) {
  editorState.read(() => {
    const root = $getRoot();
    const firstChild = root.getFirstChild();

    const markdown = $convertToMarkdownString(transformers);

    const withBrs = markdown
      // https://github.com/markedjs/marked/issues/190#issuecomment-865303317
      .replace(/\n(?=\n)/g, "\n\n<br>\n")
      // When escape(markdown) with block quotes we end up with the following:
      // '&gt; block quote text'
      // and need to convert it back to the original, so the markdown is respected
      .replace(/^(&gt\;)(?=\s)(?!.*&lt\;)/gm, ">");

    if ($isCodeNode(firstChild) && firstChild.getLanguage() === "markdown") {
      onChange(withBrs.trim().replace(/^```markdown((.*|\n)*)```$/m, "$1"));
    } else {
      onChange(withBrs);
    }
  });
}
