import {
  $createCodeNode,
  $isCodeNode,
  getCodeLanguages,
  getDefaultCodeLanguage,
} from "@lexical/code";
import {
  $isListNode,
  INSERT_ORDERED_LIST_COMMAND,
  INSERT_UNORDERED_LIST_COMMAND,
  ListNode,
  REMOVE_LIST_COMMAND,
} from "@lexical/list";
import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";
import {
  $createHeadingNode,
  $createQuoteNode,
  $isHeadingNode,
} from "@lexical/rich-text";
import { $wrapNodes } from "@lexical/selection";
import { $getNearestNodeOfType, mergeRegister } from "@lexical/utils";
import {
  $createParagraphNode,
  $getNodeByKey,
  $getSelection,
  $isRangeSelection,
  FORMAT_TEXT_COMMAND,
  SELECTION_CHANGE_COMMAND,
} from "lexical";
import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { ReactComponent as BoldIcon } from "../../../../../assets/icons/bold.svg";
import { ReactComponent as CodeIcon } from "../../../../../assets/icons/code.svg";
import { ReactComponent as ItalicIcon } from "../../../../../assets/icons/italic.svg";
import { ReactComponent as MarkdownIcon } from "../../../../../assets/icons/markdown.svg";
import { ReactComponent as StrikethroughIcon } from "../../../../../assets/icons/strikethrough.svg";
import { ReactComponent as UnderlineIcon } from "../../../../../assets/icons/underline.svg";
import ToolbarButton from "./ToolbarButton";

import Dropdown, { DropdownOption } from "../../../../Dropdown/Dropdown";

import {
  $convertFromMarkdownString,
  $convertToMarkdownString,
} from "@lexical/markdown";
import { $createTextNode, $getRoot } from "lexical";

import { useSafeState } from "../../../../../hooks/useSafeState";
import { PLAYGROUND_TRANSFORMERS } from "./MarkdownTransformers";

const LowPriority = 1;

const supportedBlockTypes = new Set([
  "paragraph",
  "quote",
  "code",
  "h1",
  "h2",
  "ul",
  "ol",
]);

interface ToolbarPluginProps {
  editable: boolean;
}

/**
 * Lexical plugin that renders the toolbar and handles its logic
 *
 * @category Components
 */
const ToolbarPlugin: React.FC<ToolbarPluginProps> = ({ editable }) => {
  const [editor] = useLexicalComposerContext();
  const toolbarRef = useRef(null);
  const [blockType, setBlockType] = useState<string | null>("paragraph");
  const [selectedElementKey, setSelectedElementKey] = useSafeState<
    string | null
  >(null);
  const [codeLanguage, setCodeLanguage] = useSafeState<string>("");
  const [isBold, setIsBold] = useSafeState<boolean>(false);
  const [isItalic, setIsItalic] = useSafeState<boolean>(false);
  const [isUnderline, setIsUnderline] = useSafeState<boolean>(false);
  const [isStrikethrough, setIsStrikethrough] = useSafeState<boolean>(false);
  const [isCode, setIsCode] = useSafeState<boolean>(false);
  const [isInMarkdownMode, setIsInMarkdownMode] = useSafeState<boolean>(false);

  const updateToolbar = useCallback(() => {
    const selection = $getSelection();
    if ($isRangeSelection(selection)) {
      const anchorNode = selection.anchor.getNode();
      const element =
        anchorNode.getKey() === "root"
          ? anchorNode
          : anchorNode.getTopLevelElementOrThrow();
      const elementKey = element.getKey();
      const elementDOM = editor.getElementByKey(elementKey);
      if (elementDOM !== null) {
        setSelectedElementKey(elementKey);
        if ($isListNode(element)) {
          const parentList = $getNearestNodeOfType(anchorNode, ListNode);
          const type = parentList ? parentList.getTag() : element.getTag();
          setBlockType(type);
        } else {
          const type = $isHeadingNode(element)
            ? element.getTag()
            : element.getType();
          setBlockType(type);
          if ($isCodeNode(element)) {
            setCodeLanguage(element.getLanguage() || getDefaultCodeLanguage());
          }
        }
      }
      // Update text format
      setIsBold(selection.hasFormat("bold"));
      setIsItalic(selection.hasFormat("italic"));
      setIsUnderline(selection.hasFormat("underline"));
      setIsStrikethrough(selection.hasFormat("strikethrough"));
      setIsCode(selection.hasFormat("code"));
    }
  }, [editor]);

  useEffect(() => {
    return mergeRegister(
      editor.registerUpdateListener(({ editorState }) => {
        editorState.read(() => {
          updateToolbar();
        });
      }),
      editor.registerCommand(
        SELECTION_CHANGE_COMMAND,
        (_payload, newEditor) => {
          updateToolbar();
          return false;
        },
        LowPriority
      )
    );
  }, [editor, updateToolbar]);

  const onCodeLanguageSelect = useCallback(
    (language: string) => {
      editor.update(() => {
        if (selectedElementKey !== null) {
          const node = selectedElementKey
            ? $getNodeByKey(selectedElementKey)
            : undefined;
          if (node && $isCodeNode(node)) {
            node.setLanguage(language);
          }
        }
      });
    },
    [editor, selectedElementKey]
  );

  const codeLanguages = useMemo<DropdownOption[]>(
    () =>
      getCodeLanguages().map((language) => ({
        label: language,
        value: language,
        onSelect: () => {
          onCodeLanguageSelect(language);
        },
      })),
    []
  );

  const formatParagraph = useCallback(() => {
    if (blockType !== "paragraph") {
      editor.update(() => {
        const selection = $getSelection();

        if ($isRangeSelection(selection)) {
          $wrapNodes(selection, () => $createParagraphNode());
        }
      });
    }
  }, [editor, blockType]);

  const formatLargeHeading = useCallback(() => {
    if (blockType !== "h1") {
      editor.update(() => {
        const selection = $getSelection();

        if ($isRangeSelection(selection)) {
          $wrapNodes(selection, () => $createHeadingNode("h1"));
        }
      });
    }
  }, [editor, blockType]);

  const formatSmallHeading = useCallback(() => {
    if (blockType !== "h2") {
      editor.update(() => {
        const selection = $getSelection();

        if ($isRangeSelection(selection)) {
          $wrapNodes(selection, () => $createHeadingNode("h2"));
        }
      });
    }
  }, [editor, blockType]);

  const formatBulletList = useCallback(() => {
    if (blockType !== "ul") {
      // @ts-expect-error wrong typing in the library
      editor.dispatchCommand(INSERT_UNORDERED_LIST_COMMAND);
    } else {
      // @ts-expect-error wrong typing in the library
      editor.dispatchCommand(REMOVE_LIST_COMMAND);
    }
  }, [editor, blockType]);

  const formatNumberedList = useCallback(() => {
    if (blockType !== "ol") {
      // @ts-expect-error wrong typing in the library
      editor.dispatchCommand(INSERT_ORDERED_LIST_COMMAND);
    } else {
      // @ts-expect-error wrong typing in the library
      editor.dispatchCommand(REMOVE_LIST_COMMAND);
    }
  }, [editor, blockType]);

  const formatQuote = useCallback(() => {
    if (blockType !== "quote") {
      editor.update(() => {
        const selection = $getSelection();

        if ($isRangeSelection(selection)) {
          $wrapNodes(selection, () => $createQuoteNode());
        }
      });
    }
  }, [editor, blockType]);

  const formatCode = useCallback(() => {
    if (blockType !== "code") {
      editor.update(() => {
        const selection = $getSelection();

        if ($isRangeSelection(selection)) {
          $wrapNodes(selection, () => $createCodeNode());
        }
      });
    }
  }, [editor, blockType]);

  const handleMarkdownToggle = useCallback(() => {
    editor.update(() => {
      const root = $getRoot();
      const firstChild = root.getFirstChild();
      if ($isCodeNode(firstChild) && firstChild.getLanguage() === "markdown") {
        $convertFromMarkdownString(
          firstChild.getTextContent(),
          PLAYGROUND_TRANSFORMERS
        );
        setIsInMarkdownMode(false);
      } else {
        const markdown = $convertToMarkdownString(PLAYGROUND_TRANSFORMERS);
        root
          .clear()
          .append(
            $createCodeNode("markdown").append($createTextNode(markdown))
          );
        setIsInMarkdownMode(true);
      }
      root.selectEnd();
    });
  }, [editor]);

  return (
    <div
      className="mb-3 flex h-12 items-center gap-1 border-b border-b-gray-300 pb-5 pt-1"
      ref={toolbarRef}
    >
      {editable && (
        <>
          {!isInMarkdownMode &&
            blockType &&
            supportedBlockTypes.has(blockType) && (
              <>
                <Dropdown
                  options={[
                    {
                      label: "Normal",
                      value: "paragraph",
                      onSelect: formatParagraph,
                    },
                    {
                      label: "Large Heading",
                      value: "h1",
                      onSelect: formatLargeHeading,
                    },
                    {
                      label: "Small Heading",
                      value: "h2",
                      onSelect: formatSmallHeading,
                    },
                    {
                      label: "Bullet List",
                      value: "ul",
                      onSelect: formatBulletList,
                    },
                    {
                      label: "Numbered list",
                      value: "ol",
                      onSelect: formatNumberedList,
                    },
                    { label: "Quote", value: "quote", onSelect: formatQuote },
                    {
                      label: "Code Block",
                      value: "code",
                      onSelect: formatCode,
                    },
                  ]}
                  activeOption={blockType ?? undefined}
                  defaultText="Select style"
                  aria-label="Formatting Options"
                  className="mr-2 min-w-[10rem]"
                />
              </>
            )}

          {blockType === "code" ? (
            !isInMarkdownMode && (
              <>
                <Dropdown
                  options={codeLanguages}
                  activeOption={codeLanguage}
                  defaultText="Language"
                  aria-label="Code language options"
                />
              </>
            )
          ) : (
            <>
              <ToolbarButton
                onClick={() => {
                  editor.dispatchCommand(FORMAT_TEXT_COMMAND, "bold");
                }}
                isActive={!!isBold}
                label="Bold"
              >
                <BoldIcon />
              </ToolbarButton>
              <ToolbarButton
                onClick={() => {
                  editor.dispatchCommand(FORMAT_TEXT_COMMAND, "italic");
                }}
                isActive={!!isItalic}
                label="Italics"
              >
                <ItalicIcon />
              </ToolbarButton>
              <ToolbarButton
                onClick={() => {
                  editor.dispatchCommand(FORMAT_TEXT_COMMAND, "underline");
                }}
                isActive={!!isUnderline}
                label="Underline"
              >
                <UnderlineIcon />
              </ToolbarButton>
              <ToolbarButton
                onClick={() => {
                  editor.dispatchCommand(FORMAT_TEXT_COMMAND, "strikethrough");
                }}
                isActive={!!isStrikethrough}
                label="Strikethrough"
              >
                <StrikethroughIcon />
              </ToolbarButton>
              <ToolbarButton
                onClick={() => {
                  editor.dispatchCommand(FORMAT_TEXT_COMMAND, "code");
                }}
                isActive={!!isCode}
                label="Code"
              >
                <CodeIcon />
              </ToolbarButton>
            </>
          )}
        </>
      )}
      <ToolbarButton
        onClick={handleMarkdownToggle}
        isActive={!!isInMarkdownMode}
        label="View markdown code"
        className="ml-auto flex h-10"
      >
        <MarkdownIcon />
      </ToolbarButton>
    </div>
  );
};

export default ToolbarPlugin;
