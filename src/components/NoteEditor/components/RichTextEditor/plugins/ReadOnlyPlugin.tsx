import { useLexicalComposerContext } from "@lexical/react/LexicalComposerContext";
import { useEffect } from "react";

export type ReadOnlyPluginType = {
  editable: boolean;
};

/**
 * Lexical plugin to update the plugin state according to the 'editable' prop
 *
 * @category Components
 */
const ReadOnlyPlugin: React.FC<ReadOnlyPluginType> = ({ editable }) => {
  const [editor] = useLexicalComposerContext();
  useEffect(() => {
    editor.setEditable(editable);
  }, [editable, editor]);
  return null;
};

export default ReadOnlyPlugin;
