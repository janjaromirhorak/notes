import { CodeHighlightNode, CodeNode } from "@lexical/code";
import { AutoLinkNode, LinkNode } from "@lexical/link";
import { ListItemNode, ListNode } from "@lexical/list";
import { $convertFromMarkdownString, TRANSFORMERS } from "@lexical/markdown";
import { AutoFocusPlugin } from "@lexical/react/LexicalAutoFocusPlugin";
import { LexicalComposer } from "@lexical/react/LexicalComposer";
import { ContentEditable } from "@lexical/react/LexicalContentEditable";
import { LinkPlugin } from "@lexical/react/LexicalLinkPlugin";
import { ListPlugin } from "@lexical/react/LexicalListPlugin";
import { MarkdownShortcutPlugin } from "@lexical/react/LexicalMarkdownShortcutPlugin";
import { RichTextPlugin } from "@lexical/react/LexicalRichTextPlugin";
import { HeadingNode, QuoteNode } from "@lexical/rich-text";
import { TableCellNode, TableNode, TableRowNode } from "@lexical/table";
import ToolbarPlugin from "./plugins/ToolbarPlugin";
import EditorTheme from "./themes/EditorTheme";

import CodeHighlightPlugin from "./plugins/CodeHighlightPlugin";

import { $createParagraphNode, $getRoot } from "lexical";
import { useEffect } from "react";
import ContentPlaceholder from "./ContentPlaceholder";
import OnChangeMarkdown from "./plugins/OnChangeMarkdown";
import ReadOnlyPlugin from "./plugins/ReadOnlyPlugin";
import "./styles.css";

/** Props for the RichTextEditor component */
type RichTextEditorProps = {
  /** The initial value of the editor */
  initialValue: string;
  /**
   * Function called when the editor value changes
   *
   * @param {string} value - The updated value of the editor
   */
  onChange: (value: string) => void;
  /** Specifies if the editor is editable */
  editable: boolean;
};

/**
 * A component representing a rich text editor
 *
 * @param {RichTextEditorProps} props
 * @category Components
 */
const RichTextEditor = ({
  initialValue,
  onChange,
  editable,
}: RichTextEditorProps) => {
  useEffect(() => {}, [editable]);

  return (
    <LexicalComposer
      initialConfig={{
        theme: EditorTheme,
        namespace: "RichTextEditor",
        // Handling of errors during update
        onError(error) {
          throw error;
        },
        // Any custom nodes go here
        nodes: [
          HeadingNode,
          ListNode,
          ListItemNode,
          QuoteNode,
          CodeNode,
          CodeHighlightNode,
          TableNode,
          TableCellNode,
          TableRowNode,
          AutoLinkNode,
          LinkNode,
        ],
        editorState: () => {
          let str = (initialValue || "").replace(/\n\n<br>\n/g, "\n");

          // If we still have br tags, we're coming from Slate, apply
          // Slate list collapse and remove remaining br tags
          // https://github.com/facebook/lexical/issues/2208
          if (str.match(/<br>/g)) {
            str = str.replace(/^(\n)(?=\s*[-+\d.])/gm, "").replace(/<br>/g, "");
          }

          str = str
            // Unescape HTML characters
            .replace(/&quot;/g, '"')
            .replace(/&amp;/g, "&")
            .replace(/&#39;/g, "'")
            .replace(/&lt;/g, "<")
            .replace(/&gt;/g, ">");

          if (!str) {
            // if string is empty and this is not an update
            // don't bother trying to $convertFromMarkdown
            // below we properly initialize with the correct state allowing for
            // AutoFocus to work (as there is state to focus on), which works better
            // than $convertFromMarkdownString('')
            const root = $getRoot();
            const paragraph = $createParagraphNode();
            root.append(paragraph);
            return;
          }

          $convertFromMarkdownString(str, TRANSFORMERS);
        },
      }}
    >
      <div className="relative w-full">
        <ToolbarPlugin editable={editable} />
        <div className="relative bg-white">
          {/* @ts-expect-error wrong typing in the library */}
          <RichTextPlugin
            contentEditable={
              <ContentEditable className="relative min-h-[25rem] resize-none caret-slate-800 outline-none [tab-size:1]" />
            }
            placeholder={<ContentPlaceholder />}
          />
          <AutoFocusPlugin />
          <ListPlugin />
          <LinkPlugin />
          <MarkdownShortcutPlugin transformers={TRANSFORMERS} />
          <CodeHighlightPlugin />
          <OnChangeMarkdown onChange={onChange} transformers={TRANSFORMERS} />
          <ReadOnlyPlugin editable={editable} />
        </div>
      </div>
    </LexicalComposer>
  );
};

export default RichTextEditor;
