import React, { ChangeEvent, useCallback, useContext, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useSafeState } from "../../../hooks/useSafeState";

import NotePermissionsContext, {
  NotePermissionsContextType,
  noAccess,
} from "../NotePermissionsContext";

import { Access } from "@inrupt/solid-client";
import { useWebId } from "../../../components/SessionProvider";
import { RemoteNote } from "../../../services";
import Button from "../../Button";
import useRemoteResourceFactory from "../../SessionProvider/useRemoteResourceFactory";
import TextInput from "../../TextInput";
import PermissionSelect from "./PermissionSelect";
import useSendNoteNotification, {
  NOTIFICATION_TYPE,
} from "./useSendNoteNotification";

/** Props for the AddNewUserForm component */
type AddNewUserFormProps = {
  /** The remote note */
  note: RemoteNote;
  /** Callback that closes the modal */
  closeModal: () => void;
};

/** The default permissions for a new user */
const defaultPermissions: Access = {
  read: true,
  write: false,
  append: false,
  control: false,
};

/**
 * A component representing a form to add a new user
 *
 * @param {AddNewUserFormProps} props
 * @category Components
 */
const AddNewUserForm: React.FC<AddNewUserFormProps> = ({
  note,
  closeModal,
}) => {
  const { t } = useTranslation();
  const webId: string | undefined = useWebId();
  const factory = useRemoteResourceFactory();

  const sendNoteNotification = useSendNoteNotification();

  const [isLoading, setIsLoading] = useSafeState<boolean>(false);
  const { permissions, updatePermissions } =
    useContext<NotePermissionsContextType>(NotePermissionsContext);
  const [friends, setFriends] = useSafeState<string[]>([]);

  const [newUserWebId, setNewUserWebId] = useSafeState<string>("");
  const onNewUserWebIdChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setNewUserWebId(event.target.value);
    },
    [setNewUserWebId]
  );

  const [newUserPermissions, setNewUserPermissions] =
    useSafeState<Access>(defaultPermissions);

  const addNewUser = useCallback(async () => {
    if (newUserWebId && webId !== newUserWebId) {
      setIsLoading(true);

      const newPermissions = (permissions ?? []).filter(
        (permission) => permission.webId !== newUserWebId
      );

      updatePermissions([
        ...newPermissions,
        {
          webId: newUserWebId,
          permissions: newUserPermissions ?? noAccess,
        },
      ]);

      if (newUserWebId !== webId) {
        await sendNoteNotification(
          newUserWebId,
          note.getUri(),
          NOTIFICATION_TYPE.ADDED_TO_DOCUMENT
        );
      }

      setNewUserWebId("");
      setIsLoading(false);
      closeModal();
    }
  }, [
    webId,
    newUserWebId,
    newUserPermissions,
    updatePermissions,
    permissions,
    sendNoteNotification,
    note,
    setIsLoading,
    setNewUserWebId,
  ]);

  useEffect(() => {
    if (webId) {
      const profile = factory.getProfile(webId);
      profile.getKnows().then(setFriends);
    } else {
      setFriends([]);
    }
  }, [webId, setFriends]);

  return (
    <>
      <div className="mb-4">
        <label className="mb-1 block" htmlFor="webid">
          {t("editor.permissions.newUserWebId")}
        </label>
        <TextInput
          id="webid"
          type="text"
          placeholder="https://..."
          value={newUserWebId}
          onChange={onNewUserWebIdChange}
          list="webIdList"
        />
        <datalist id="webIdList">
          {(friends ?? []).map((friendWebId) => (
            <option key={friendWebId} value={friendWebId} />
          ))}
        </datalist>
      </div>
      <div className="mb-4">
        <label className="mb-1 block" htmlFor="permissionSelect">
          {t("editor.permissions.newUserPermissions")}
        </label>
        <PermissionSelect
          value={newUserPermissions ?? noAccess}
          onChange={setNewUserPermissions}
        />
      </div>
      <div className="gap-2 py-3 sm:flex sm:flex-row-reverse">
        <Button onClick={addNewUser} isLoading={isLoading}>
          {t("editor.permissions.shareWithUserSubmit")}
        </Button>
        <Button theme="secondary" onClick={closeModal}>
          Cancel
        </Button>
      </div>
    </>
  );
};

export default AddNewUserForm;
