import React, { useCallback, useContext, useMemo } from "react";
import { BsPeopleFill } from "react-icons/bs";
import { useSafeState } from "../../../hooks/useSafeState";

import { Access } from "@inrupt/solid-client";
import { useTranslation } from "react-i18next";
import { useWebId } from "../../../components/SessionProvider";
import { RemoteNote } from "../../../services";
import Button from "../../Button";
import Modal from "../../Modal";
import Tabs from "../../Tabs";
import type { NotePermissionsContextType } from "../NotePermissionsContext";
import NotePermissionsContext from "../NotePermissionsContext";
import AddNewUserForm from "./AddNewUserForm";
import UserList from "./UserList";

/** Props for the PermissionManager component */
type PermissionManagerProps = {
  /** The remote note */
  note: RemoteNote;
};

/**
 * A component representing a permission manager
 *
 * @param {PermissionManagerProps} props
 * @category Components
 */
const PermissionManager: React.FC<PermissionManagerProps> = ({ note }) => {
  const { t } = useTranslation();
  const webId: string | undefined = useWebId();

  const { permissions } = useContext<NotePermissionsContextType>(
    NotePermissionsContext
  );

  const userPermissions = useMemo<Access>(() => {
    if (permissions) {
      const [userPermissions] = permissions.filter(
        (permissionItem) => permissionItem.webId === webId
      );
      if (userPermissions) {
        return userPermissions.permissions;
      }
    }

    return {
      read: false,
      write: false,
      append: false,
      control: false,
    };
  }, [permissions, webId]);

  const [isModalOpen, setIsModalOpen] = useSafeState<boolean>(false);

  const openModal = useCallback(() => {
    setIsModalOpen(true);
  }, [setIsModalOpen]);

  const closeModal = useCallback(() => {
    setIsModalOpen(false);
  }, [setIsModalOpen]);

  return (
    <>
      {userPermissions.control ? (
        <>
          <Button onClick={openModal} leftIcon={<BsPeopleFill />}>
            {t("editor.shareButton")}
          </Button>
          <Modal
            title="Share this note with others"
            isOpen={!!isModalOpen}
            close={closeModal}
          >
            <Tabs
              tabs={[
                {
                  title: "Current permissions",
                  content: (
                    <>
                      {userPermissions.control && permissions ? (
                        <UserList closeModal={closeModal} />
                      ) : null}
                    </>
                  ),
                },
                {
                  title: "Invite someone",
                  content: (
                    <AddNewUserForm note={note} closeModal={closeModal} />
                  ),
                },
              ]}
            />
          </Modal>
        </>
      ) : null}
    </>
  );
};

export default PermissionManager;
