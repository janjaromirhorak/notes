import { Access } from "@inrupt/solid-client";
import React, { useCallback, useContext, useEffect, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { useSafeState } from "../../../hooks/useSafeState";
import Button from "../../Button";
import Toggle from "../../Toggle";
import type {
  NotePermissionsContextType,
  PermissionContextItem,
} from "../NotePermissionsContext";
import NotePermissionsContext from "../NotePermissionsContext";
import UserListItem from "./UserListItem";

/** Props for the UserList component */
type UserListProps = {
  /** Callback that closes the modal */
  closeModal: () => void;
};

/**
 * A component representing an editable list of users with access to the note
 *
 * @prop {UserListProps} props
 * @category Components
 */
const UserList: React.FC<UserListProps> = ({ closeModal }) => {
  const { t } = useTranslation();

  const { permissions, updatePermissions } =
    useContext<NotePermissionsContextType>(NotePermissionsContext);
  const [permissionsState, setPermissionsState] = useSafeState<
    PermissionContextItem[] | undefined
  >(undefined);
  const [isLoading, setIsLoading] = useSafeState<boolean>(false);

  const setAndSortPermissionsState = useCallback(
    (permissions: PermissionContextItem[] | undefined) => {
      if (permissions) {
        setPermissionsState(
          permissions.sort((a, b) => {
            if (a.webId && b.webId) {
              if (a.webId < b.webId) {
                return -1;
              } else if (a.webId > b.webId) {
                return 1;
              }
              return 0;
            } else if (a.webId) {
              return -1;
            } else if (b.webId) {
              return 1;
            }

            return 0;
          })
        );
      } else {
        setPermissionsState(permissions);
      }
    },
    [setPermissionsState]
  );

  useEffect(() => {
    setAndSortPermissionsState(permissions);
  }, [permissions, setAndSortPermissionsState]);

  const onUserPermissionsChange = useCallback(
    (webId: string | undefined, userPermissions: Access) => {
      const newPermissions = (permissionsState ?? []).filter(
        (permission) => permission.webId !== webId
      );
      setAndSortPermissionsState([
        ...newPermissions,
        {
          webId,
          permissions: userPermissions,
        },
      ]);
    },
    [setAndSortPermissionsState, permissionsState]
  );

  const savePermissionChanges = useCallback(async () => {
    setIsLoading(true);
    await updatePermissions(permissionsState ?? []);
    setIsLoading(false);
  }, [updatePermissions, permissionsState, setIsLoading]);

  const publicPermissionState: Access = useMemo(() => {
    if (permissionsState) {
      const [publicPermissionState] = (permissionsState ?? []).filter(
        (item) => !item.webId
      );
      if (publicPermissionState) {
        return publicPermissionState.permissions;
      }
    }

    return {
      read: false,
      write: false,
      append: false,
      control: false,
    };
  }, [permissionsState]);

  const onPublicPermissionsChange = useCallback(() => {
    onUserPermissionsChange(undefined, {
      ...publicPermissionState,
      read: !publicPermissionState.read,
    });
  }, [onUserPermissionsChange, publicPermissionState]);

  return (
    <>
      <div className="mt-2 flex items-center gap-4">
        <Toggle
          enabled={publicPermissionState.read}
          onChange={onPublicPermissionsChange}
        />
        <button onClick={onPublicPermissionsChange}>
          {t("editor.permissions.publish.label")}
        </button>
      </div>
      <table className="mt-6">
        <thead>
          <tr>
            <th className="text-left">{t("editor.permissions.table.user")}</th>
            <th className="text-left">
              {t("editor.permissions.table.permissions")}
            </th>
          </tr>
        </thead>
        <tbody>
          {(permissionsState ?? [])
            .filter((userPermissions) => userPermissions.webId)
            .map((userPermissions) => (
              <UserListItem
                key={userPermissions.webId}
                webId={userPermissions.webId ?? ""}
                permissions={userPermissions.permissions}
                onPermissionsChange={(permissions) =>
                  onUserPermissionsChange(userPermissions.webId, permissions)
                }
              />
            ))}
        </tbody>
      </table>
      <div className="gap-2 py-3 sm:flex sm:flex-row-reverse">
        <Button onClick={savePermissionChanges} isLoading={isLoading}>
          {t("editor.permissions.save")}
        </Button>
        <Button theme="secondary" onClick={closeModal}>
          Cancel
        </Button>
      </div>
    </>
  );
};

export default UserList;
