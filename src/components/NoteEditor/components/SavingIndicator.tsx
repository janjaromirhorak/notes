import { CheckIcon, PencilIcon } from "@heroicons/react/20/solid";
import React, { memo, useMemo } from "react";
import { useTranslation } from "react-i18next";

import formatDateTime from "../../../utils/formatDateTime";
import Tooltip from "../../Tooltip";
import EditorSavingStatus from "../EditorSavingStatus";

/** Props for the SavingIndicator component */
type SavingIndicatorProps = {
  /** The saving status */
  savingStatus: EditorSavingStatus;
  /** The date when the content was last saved (optional) */
  lastSavedDate: Date | undefined;
};

/**
 * A component representing a saving indicator
 *
 * @param {SavingIndicatorProps} props
 * @category Components
 */
const SavingIndicator: React.FC<SavingIndicatorProps> = ({
  savingStatus,
  lastSavedDate,
}) => {
  const { t } = useTranslation();

  const formattedSaveDate = useMemo<string | undefined>(() => {
    if (!lastSavedDate) {
      return undefined;
    }

    return formatDateTime(lastSavedDate);
  }, [lastSavedDate]);

  if (savingStatus === EditorSavingStatus.Changed) {
    return <PencilIcon className="w-4 opacity-50" />;
  } else if (savingStatus === EditorSavingStatus.Saving) {
    return (
      <Tooltip
        placement="bottom"
        content={t("editor.saving")}
        className="block opacity-50"
      >
        <div className="h-4 w-4 animate-spin rounded-full border-2 border-slate-700 border-t-transparent"></div>
      </Tooltip>
    );
  }

  return (
    <Tooltip
      placement="bottom"
      content={
        <div className="text-center">
          {t("editor.savedTitle")}
          {formattedSaveDate && (
            <>
              <br />
              {formattedSaveDate}
            </>
          )}
        </div>
      }
      className="block opacity-50"
    >
      <span
        className={
          import.meta.env.VITE_IUM_CLASS_NAMES
            ? "testing-note-saving-saved"
            : ""
        }
      >
        <CheckIcon className="w-4" />
      </span>
    </Tooltip>
  );
};

const FinalSavingIndicator: React.FC<SavingIndicatorProps> =
  memo<SavingIndicatorProps>(SavingIndicator);

export default FinalSavingIndicator;
