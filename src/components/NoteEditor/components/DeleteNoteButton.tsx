import { TrashIcon } from "@heroicons/react/20/solid";
import { TrashIcon as OutlineTrashIcon } from "@heroicons/react/24/outline";
import React, { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useWebId } from "../../../components/SessionProvider";
import { useSafeState } from "../../../hooks/useSafeState";

import { RemoteNote } from "../../../services";

import { useNavigate } from "react-router-dom";
import Button from "../../Button";
import Modal from "../../Modal";
import useNoteListHelpers from "../../SessionProvider/useNoteListHelpers";
import useRemoteResourceFactory from "../../SessionProvider/useRemoteResourceFactory";

/** Props for the DeleteNoteButton component */
type DeleteNoteButtonProps = {
  /** The remote note */
  note: RemoteNote;
};

/**
 * A component representing a button to delete a note
 *
 * @param {DeleteNoteButtonProps} props
 * @category Components
 */
const DeleteNoteButton: React.FC<DeleteNoteButtonProps> = ({ note }) => {
  const webId = useWebId();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const factory = useRemoteResourceFactory();

  const [isModalOpen, setIsModalOpen] = useSafeState<boolean>(false);
  const [isDeletingNote, setIsDeletingNote] = useSafeState<boolean>(false);
  const { removeNoteFromList } = useNoteListHelpers();

  const openModal = useCallback(() => {
    setIsModalOpen(true);
  }, [setIsModalOpen]);

  const closeModal = useCallback(() => {
    setIsModalOpen(false);
  }, [setIsModalOpen]);

  const deleteNote = useCallback(async () => {
    if (webId) {
      setIsDeletingNote(true);
      const noteUri = note.getUri();
      const closestDirUri = note.getClosestDirUri();

      // delete the note
      await note.deleteRemoteFile();

      // clear parent directory cache
      factory.getDirectory(closestDirUri).clearListingCache();
      removeNoteFromList(noteUri);

      setIsDeletingNote(false);
      navigate("/");
    }
  }, [webId, note, history]);

  return (
    <>
      <Button
        onClick={openModal}
        leftIcon={<TrashIcon />}
        theme="secondaryDanger"
      >
        {t("editor.deleteButton")}
      </Button>

      <Modal
        isOpen={!!isModalOpen}
        close={closeModal}
        title="Delete this note"
        theme="danger"
        onSubmit={deleteNote}
        Icon={OutlineTrashIcon}
        submitButtonText="Delete note"
        isSubmitting={isDeletingNote}
      >
        {t("editor.deleteConfirm")}
      </Modal>
    </>
  );
};

export default DeleteNoteButton;
