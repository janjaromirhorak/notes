import React, { memo, useCallback, useEffect } from "react";
import { useWebId } from "../../../components/SessionProvider";
import { useSafeState } from "../../../hooks/useSafeState";

import { Access } from "@inrupt/solid-client";
import { useTranslation } from "react-i18next";
import ProfilePicture from "../../ControlPanel/TopBar/ProfilePicture";
import Link from "../../Link";
import useRemoteResourceFactory from "../../SessionProvider/useRemoteResourceFactory";
import PermissionSelect from "./PermissionSelect";

/** Props for the UserListItem component */
type UserListItemProps = {
  /** The WebID of the user */
  webId: string;
  /** The permissions assigned to the user */
  permissions: Access;
  /** The callback function for when the permissions change */
  onPermissionsChange: (permissions: Access) => void;
};

/**
 * A component representing a user item in the user list
 *
 * @param {UserListItemProps} props
 * @category Components
 */
const UserListItem: React.FC<UserListItemProps> = memo(
  ({ webId, permissions, onPermissionsChange }) => {
    const { t } = useTranslation();
    const currentWebId: string | undefined = useWebId();
    const factory = useRemoteResourceFactory();

    const [name, setName] = useSafeState<string>("");

    const [permissionState, setPermissionState] =
      useSafeState<Access>(permissions);

    const onSelectChange = useCallback(
      (newPermissions: Access) => {
        setPermissionState(newPermissions);
        onPermissionsChange(newPermissions);
      },
      [setPermissionState, onPermissionsChange]
    );

    useEffect(() => {
      const profile = factory.getProfile(webId);
      profile.getName().then(setName);
    }, [webId, setName]);

    return (
      <tr>
        <td className="min-w-[12rem] py-3 pr-4">
          <div className="flex items-center justify-start gap-4">
            <ProfilePicture webId={webId} />
            <div className="text-sm">
              {webId ? (
                <Link
                  isExternal
                  to={webId}
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  {name}
                </Link>
              ) : (
                name
              )}
              {webId === currentWebId
                ? ` (${t("editor.permissions.you")})`
                : null}
            </div>
          </div>
        </td>
        <td className="py-3">
          {permissionState ? (
            <div className="w-[16rem]">
              <PermissionSelect
                value={permissionState}
                onChange={onSelectChange}
              />
            </div>
          ) : null}
        </td>
      </tr>
    );
  }
);

export default UserListItem;
