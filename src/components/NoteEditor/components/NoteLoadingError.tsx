import React from "react";
import { Trans, useTranslation } from "react-i18next";
import { useIsLoggedIn } from "../../../components/SessionProvider";
import ErrorBox from "../../ErrorBox";
import Link from "../../Link";

/** Props for the NoteLoadingError component */
type NoteLoadingErrorProps = {
  /** The URI of the note */
  noteUri: string;
};

/**
 * A component representing an error when loading a note
 *
 * @param {NoteLoadingErrorProps} props
 * @category Components
 */
const NoteLoadingError: React.FC<NoteLoadingErrorProps> = ({ noteUri }) => {
  const { t } = useTranslation();
  const isLoggedIn = useIsLoggedIn();

  return (
    <ErrorBox title={t("editor.noteLoadingError.title")}>
      <p className="mb-6 max-w-screen-sm">
        {isLoggedIn ? (
          t("editor.noteLoadingError.loggedInText")
        ) : (
          <Trans
            t={t}
            i18nKey="editor.noteLoadingError.loggedOutText"
            components={{
              l: <Link to={"/login"} theme="danger" />,
            }}
          />
        )}
      </p>
      <p className="max-w-screen-sm">
        {t("editor.noteLoadingError.noteUri")}{" "}
        <Link to={noteUri} isExternal theme="danger">
          {noteUri}
        </Link>
      </p>
    </ErrorBox>
  );
};

export default NoteLoadingError;
