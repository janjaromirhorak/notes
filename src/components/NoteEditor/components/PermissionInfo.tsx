import {
  AdjustmentsVerticalIcon,
  EyeIcon,
  PencilSquareIcon,
} from "@heroicons/react/20/solid";
import { Access } from "@inrupt/solid-client";
import React, { useMemo } from "react";
import { useTranslation } from "react-i18next";
import Tooltip from "../../Tooltip";

/** Props for the PermissionInfo and PermissionIcon components */
type PermissionInfoProps = {
  /** The access permissions */
  permissions: Access;
};

/**
 * An icon representing permission information
 *
 * @param {PermissionInfoProps} props
 * @category Components
 */
const PermissionIcon: React.FC<PermissionInfoProps> = ({ permissions }) => {
  if (permissions.control) {
    return <AdjustmentsVerticalIcon className="w-4" />;
  }

  if (permissions.write) {
    return <PencilSquareIcon className="w-4" />;
  }

  if (permissions.read) {
    return <EyeIcon className="w-4" />;
  }

  return <></>;
};

/**
 * A component representing permission information
 *
 * @param {PermissionInfoProps} props
 * @category Components
 */
const PermissionInfo: React.FC<PermissionInfoProps> = ({ permissions }) => {
  const { t } = useTranslation();

  const tooltipText = useMemo<string | undefined>(() => {
    if (permissions.control) {
      return t("editor.permissions.youHaveControl");
    }

    if (permissions.write) {
      return t("editor.permissions.youHaveWrite");
    }

    if (permissions.read) {
      return t("editor.permissions.youHaveRead");
    }

    return undefined;
  }, [permissions, t]);

  return (
    <Tooltip
      content={tooltipText}
      placement="bottom"
      className="block opacity-50"
    >
      <PermissionIcon permissions={permissions} />
    </Tooltip>
  );
};

export default PermissionInfo;
