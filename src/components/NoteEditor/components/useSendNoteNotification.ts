import { useCallback } from "react";
import { useTranslation } from "react-i18next";
import { useWebId } from "../../../components/SessionProvider";
import { RemoteNotification } from "../../../services";
import useRemoteResourceFactory from "../../SessionProvider/useRemoteResourceFactory";

type SendNoteNotificationFunction = (
  recipient: string,
  noteUri: string,
  type: string
) => Promise<void>;

const NOTIFICATION_TYPE = {
  ADDED_TO_DOCUMENT: "ADDED_TO_DOCUMENT",
  CHANGED_PERMISSIONS: "CHANGED_PERMISSIONS",
};

/** A hook for sending a notification to another user */
const useSendNoteNotification: () => SendNoteNotificationFunction = () => {
  const webId = useWebId();
  const { t } = useTranslation();
  const factory = useRemoteResourceFactory();

  const getNotificationSummary = useCallback(
    (actorName: string, noteName: string, type: string): string => {
      switch (type) {
        case NOTIFICATION_TYPE.ADDED_TO_DOCUMENT:
          return t("editor.notifications.addedToDocument.text", {
            name: actorName,
          });
        case NOTIFICATION_TYPE.CHANGED_PERMISSIONS:
          return t("editor.notifications.changedPermissions.text", {
            name: actorName,
            note: noteName,
          });
        default:
          return t("editor.notifications.default.text", {
            name: actorName,
          });
      }
    },
    [t]
  );

  return useCallback(
    async (recipient: string, noteUri: string, type: string) => {
      if (!webId) {
        return;
      }
      try {
        const inboxUrl = await factory.getProfile(recipient).getInbox();

        if (!inboxUrl) {
          return;
        }

        const note = factory.getNote(noteUri);
        const noteTitle = await note.getTitle();

        const actorProfile = factory.getProfile(webId);
        const actorProfileName = await actorProfile.getName();

        const actorName = actorProfileName.length
          ? actorProfileName
          : t("editor.notifications.unnamedUser");

        await RemoteNotification.createRemoteNotification(
          {
            title: noteTitle,
            actorUri: webId,
            objectUri: noteUri,
            summary: getNotificationSummary(actorName, noteTitle, type),
            published: new Date(),
            containerUri: inboxUrl,
          },
          factory
        );
      } catch (ex) {
        // eslint-disable-next-line no-console
      }
    },
    [webId, getNotificationSummary, t]
  );
};

export { NOTIFICATION_TYPE };

export default useSendNoteNotification;
