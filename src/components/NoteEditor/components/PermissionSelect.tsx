import { Access } from "@inrupt/solid-client";
import React, { useCallback, useMemo } from "react";
import { useTranslation } from "react-i18next";
import Dropdown, { DropdownOption } from "../../Dropdown/Dropdown";

/** Props for the PermissionSelect component */
type PermissionSelectProps = {
  /** The selected access level */
  value: Access;
  /** The callback function for when the access level changes */
  onChange: (access: Access) => void;
};

/** Enum representing permission levels */
const enum PermissionLevel {
  NoAccess = "NoAccess",
  Read = "Read",
  ReadWrite = "ReadWrite",
  ReadWriteControl = "ReadWriteControl",
}

/**
 * A component representing a permission select dropdown
 *
 * @param {PermissionSelectProps} props
 * @category Components
 */
const PermissionSelect: React.FC<PermissionSelectProps> = ({
  value,
  onChange,
}) => {
  const { t } = useTranslation();

  const onPermissionSelect = useCallback(
    (level: PermissionLevel) => {
      const newPermissions: Access = {
        read: level !== PermissionLevel.NoAccess,
        write:
          level === PermissionLevel.ReadWrite ||
          level === PermissionLevel.ReadWriteControl,
        append:
          level === PermissionLevel.ReadWrite ||
          level === PermissionLevel.ReadWriteControl,
        control: level === PermissionLevel.ReadWriteControl,
      };
      onChange(newPermissions);
    },
    [onChange]
  );

  const dropdownOptions = useMemo<DropdownOption[]>(
    () => [
      {
        label: t("editor.permissions.levels.noAccess"),
        value: PermissionLevel.NoAccess,
        onSelect: () => onPermissionSelect(PermissionLevel.NoAccess),
      },
      {
        label: t("editor.permissions.levels.readOnly"),
        value: PermissionLevel.Read,
        onSelect: () => onPermissionSelect(PermissionLevel.Read),
      },
      {
        label: t("editor.permissions.levels.readWrite"),
        value: PermissionLevel.ReadWrite,
        onSelect: () => onPermissionSelect(PermissionLevel.ReadWrite),
      },

      {
        label: t("editor.permissions.levels.control"),
        value: PermissionLevel.ReadWriteControl,
        onSelect: () => onPermissionSelect(PermissionLevel.ReadWriteControl),
      },
    ],
    [onPermissionSelect]
  );

  const dropdownLabel = useMemo<string>(() => {
    if (value.control) {
      return t("editor.permissions.levels.control");
    } else if (value.write) {
      return t("editor.permissions.levels.readWrite");
    } else if (value.read) {
      return t("editor.permissions.levels.readOnly");
    }
    return t("editor.permissions.levels.noAccess");
  }, [t, value]);

  return (
    <Dropdown
      className="w-full"
      options={dropdownOptions}
      defaultText={dropdownLabel}
    />
  );
};

export default PermissionSelect;
