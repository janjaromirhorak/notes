import { Access } from "@inrupt/solid-client";
import { createContext } from "react";

export type PermissionContextItem = {
  webId: string | undefined;
  permissions: Access;
};

export type NotePermissionsContextType = {
  permissions: PermissionContextItem[] | undefined;
  updatePermissions: (permissions: PermissionContextItem[]) => Promise<void>;
  currentUserPermissions: Access;
  updateCurrentUserPermissions: (access: Access) => Promise<void>;
};

export const noAccess: Access = {
  read: false,
  write: false,
  append: false,
  control: false,
};

const NotePermissionsContext = createContext<NotePermissionsContextType>({
  permissions: undefined,
  updatePermissions: async () => {},
  currentUserPermissions: noAccess,
  updateCurrentUserPermissions: async () => {},
});

export default NotePermissionsContext;
