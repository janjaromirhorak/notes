import NotePermissionsContext from "./NotePermissionsContext";

const NotePermissionsContextProvider = NotePermissionsContext.Provider;

export default NotePermissionsContextProvider;
