enum EditorSavingStatus {
  Loaded,
  Changed,
  Saving,
  Saved,
}

export default EditorSavingStatus;
