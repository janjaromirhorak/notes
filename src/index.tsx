import { createRoot } from "react-dom/client";
import App from "./App";
import { ErrorBoundary, GlobalError } from "./components";
import "./i18n";
import "./index.css";

// @ts-expect-error we can safely assume that the root element exists
const root = createRoot(document.getElementById("root"));
root.render(
  <ErrorBoundary component={() => <GlobalError />}>
    <App />
  </ErrorBoundary>
);
