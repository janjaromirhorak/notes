import { useCallback } from "react";

/**
 * Hook that returns a callback to reload the page.
 */
const useReloadPage = (): (() => void) => {
  return useCallback(() => {
    window.location.reload();
  }, []);
};

export default useReloadPage;
