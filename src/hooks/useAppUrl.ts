import { useMemo } from "react";

/**
 * Hook that returns the base URL of the app.
 */
const useAppUrl = (): string => {
  return useMemo(() => {
    return window.location.origin;
  }, []);
};

export default useAppUrl;
