import { Thing, ThingBuilder } from "@inrupt/solid-client";

type Operation = {
  kind:
    | "setLiteralValue"
    | "setReferenceValue"
    | "addLiteralValue"
    | "addReferenceValue";
  predicate: string;
  value: any;
};

export type Plan<T extends Thing> = {
  predicatesToClearFromCache: string[];
  builder: ThingBuilder<T>;
};

/**
 * Class that collects operations for a batch update and then applies them to a query builder
 *
 * @category Solid abstraction
 */
export class BatchUpdate {
  private operations: Operation[] = [];

  /**
   * Sets a remote literal value (string, number...) of a node specified by predicate
   */
  async setRemoteLiteralValue(
    predicate: string,
    literalValue: any
  ): Promise<void> {
    this.operations.push({
      kind: "setLiteralValue",
      predicate,
      value: literalValue,
    });
  }

  /**
   * Sets a remote reference value (link to a node) of a node specified by predicate
   */
  async setRemoteReferenceValue(
    predicate: string,
    referenceUri: string
  ): Promise<void> {
    this.operations.push({
      kind: "setReferenceValue",
      predicate,
      value: referenceUri,
    });
  }

  /**
   * Adds a new node with the specified predicate and a literal value (string, number)
   */
  async addRemoteLiteralValue(
    predicate: string,
    literalValue: any
  ): Promise<void> {
    this.operations.push({
      kind: "addLiteralValue",
      predicate,
      value: literalValue,
    });
  }

  /**
   * Adds a new node with the specified predicate and a reference value (link to a node)
   */
  async addRemoteReferenceValue(
    predicate: string,
    referenceUri: string
  ): Promise<void> {
    this.operations.push({
      kind: "addReferenceValue",
      predicate,
      value: referenceUri,
    });
  }

  /**
   * Applies the batch update to a thing builder.
   */
  applyToBuilder<T extends Thing>(builder: ThingBuilder<T>): ThingBuilder<T> {
    let modifiedBuilder = builder;

    this.operations.forEach(({ kind, predicate, value }) => {
      switch (kind) {
        case "setLiteralValue":
          modifiedBuilder = modifiedBuilder.setStringNoLocale(predicate, value);
          break;
        case "setReferenceValue":
          modifiedBuilder = modifiedBuilder.setUrl(predicate, value);
          break;
        case "addLiteralValue":
          modifiedBuilder = modifiedBuilder.addStringNoLocale(predicate, value);
          break;
        case "addReferenceValue":
          modifiedBuilder = modifiedBuilder.addUrl(predicate, value);
          break;
      }
    });

    return modifiedBuilder;
  }

  /**
   * Returns a list of predicates to be cleared from cache
   */
  getPredicatesToClearFromCache(): string[] {
    return this.operations.map(({ predicate }) => predicate);
  }
}
