import {
  Access,
  Thing,
  ThingBuilder,
  WithServerResourceInfo,
  buildThing,
  createSolidDataset,
  createThing,
  deleteFile,
  deleteSolidDataset,
  getSolidDataset,
  getThing,
  saveSolidDatasetAt,
  saveSolidDatasetInContainer,
  setThing,
  type SolidDataset,
} from "@inrupt/solid-client";
import * as authClient from "@inrupt/solid-client-authn-browser";
import { RemoteNote, RemoteResourceFactory } from "../index";
import { LDP, RDF, SCHEMAORG } from "../namespaces";
import { BatchUpdate } from "./BatchUpdate";

/**
 * Base class representing any remote resource on the Solid Pod
 *
 * @category Solid abstraction
 */
class RemoteResource {
  private uri: string;
  private valueCache: Map<string, any> = new Map();
  private headerCache: Headers | undefined = undefined;
  private datasetWithoutAcl:
    | undefined
    | (SolidDataset & WithServerResourceInfo) = undefined;
  protected factory: RemoteResourceFactory;

  constructor(uri: string, factory: RemoteResourceFactory) {
    this.uri = uri;
    this.factory = factory;
  }

  setDataset(dataset: undefined | (SolidDataset & WithServerResourceInfo)) {
    this.datasetWithoutAcl = dataset;
  }

  async getDataset(): Promise<SolidDataset & WithServerResourceInfo> {
    this.datasetWithoutAcl = await getSolidDataset(this.getUri(), {
      fetch: authClient.fetch.bind(authClient),
    });

    return this.datasetWithoutAcl;
  }

  async isAccessible(): Promise<boolean> {
    try {
      const result = await (authClient.fetch(this.getUri()) as ReturnType<
        typeof fetch
      >);
      return result.ok;
    } catch (e) {
      return false;
    }
  }

  async getResourceThing() {
    const dataset = await this.getDataset();
    const thing = getThing(dataset, this.getUri());
    if (!thing) {
      throw new Error("Could not load dataset.");
    }
    return thing;
  }

  async getPredicates() {
    return (await this.getResourceThing())?.predicates;
  }

  /**
   * Returns the URI of this document
   */
  getUri(): string {
    return this.uri;
  }

  /**
   * Returns the URI of the file containing this document
   */
  getFileUri(): string {
    return this.uri.replace(/#.*/, "");
  }

  /**
   * Returns the URI of the closest directory
   */
  getClosestDirUri(): string {
    return this.uri.replace(/[^/]*$/, "");
  }

  async getHeaders(
    name: string,
    forceReload: boolean = false
  ): Promise<string[]> {
    if (!this.headerCache || forceReload) {
      const response = await authClient.fetch(this.getUri());
      this.headerCache = response.headers;
    }
    return [this.headerCache?.get(name)].filter((value) => !!value) as string[];
  }

  async getPermissionsForKey(key: string): Promise<Access> {
    const header = await this.getHeaders("WAC-Allow");
    if (header) {
      const [string] = header;
      if (string) {
        const parts = string.split(",");
        for (const part of parts) {
          const [agent, permissionString] = part.split("=");
          if (key === agent) {
            const permissions = permissionString
              .replace(/^"(.*)"$/, "$1")
              .split(" ");
            return {
              read: permissions.includes("read"),
              write: permissions.includes("write"),
              append: permissions.includes("append"),
              control: permissions.includes("control"),
            };
          }
        }
      }
    }

    return {
      read: false,
      write: false,
      append: false,
      control: false,
    };
  }

  /**
   * Returns true, iff a request to this.uri returns a 404 response error
   */
  async newFileCanBeCreatedHere(): Promise<boolean> {
    try {
      const result = await authClient.fetch(this.getUri());
      // In Google Chrome, the response status is 401 instead of 404
      return result.status === 404 || result.status === 401;
    } catch (e) {
      // In Google Chrome, the response status is 401 instead of 404
      // @ts-expect-error vague types in oficial typings
      return e.status === 404 || e.status === 401;
    }
  }

  /**
   * Returns true if and only if the remote document has a Content-Type of text/turtle
   */
  async isTurtle(): Promise<boolean> {
    // to check _headers, the fetcher must work with a base file uri
    // if the current uri is not a base file uri, we have to fetch it separately
    const fileUri = this.getFileUri();
    if (fileUri !== this.uri) {
      const remoteDoc = this.factory.getResource(fileUri);
      return await remoteDoc.isTurtle();
    }

    const contentTypes = await this.getHeaders("content-type");

    if (contentTypes) {
      return contentTypes.includes("text/turtle");
    }

    return false;
  }

  /**
   * Returns true if the file of this remote document contains a node,
   * that is marked as a Note.
   */
  async hasNote(): Promise<boolean> {
    try {
      return await this.getNoteEquivalent().isNote();
    } catch (e) {
      return false;
    }
  }

  /**
   * Returns a RemoteResource of a Note found is the file of this RemoteResource.
   * This function expects a Note to exist (or to be subsequently created with createRemoteResource).
   */
  getNoteEquivalent(): RemoteNote {
    return this.factory.getNote(`${this.getFileUri()}#this`);
  }

  /**
   * Returns true if the remote document is marked as a note document,
   * otherwise returns false
   */
  async isNote(): Promise<boolean> {
    if (await this.isTurtle()) {
      const result = await this.getRemoteValues(RDF("type"));
      if (result) {
        return result.includes(SCHEMAORG("TextDigitalDocument"));
      }
    }
    return false;
  }

  /**
   * Returns true if the remote document is marked as LPD('Container')
   */
  async isDirectory(): Promise<boolean> {
    try {
      if (await this.isTurtle()) {
        const result = await this.getRemoteValues(RDF("type"));

        if (result) {
          return result.includes(LDP("Container"));
        }
      }
    } catch (e) {}
    return false;
  }

  async getType(): Promise<string | undefined> {
    return this.getRemoteValue(RDF("type"));
  }

  async setType(type: string): Promise<void> {
    return this.setRemoteValue(RDF("type"), (builder) =>
      builder.setUrl(RDF("type"), type)
    );
  }

  /**
   * Creates a remote file for this document.
   * Caution: If a document with this uri already exists, it will be overwritten!
   */
  async createRemoteResource(
    type: string,
    modifier: <T extends Thing>(builder: ThingBuilder<T>) => ThingBuilder<T> = (
      b
    ) => b,
    {
      saveToContainer,
    }: {
      saveToContainer?: string;
    } = {}
  ): Promise<void> {
    let resourceDataset = createSolidDataset();
    const newThing = modifier(
      buildThing(createThing({ name: "this" })).addUrl(RDF("type"), type)
    ).build();
    resourceDataset = setThing(resourceDataset, newThing);
    if (saveToContainer) {
      await saveSolidDatasetInContainer(saveToContainer, resourceDataset);
    } else {
      await saveSolidDatasetAt(this.getFileUri(), resourceDataset, {
        fetch: authClient.fetch.bind(authClient),
      });
    }
  }

  /**
   * Returns a single matching remote value specified by the predicate
   */
  async getRemoteValue(
    predicate: string,
    forceReload: boolean = false
  ): Promise<string | undefined> {
    return (await this.getRemoteValues(predicate, forceReload))[0];
  }

  async getRemoteDateValue(
    predicate: string,
    forceReload: boolean = false
  ): Promise<Date | undefined> {
    const date = await this.getRemoteValue(predicate, forceReload);
    return date ? new Date(date) : undefined;
  }

  /**
   * Returns an array of matching remote values specified by the predicate
   */
  async getRemoteValues(
    predicate: string,
    forceReload: boolean = false
  ): Promise<string[]> {
    const cachedValue = this.valueCache.get(predicate);

    if (cachedValue === undefined || forceReload) {
      const predicates = await this.getPredicates();
      const foundNamedNodes = [...(predicates?.[predicate]?.namedNodes ?? [])];
      const foundLiterals = predicates?.[predicate]?.literals ?? {};

      const foundValues = [
        ...foundNamedNodes,
        ...Object.values(foundLiterals).reduce<string[]>((values, literal) => {
          return [...values, ...Object.values(literal)];
        }, []),
      ];
      this.valueCache.set(predicate, foundValues);
      return foundValues;
    } else {
      return cachedValue;
    }
  }

  /**
   * Sets a remote value of a node specified by predicate
   */
  async setRemoteValue(
    predicate: string,
    modifier: <T extends Thing>(builder: ThingBuilder<T>) => ThingBuilder<T>
  ): Promise<void> {
    return this.executeUpdate([predicate], modifier);
  }

  async runBatchUpdate(batch: BatchUpdate): Promise<void> {
    return this.executeUpdate(
      batch.getPredicatesToClearFromCache(),
      (builder) => batch.applyToBuilder(builder)
    );
  }

  private async executeUpdate(
    predicates: string[],
    modifier: <T extends Thing>(builder: ThingBuilder<T>) => ThingBuilder<T>
  ): Promise<void> {
    let modifiedDataset = await this.getDataset();
    let modifiedThing = await this.getResourceThing();

    let builder = buildThing(modifiedThing);
    let updatedBuilder = modifier(builder);
    modifiedThing = updatedBuilder.build();

    modifiedDataset = setThing(modifiedDataset, modifiedThing);
    this.setDataset(modifiedDataset);
    await saveSolidDatasetAt(this.getFileUri(), modifiedDataset, {
      fetch: authClient.fetch.bind(authClient),
    });

    // clear relevant items from cache
    predicates.forEach((predicate) => {
      try {
        this.valueCache.delete(predicate);
      } catch (e) {
        // the value is not in the cache
      }
    });
  }

  /**
   * Sets a remote literal value (string, number...) of a node specified by predicate
   */
  async setRemoteLiteralValue(
    predicate: string,
    literalValue: any,
    batch?: BatchUpdate
  ): Promise<void> {
    if (batch) {
      return batch.setRemoteLiteralValue(predicate, literalValue);
    }
    return this.setRemoteValue(predicate, (builder) =>
      builder.setStringNoLocale(predicate, literalValue)
    );
  }

  async setRemoteLiteralDateValue(
    predicate: string,
    literalValue: Date | undefined,
    batch?: BatchUpdate
  ): Promise<void> {
    return this.setRemoteLiteralValue(
      predicate,
      literalValue ? literalValue.toISOString() : undefined,
      batch
    );
  }

  /**
   * Sets a remote reference value (link to a node) of a node specified by predicate
   */
  async setRemoteReferenceValue(
    predicate: string,
    referenceUri: string,
    batch?: BatchUpdate
  ): Promise<void> {
    if (batch) {
      return batch.setRemoteReferenceValue(predicate, referenceUri);
    }
    return this.setRemoteValue(predicate, (builder) =>
      builder.setUrl(predicate, referenceUri)
    );
  }

  /**
   * Adds a new node with the specified predicate and a literal value (string, number)
   */
  async addRemoteLiteralValue(
    predicate: string,
    literalValue: any,
    batch?: BatchUpdate
  ): Promise<void> {
    if (batch) {
      return batch.addRemoteLiteralValue(predicate, literalValue);
    }
    return this.setRemoteValue(predicate, (builder) =>
      builder.addStringNoLocale(predicate, literalValue)
    );
  }

  async addRemoteLiteralDateValue(
    predicate: string,
    literalValue: Date | undefined,
    batch?: BatchUpdate
  ): Promise<void> {
    return this.addRemoteLiteralValue(
      predicate,
      literalValue ? literalValue.toISOString() : undefined,
      batch
    );
  }

  /**
   * Adds a new node with the specified predicate and a reference value (link to a node)
   */
  async addRemoteReferenceValue(
    predicate: string,
    referenceUri: string,
    batch?: BatchUpdate
  ): Promise<void> {
    if (batch) {
      return batch.addRemoteLiteralValue(predicate, referenceUri);
    }
    return this.setRemoteValue(predicate, (builder) =>
      builder.addUrl(predicate, referenceUri)
    );
  }

  /**
   * Removes a remote node matching the predicate and a reference value (link to a node)
   */
  async removeRemoteReferenceValueByValue(
    predicate: string,
    referenceValue: string
  ): Promise<void> {
    return this.setRemoteValue(predicate, (builder) =>
      builder.removeNamedNode(predicate, {
        termType: "NamedNode",
        value: referenceValue,
        equals: (other: any) => other === referenceValue,
      })
    );
  }

  /**
   * Deletes this remote resource from the Solid Pod
   */
  async deleteRemoteResource(): Promise<void> {
    await deleteSolidDataset(this.getFileUri(), {
      fetch: authClient.fetch.bind(authClient),
    });
  }

  /**
   * Deletes the whole file with this resource from the Solid Pod
   */
  async deleteRemoteFile(): Promise<void> {
    try {
      await deleteFile(this.getFileUri(), {
        fetch: authClient.fetch.bind(authClient),
      });
    } catch (e) {
      console.error(`Cannot delete file ${this.getFileUri()}.`, e);
    }
  }
}

export default RemoteResource;
