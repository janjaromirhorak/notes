import RemoteResource from "../RemoteResource";
import { LDP } from "../namespaces";

type Listing = {
  files: RemoteResource[];
  folders: RemoteDirectory[];
};

/**
 * Class representing a remote directory
 *
 * @extends {RemoteResource}
 * @category Solid abstraction
 */
export default class RemoteDirectory extends RemoteResource {
  parentUri?: string = undefined;
  cachedListing?: Listing = undefined;

  /**
   * Set the parent directory URI (used in FileBrowser)
   */
  setParentUri(uri: string | undefined): void {
    this.parentUri = uri;
  }

  /**
   * Get the directory listing represented as an object with the following elements:
   *      files: array of RemoteResource objects (specialized to RemoteNote if possible)
   *      folders: array of RemoteDirectory objects
   */
  async getDirectoryListing(forceReload: boolean = false): Promise<Listing> {
    if (!this.cachedListing || forceReload) {
      const values = await this.getRemoteValues(LDP("contains"), true);

      // convert the uris to generic documents
      const documents = values.map((value) => this.factory.getResource(value));

      let files: RemoteResource[] = [];
      let folders: RemoteDirectory[] = [];

      // asynchronously divide documents into files and folders
      await Promise.all(
        documents.map(async (document: RemoteResource) => {
          try {
            const [isDirectory, hasNote] = await Promise.all([
              document.isDirectory(),
              document.hasNote(),
            ]);

            if (isDirectory) {
              folders.push(this.factory.getDirectory(document.getUri()));
            } else if (hasNote) {
              files.push(document.getNoteEquivalent());
            } else {
              files.push(document);
            }
          } catch (e) {
            files.push(document);
          }
        })
      );

      // sort files and folders by name
      const sortFunction = (a: RemoteResource, b: RemoteResource) => {
        if (a.getUri() < b.getUri()) {
          return -1;
        } else if (a.getUri() > b.getUri()) {
          return 1;
        }
        return 0;
      };
      files.sort(sortFunction);
      folders.sort(sortFunction);

      this.cachedListing = {
        files,
        folders,
      };
    }

    return this.cachedListing;
  }

  /**
   * Clear cache for the directory listing - forces reload of the directory on the next listing request
   */
  clearListingCache(): void {
    this.cachedListing = undefined;
  }

  getParentUri(): string | undefined {
    return this.parentUri;
  }

  /**
   * Tries to guess the URI of the parent directory by removing one level of directory depth from the current URI.
   * If there is enough levels in the URI and on the guessed URI is a directory, returns the URI, otherwise returns undefined.
   */
  async guessParentUri(): Promise<string | undefined> {
    const fileUri = this.getFileUri();
    const currentUri = new URL(fileUri);
    const path = currentUri.pathname;
    const base = fileUri.substring(0, fileUri.length - path.length);

    let pathParts = path.split("/");
    pathParts.shift();
    pathParts.pop();

    if (pathParts.length === 0) {
      return undefined;
    }

    pathParts.pop();
    const guessedPath = pathParts.length ? pathParts.join("/") + "/" : "";
    const guessedParentUri = `${base}/${guessedPath}`;

    const guessedParent = this.factory.getResource(guessedParentUri);

    const isDirectory = await guessedParent.isDirectory();
    if (isDirectory) {
      return guessedParentUri;
    }

    return undefined;
  }
}
