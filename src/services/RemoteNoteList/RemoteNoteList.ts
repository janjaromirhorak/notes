import { RemoteNote } from "../../services";
import RemoteResource from "../RemoteResource";
import { SCHEMAORG } from "../namespaces";

/**
 * Class to represent the list of user-saved notes
 *
 * @extends {RemoteResource}
 * @category Solid abstraction
 */
class RemoteNoteList extends RemoteResource {
  /**
   * Returns an array of RemoteNote objects
   */
  async getNotes(
    forceReload: boolean = false,
    isRetry: boolean = false
  ): Promise<RemoteNote[]> {
    try {
      const remotes = await this.getRemoteValues(
        SCHEMAORG("ListItem"),
        forceReload
      );

      return remotes.map((uri: string) => this.factory.getNote(uri));
    } catch (e) {
      // @ts-expect-error incomplete typing
      if (e.status === 404 && !isRetry) {
        this.createRemoteResource(SCHEMAORG("ItemList"));
        return this.getNotes(true);
      } else {
        throw e;
      }
    }
  }

  /**
   * Add a note to the list of notes
   */
  async addNoteToList(uri: string): Promise<void> {
    return this.addRemoteReferenceValue(SCHEMAORG("ListItem"), uri);
  }

  /**
   * Remove a note from the list of notes
   */
  async removeNoteFromList(uri: string): Promise<void> {
    return this.removeRemoteReferenceValueByValue(SCHEMAORG("ListItem"), uri);
  }
}

export default RemoteNoteList;
