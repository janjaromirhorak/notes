import {
  RemoteDirectory,
  RemoteInbox,
  RemoteNote,
  RemoteNoteList,
  RemoteNotification,
  RemoteProfile,
  RemoteResource,
} from ".";

/**
 * A factory class for managing remote resources. Maintains a cache of loaded objects to avoid
 * unnecessary requests.
 */
export default class RemoteResourceFactory {
  private cache = new Map<string, RemoteResource>();

  /**
   * Returns an unique identifier of a resource based on its type and URI.
   * Used to represent the objects in the cache.
   */
  getIdentifier(uri: string, className: string): string {
    return JSON.stringify([uri, className]);
  }

  /**
   * Clears the factory cache
   */
  clearCache(): void {
    this.cache.clear();
  }

  /**
   * Deletes one specific record from the factory cache
   *
   * @param identifier
   */
  deleteFromCache(identifier: string): void {
    this.cache.delete(identifier);
  }

  /**
   * Returns a (possibly cached) instance of a resource based on its URI and a prototype
   */
  protected get<T extends RemoteResource>(
    uri: string,
    generator: (uri: string, factory: RemoteResourceFactory) => T,
    className: string,
    forceReload: boolean = false
  ): T {
    const identifier = this.getIdentifier(uri, className);
    const cached = forceReload ? undefined : (this.cache.get(identifier) as T);
    if (cached) {
      return cached;
    } else {
      const newInstance = generator(uri, this);
      this.cache.set(identifier, newInstance);
      return newInstance;
    }
  }

  /**
   * Returns a (possibly cached) instance of a RemoteResource based on its URI
   */
  getResource(uri: string, forceReload: boolean = false): RemoteResource {
    return this.get(
      uri,
      (uri, factory) => new RemoteResource(uri, factory),
      "RemoteResource",
      forceReload
    );
  }

  /**
   * Returns a (possibly cached) instance of a RemoteDirectory based on its URI
   */
  getDirectory(uri: string, forceReload: boolean = false): RemoteDirectory {
    return this.get(
      uri,
      (uri, factory) => new RemoteDirectory(uri, factory),
      "RemoteDirectory",
      forceReload
    );
  }

  /**
   * Returns a (possibly cached) instance of a RemoteNoteList based on its URI
   */
  getNoteList(uri: string, forceReload: boolean = false): RemoteNoteList {
    return this.get(
      uri,
      (uri, factory) => new RemoteNoteList(uri, factory),
      "RemoteNoteList",
      forceReload
    );
  }

  /**
   * Returns a (possibly cached) instance of a RemoteNote based on its URI
   */
  getNote(uri: string, forceReload: boolean = false): RemoteNote {
    return this.get(
      uri,
      (uri, factory) => new RemoteNote(uri, factory),
      "RemoteNote",
      forceReload
    );
  }

  /**
   * Returns a (possibly cached) notification
   */
  getNotification(
    uri: string,
    forceReload: boolean = false
  ): RemoteNotification {
    return this.get(
      uri,
      (uri, factory) => new RemoteNotification(uri, factory),
      "RemoteNotification",
      forceReload
    );
  }

  /**
   * Returns a (possibly cached) inbox
   */
  getInbox(uri: string, forceReload: boolean = false): RemoteInbox {
    return this.get(
      uri,
      (uri, factory) => new RemoteInbox(uri, factory),
      "RemoteInbox",
      forceReload
    );
  }

  /**
   * Returns a (possibly cached) instance of a RemoteProfile based on its URI (webId)
   */
  getProfile(uri: string, forceReload: boolean = false): RemoteProfile {
    return this.get(
      uri,
      (uri, factory) => new RemoteProfile(uri, factory),
      "RemoteProfile",
      forceReload
    );
  }
}
