/**
 * Returns the URI of user's Solid Pod, based on their WebID
 */
const getPodUriFromWebId = (webId?: string): string | undefined => {
  if (webId) {
    const regex = /^[^/]+\/\/[^/]+\//;

    const m = regex.exec(webId);
    if (m && m[0]) {
      return `${m[0]}`;
    }
  }
  return undefined;
};

export default getPodUriFromWebId;
