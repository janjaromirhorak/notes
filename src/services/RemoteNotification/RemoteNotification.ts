import RemoteResource from "../RemoteResource";
import { BatchUpdate } from "../RemoteResource/BatchUpdate";
import RemoteResourceFactory from "../RemoteResourceFactory";
import { ACTIVITYSTREAM, SCHEMAORG, SOLID, TERMS } from "../namespaces";

/**
 * Represents the data required for creating a notification
 */
type NotificationCreationData = {
  /** The title of the notification */
  title: string;
  /** The publication date of the notification */
  published: Date;
  /** The URI of the actor associated with the notification */
  actorUri: string;
  /** The URI of the object associated with the notification */
  objectUri: string;
  /** The summary of the notification */
  summary: string;
  /** The URI of the container associated with the notification */
  containerUri: string;
};

/**
 * Class to represent a remote notification
 *
 * @extends {RemoteResource}
 * @category Solid abstraction
 */
class RemoteNotification extends RemoteResource {
  /**
   * Creates a remote file for this document and initialises a note by adding mandatory nodes.
   * Caution: If a document with this uri already exists, it will be overwritten!
   */
  static async createRemoteNotification(
    notificationData: NotificationCreationData,
    factory: RemoteResourceFactory
  ): Promise<void> {
    new RemoteNotification("", factory).fillNotificationData(notificationData);
  }

  /**
   * Fills up an empty notification instance with data and submits it to the container (inbox)
   */
  private async fillNotificationData({
    title,
    published,
    actorUri,
    objectUri,
    summary,
    containerUri,
  }: NotificationCreationData): Promise<void> {
    const batch = new BatchUpdate();
    await this.addTitle(title, batch);
    await this.addLicenseUri(
      "https://creativecommons.org/licenses/by-sa/4.0/",
      batch
    );
    await this.addSummary(summary, batch);
    await this.addActorUri(actorUri, batch);
    await this.addObjectUri(objectUri, batch);
    await this.addPublished(published, batch);

    await this.createRemoteResource(
      ACTIVITYSTREAM("Document"),
      (builder) => batch.applyToBuilder(builder),
      { saveToContainer: containerUri }
    );
  }

  /**
   * Returns title of this notification
   */
  async getTitle(forceReload: boolean = false): Promise<string | undefined> {
    return await this.getRemoteValue(TERMS("title"), forceReload);
  }

  /**
   * Adds the title of this notification
   */
  async addTitle(title: string, batch?: BatchUpdate): Promise<void> {
    return this.addRemoteLiteralValue(TERMS("title"), title, batch);
  }

  /**
   * Returns true if the notification has been already read
   */
  async getReadStatus(forceReload: boolean = false): Promise<boolean> {
    const status = await this.getRemoteValue(SOLID("read"), forceReload);
    return status === "true";
  }

  /**
   * Sets the read status of the notification
   */
  async setReadStatus(newReadStatus: string): Promise<void> {
    await this.setRemoteLiteralValue(SOLID("read"), newReadStatus);
  }

  /**
   * Marks the notification as read
   */
  async markAsRead(): Promise<void> {
    await this.setReadStatus("true");
  }

  /**
   * Returns the publication date of the notification
   */
  async getPublished(forceReload: boolean = false): Promise<Date | undefined> {
    const date = await this.getRemoteValue(
      ACTIVITYSTREAM("published"),
      forceReload
    );
    if (date) {
      return new Date(date);
    }
    return undefined;
  }

  /**
   * Adds the notification published date
   */
  async addPublished(published: Date, batch?: BatchUpdate): Promise<void> {
    await this.addRemoteLiteralValue(
      ACTIVITYSTREAM("published"),
      published.toISOString(), // TODO mark as: "2021-04-25T16:45:29.511Z"^^xsd:dateTime.
      batch
    );
  }

  /**
   * Returns URI of the notification's actor (usually the author)
   */
  async getActorUri(forceReload: boolean = false): Promise<string | undefined> {
    return await this.getRemoteValue(ACTIVITYSTREAM("actor"), forceReload);
  }

  /**
   * Adds the actor of this notification
   */
  async addActorUri(actorUri: string, batch?: BatchUpdate): Promise<void> {
    return this.addRemoteReferenceValue(
      ACTIVITYSTREAM("actor"),
      actorUri,
      batch
    );
  }

  /**
   * Returns URI of the notification's object (usually the note URI)
   */
  async getObjectUri(
    forceReload: boolean = false
  ): Promise<string | undefined> {
    const uri = await this.getRemoteValue(
      ACTIVITYSTREAM("object"),
      forceReload
    );
    if (!uri) {
      return undefined;
    }
    const url = new URL(uri);
    if (!url.hash) {
      url.hash = "this";
    }
    return url.toString();
  }

  /**
   * Adds the actor of this notification
   */
  async addObjectUri(objectUri: string, batch?: BatchUpdate): Promise<void> {
    return this.addRemoteReferenceValue(
      ACTIVITYSTREAM("object"),
      objectUri,
      batch
    );
  }

  /**
   * Returns the summary of this notification
   */
  async getSummary(forceReload: boolean = false): Promise<string | undefined> {
    return await this.getRemoteValue(ACTIVITYSTREAM("summary"), forceReload);
  }

  /**
   * Adds the summary of this notification
   */
  async addSummary(summary: string, batch?: BatchUpdate): Promise<void> {
    return this.addRemoteLiteralValue(
      ACTIVITYSTREAM("summary"),
      summary,
      batch
    );
  }

  /**
   * Adds the license of this notification
   */
  async addLicenseUri(licenseUri: string, batch?: BatchUpdate): Promise<void> {
    return this.addRemoteReferenceValue(
      SCHEMAORG("license"),
      licenseUri,
      batch
    );
  }
}

export default RemoteNotification;
