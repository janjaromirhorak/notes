import {
  Access,
  WithAcl,
  WithServerResourceInfo,
  getAgentAccess,
  getPublicAccess,
  getSolidDatasetWithAcl,
  type SolidDataset,
} from "@inrupt/solid-client";
import * as authClient from "@inrupt/solid-client-authn-browser";
import { RemoteResource } from "..";
import { noAccess } from "../../components/NoteEditor/NotePermissionsContext";

/**
 * Remote resource on a Solid Pod, bundled with its ACL.
 *
 * Used for resources, where ACL is needed for further interaciton (like a remote note)
 *
 * @extends {RemoteResource}
 * @category Solid abstraction
 */
class RemoteResourceWithAcl extends RemoteResource {
  private datasetWithAcl:
    | undefined
    | (SolidDataset & WithAcl & WithServerResourceInfo) = undefined;

  async getDataset(): Promise<SolidDataset & WithServerResourceInfo & WithAcl> {
    this.datasetWithAcl = await getSolidDatasetWithAcl(this.getUri(), {
      fetch: authClient.fetch.bind(authClient),
    });

    return this.datasetWithAcl;
  }

  setDataset(
    dataset: undefined | (SolidDataset & WithAcl & WithServerResourceInfo)
  ) {
    this.datasetWithAcl = dataset;
  }

  async getUserPermissions(webId: string): Promise<Access> {
    return getAgentAccess(await this.getDataset(), webId) ?? noAccess;
  }

  async getPublicPermissions(): Promise<Access> {
    return getPublicAccess(await this.getDataset()) ?? noAccess;
  }
}

export default RemoteResourceWithAcl;
