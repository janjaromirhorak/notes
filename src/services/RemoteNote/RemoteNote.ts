import { BatchUpdate } from "../RemoteResource/BatchUpdate";
import RemoteResourceWithAcl from "../RemoteResourceWithAcl";
import { DCMI, SCHEMAORG } from "../namespaces";

/**
 * Class representing a remote note
 *
 * @extends {RemoteResourceWithAcl}
 * @category Solid abstraction
 */
export default class RemoteNote extends RemoteResourceWithAcl {
  /**
   * Creates a remote file for this document and initialises a note by adding mandatory nodes.
   * Caution: If a document with this uri already exists, it will be overwritten!
   */
  async createRemoteNote(title: string): Promise<void> {
    await this.createRemoteResource(SCHEMAORG("TextDigitalDocument"));

    const batch = new BatchUpdate();
    await this.addTitle(title, batch);
    await this.addContent("", batch);
    await this.addModified(new Date(), batch);
    await this.runBatchUpdate(batch);
  }

  /**
   * Returns the title of this note
   */
  async getTitle(forceReload: boolean = false): Promise<string> {
    return (await this.getRemoteValue(DCMI("title"), forceReload)) ?? "";
  }

  /**
   * Sets the title of this note
   */
  async setTitle(title: string, batch?: BatchUpdate): Promise<void> {
    return this.setRemoteLiteralValue(DCMI("title"), title, batch);
  }

  /**
   * Adds the title of this note
   */
  protected async addTitle(title: string, batch?: BatchUpdate): Promise<void> {
    return this.addRemoteLiteralValue(DCMI("title"), title, batch);
  }

  /**
   * Returns the content of this note
   */
  async getContent(forceReload: boolean = false): Promise<string> {
    return (await this.getRemoteValue(SCHEMAORG("text"), forceReload)) ?? "";
  }

  /**
   * Sets the content of this note
   */
  async setContent(content: string, batch?: BatchUpdate): Promise<void> {
    return this.setRemoteLiteralValue(SCHEMAORG("text"), content, batch);
  }

  /**
   * Adds the content of this note
   */
  protected async addContent(
    content: string,
    batch?: BatchUpdate
  ): Promise<void> {
    return this.addRemoteLiteralValue(SCHEMAORG("text"), content, batch);
  }

  protected async addModified(
    modified: Date | undefined,
    batch?: BatchUpdate
  ): Promise<void> {
    return this.addRemoteLiteralDateValue(
      SCHEMAORG("dateModified"),
      modified,
      batch
    );
  }

  /**
   * Sets the modification date of the note
   */
  async setModified(
    modified: Date | undefined,
    batch?: BatchUpdate
  ): Promise<void> {
    return this.setRemoteLiteralDateValue(
      SCHEMAORG("dateModified"),
      modified,
      batch
    );
  }

  /**
   * Returns the modification date of the note
   */
  async getModified(forceReload: boolean = false): Promise<Date | undefined> {
    return this.getRemoteDateValue(SCHEMAORG("dateModified"), forceReload);
  }
}
