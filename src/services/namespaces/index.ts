const createNamespace =
  (base: string) =>
  (type: string): string =>
    `${base}${type}`;

export const SIOC = createNamespace("http://rdfs.org/sioc/ns#");
export const DCMI = createNamespace("http://purl.org/dc/elements/1.1/");
export const SCHEMAORG = createNamespace("http://schema.org/");
export const LDP = createNamespace("http://www.w3.org/ns/ldp#");
export const RDF = createNamespace(
  "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
);
export const TERMS = createNamespace("http://purl.org/dc/terms#");
export const SOLID = createNamespace("https://www.w3.org/ns/solid/terms#");
export const ACTIVITYSTREAM = createNamespace(
  "https://www.w3.org/ns/activitystreams#"
);
export const FOAF = createNamespace("http://xmlns.com/foaf/0.1/");
export const VCARD = createNamespace("http://www.w3.org/2006/vcard/ns#");
