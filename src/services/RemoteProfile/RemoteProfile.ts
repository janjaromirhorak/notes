import RemoteResource from "../RemoteResource";
import { FOAF, LDP, VCARD } from "../namespaces";

/**
 * Class representing a remote user profile
 *
 * @extends {RemoteResource}
 * @category Solid abstraction
 */
export default class RemoteProfile extends RemoteResource {
  /**
   * Returns name from the profile
   */
  async getName(forceReload: boolean = false): Promise<string> {
    return (await this.getRemoteValue(FOAF("name"), forceReload)) ?? "";
  }

  /**
   * Returns URL of the profile's picture, or undefined if it does not exist
   */
  async getPicture(forceReload: boolean = false): Promise<string | undefined> {
    return await this.getRemoteValue(VCARD("hasPhoto"), forceReload);
  }

  /**
   * Returns list of urls from the FOAF "knows" relation
   */
  async getKnows(forceReload: boolean = false): Promise<string[]> {
    return await this.getRemoteValues(FOAF("knows"), forceReload);
  }

  /**
   * Returns the URL of an users inbox (if it exists)
   */
  async getInbox(forceReload: boolean = false): Promise<string | undefined> {
    return await this.getRemoteValue(LDP("inbox"), forceReload);
  }
}
