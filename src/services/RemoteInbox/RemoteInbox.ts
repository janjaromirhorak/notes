import RemoteNotification from "../RemoteNotification";
import RemoteResource from "../RemoteResource";
import { LDP } from "../namespaces";

/**
 * Class to represent a remote notification
 *
 * @extends {RemoteResource}
 * @category Solid abstraction
 */
class RemoteInbox extends RemoteResource {
  async getNotifications(
    forceReload: boolean = false
  ): Promise<RemoteNotification[]> {
    const items = await this.getRemoteValues(LDP("contains"), forceReload);
    return items.map((notificationUrl) => {
      const url = new URL(notificationUrl);
      if (!url.hash) {
        url.hash = "this";
      }
      return this.factory.getNotification(url.toString());
    });
  }
}

export default RemoteInbox;
