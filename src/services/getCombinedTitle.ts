/**
 * Creates a string to be used in the page title. If the title argument is specified,
 * it is used as part of the title, otherwise the function returns just the app name.
 *
 * @param title
 * @returns {string}
 */
const getCombinedTitle = (title?: string): string => {
  let parts = [];
  if (title) {
    parts.push(title);
  }

  if (import.meta.env.VITE_TITLE_BASE) {
    parts.push(import.meta.env.VITE_TITLE_BASE);
  }

  if (import.meta.env.VITE_VERSION) {
    parts.push(import.meta.env.VITE_VERSION);
  }

  return parts.join(" | ");
};

export default getCombinedTitle;
