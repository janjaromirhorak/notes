import { getPodUriFromWebId } from "./index";

/**
 * Returns the URI of a note list, based on the user's WebID
 *
 * @param webId
 * @returns {string|undefined}
 */
const getNoteListUriFromWebId = (webId?: string): string | undefined => {
  const podUri = getPodUriFromWebId(webId);
  if (podUri) {
    return `${podUri}private/notes.ttl#this`;
  }
  return undefined;
};

export default getNoteListUriFromWebId;
