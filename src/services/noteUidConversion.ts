/**
 * Converts an URI of a note to the internal representation used in the routes of the app
 *
 * @param uri
 * @returns {string}
 */
export const uriToUid = (uri: string): string => {
  return btoa(uri);
};

/**
 * Converts an internal representation used in the routes, to the real URI of the note
 *
 * @param uid
 * @returns {string}
 */
export const uidToUri = (uid: string): string => {
  let uri = atob(uid);
  const url = new URL(uri);
  if (!url.hash) {
    uri = `${uri}#this`;
  }
  return uri;
};
