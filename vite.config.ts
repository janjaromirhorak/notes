import react from "@vitejs/plugin-react";
import { defineConfig, splitVendorChunkPlugin } from "vite";
import svgrPlugin from "vite-plugin-svgr";
import viteTsconfigPaths from "vite-tsconfig-paths";

// https://vitejs.dev/config/
export default defineConfig(() => {
  const config = {
    plugins: [
      react(),
      viteTsconfigPaths(),
      svgrPlugin(),
      splitVendorChunkPlugin(),
    ],
    build: {
      outDir: "dist",
    },
    clearScreen: false,
  };

  return config;
});
