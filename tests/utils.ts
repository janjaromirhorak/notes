import { Page, expect } from "@playwright/test";

export const logIn = async (
  page: Page,
  {
    appUrl,
    webId,
    login,
    password,
  }: {
    appUrl: string;
    webId: string;
    login: string;
    password: string;
  }
) => {
  await page.goto(`${appUrl}/login`);
  await page.locator("input[type=text]").type(webId);
  await page.locator("button", { hasText: /log in/ }).click();
  await expect(page).toHaveURL(/https:\/\/inrupt.net\/login/, {
    timeout: 60 * 1000,
  });
  await page.getByPlaceholder("Username").type(login);
  await page.getByPlaceholder("Password").type(password);
  await page
    .locator("button", { hasText: "Log In", hasNotText: "Certificate" })
    .click();
  // wait until the login process is finished
  await expect(page).toHaveURL(/code/, { timeout: 60 * 1000 });
  await expect(page).not.toHaveURL(/code/, { timeout: 60 * 1000 });

  await expect(
    page.locator("button:visible", { hasText: /Log out/ }).first()
  ).toBeVisible({
    timeout: 60 * 1000,
  });
};
