import { expect, test } from "@playwright/test";
import { logIn } from "./utils";

const appUrl = process?.env?.PLAYWRIGHT_APP_URL as string;

test.describe("unauthenticated user", async () => {
  test.describe("sidebar", async () => {
    test("has a login button", async ({ page }) => {
      await page.goto(appUrl);
      await page
        .getByText(/Log in/)
        .first()
        .click();
      await expect(page).toHaveURL(`${appUrl}/login`);
    });

    test("has a file browser button", async ({ page }) => {
      await page.goto(appUrl);
      await page.getByText(/Find a note/).click();
      await expect(page).toHaveURL(`${appUrl}/open`);
    });
  });

  test.describe("homepage", async () => {
    test("renders correctly", async ({ page }) => {
      await page.goto(appUrl);
      await Promise.all([
        // page has a working title
        expect(page).toHaveTitle(/Notes/),
        // homepage has a single H1 containing the app name
        expect(page.locator("h1")).toHaveText(/Notes/),
      ]);
    });

    test("solid link works", async ({ page }) => {
      await page.goto(appUrl);
      await page.getByText("Learn about Solid").click();
      await expect(page).toHaveURL("https://solidproject.org");
    });

    test("repository link works", async ({ page }) => {
      await page.goto(appUrl);
      await page.getByText("View source code").click();
      await expect(page).toHaveURL("https://gitlab.com/janjaromirhorak/notes");
    });
  });

  test.describe("login page", async () => {
    test("renders correctly", async ({ page }) => {
      await page.goto(`${appUrl}/login`);

      await Promise.all([
        expect(page.locator("h1")).toHaveText(/Log in with your Web ID/),
        expect(page.locator("input[type=text]")).toBeVisible(),
        expect(page.getByText(/inrupt\.net/)).toHaveCount(2),
        expect(page.getByText(/solidcommunity\.net/)).toHaveCount(2),
        expect(page.getByText(/register with inrupt\.net/)).toBeVisible(),
        expect(
          page.getByText(/register with solidcommunity\.net/)
        ).toBeVisible(),
      ]);
    });
  });

  test.describe("file browser", async () => {
    test("renders correctly", async ({ page }) => {
      await page.goto(`${appUrl}/open`);

      await Promise.all([
        expect(page.locator("h1")).toHaveText(/You are not logged in/),
        expect(page.locator("input[type=text]")).toBeVisible(),
      ]);
    });

    test("loads public directory", async ({ page }) => {
      const publicDir = process.env.PLAYWRIGHT_PUBLIC_DIRECTORY as string;

      await page.goto(`${appUrl}/open`);
      await page.locator("input[type=text]").type(publicDir);
      await page.locator("button", { hasText: "Browse" }).click();
      await expect(
        page.locator("main").getByText(/e2e-testing-directory/)
      ).toBeVisible({
        timeout: 60 * 1000,
      });
    });

    test("navigates directories up and down", async ({ page }) => {
      const publicDir = process.env.PLAYWRIGHT_PUBLIC_DIRECTORY as string;

      await page.goto(`${appUrl}/open`);
      await page.locator("input[type=text]").type(publicDir);
      await page.locator("button", { hasText: "Browse" }).click();
      await page
        .locator("main")
        .getByText(/e2e-testing-directory/)
        .click({
          timeout: 60 * 1000,
        });
      await expect(
        page.locator("main").getByText(/public-testing-note\.ttl/)
      ).toBeVisible({
        timeout: 60 * 1000,
      });
      await page
        .locator("main")
        .getByText(/\.\./)
        .click({
          timeout: 60 * 1000,
        });
      await expect(
        page.locator("main").getByText(/e2e-testing-directory/)
      ).toBeVisible({
        timeout: 60 * 1000,
      });
    });

    test("opens a note", async ({ page }) => {
      const publicDir = process.env.PLAYWRIGHT_PUBLIC_DIRECTORY as string;

      await page.goto(`${appUrl}/open`);
      await page
        .locator("input[type=text]")
        .type(`${publicDir}e2e-testing-directory/`);
      await page.locator("button", { hasText: "Browse" }).click();
      await page
        .locator("main")
        .getByText(/public-testing-note\.ttl/)
        .click({
          timeout: 60 * 1000,
        });
      await expect(page).toHaveTitle(/Testing note/);
    });
  });

  test("can log in", async ({ page }) => {
    await logIn(page, {
      appUrl,
      webId: process.env.PLAYWRIGHT_TESTING_USER as string,
      login: process.env.PLAYWRIGHT_TESTING_USER_USERNAME as string,
      password: process.env.PLAYWRIGHT_TESTING_USER_PASSWORD as string,
    });
  });
});
