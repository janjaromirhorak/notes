import { expect, test } from "@playwright/test";
import { logIn } from "./utils";

const appUrl = process?.env?.PLAYWRIGHT_APP_URL as string;

test.beforeEach(async ({ page }) => {
  await logIn(page, {
    appUrl,
    webId: process.env.PLAYWRIGHT_TESTING_USER as string,
    login: process.env.PLAYWRIGHT_TESTING_USER_USERNAME as string,
    password: process.env.PLAYWRIGHT_TESTING_USER_PASSWORD as string,
  });
});

test.describe("authenticated user", async () => {
  test("can log out", async ({ page }) => {
    await page
      .getByText(/Log out/)
      .first()
      .click();
    await expect(page.getByText(/Log in/).first()).toBeVisible();
  });

  test.describe("sidebar", async () => {
    test("has the testing note in the note list", async ({ page }) => {
      await expect(page.getByText(/Testing note/)).toBeVisible();
    });

    test("has a file browser button", async ({ page }) => {
      await page.getByText(/Find or create a note/).click();
      await expect(page).toHaveURL(`${appUrl}/open`);
    });
  });

  test.describe("homepage", async () => {
    test("renders correctly", async ({ page }) => {
      await page.goto(appUrl);
      await Promise.all([
        // page has a working title
        expect(page).toHaveTitle(/Notes/),
        // homepage has a single H1 containing the app name
        expect(page.locator("h1")).toHaveText(/Notes/),
      ]);
    });

    test("solid link works", async ({ page }) => {
      await page.goto(appUrl);
      await page.getByText("Learn about Solid").click();
      await expect(page).toHaveURL("https://solidproject.org");
    });

    test("repository link works", async ({ page }) => {
      await page.goto(appUrl);
      await page.getByText("View source code").click();
      await expect(page).toHaveURL("https://gitlab.com/janjaromirhorak/notes");
    });
  });

  test.describe("file browser", async () => {
    test("renders correctly", async ({ page }) => {
      await page.getByText(/Find or create a note/).click();
      await Promise.all([
        expect(page.locator("h1")).toHaveText(/You are not logged in/),
        expect(page.locator("input[type=text]")).toBeVisible(),
      ]);
    });

    test("loads main directory", async ({ page }) => {
      await page.getByText(/Find or create a note/).click();
      // smoke test for three standard directories in the root folder
      await expect(page.locator("main").getByText("public/")).toBeVisible({
        timeout: 60 * 1000,
      });
      await expect(page.locator("main").getByText("private/")).toBeVisible({
        timeout: 60 * 1000,
      });
      await expect(page.locator("main").getByText("profile/")).toBeVisible({
        timeout: 60 * 1000,
      });
    });

    test("navigates directories up and down", async ({ page }) => {
      const publicDir = process.env.PLAYWRIGHT_PUBLIC_DIRECTORY as string;

      await page.getByText(/Find or create a note/).click();
      await expect(page.locator("main").getByText("public/")).toBeVisible({
        timeout: 60 * 1000,
      });
      await page.locator("input[type=text]").clear();
      await page.locator("input[type=text]").type(publicDir);
      await page.locator("[aria-label=Submit]").click();
      await page
        .locator("main")
        .getByText(/e2e-testing-directory/)
        .click({
          timeout: 60 * 1000,
        });
      await expect(
        page.locator("main").getByText(/public-testing-note\.ttl/)
      ).toBeVisible({
        timeout: 60 * 1000,
      });
      await page
        .locator("main")
        .getByText(/\.\./)
        .click({
          timeout: 60 * 1000,
        });
      await expect(
        page.locator("main").getByText(/e2e-testing-directory/)
      ).toBeVisible({
        timeout: 60 * 1000,
      });
    });

    test("opens a note", async ({ page }) => {
      const publicDir = process.env.PLAYWRIGHT_PUBLIC_DIRECTORY as string;
      await page.getByText(/Find or create a note/).click();

      await expect(page.locator("main").getByText("public/")).toBeVisible({
        timeout: 60 * 1000,
      });
      await page.locator("input[type=text]").clear();
      await page
        .locator("input[type=text]")
        .type(`${publicDir}e2e-testing-directory/`);
      await page.locator("[aria-label=Submit]").click();
      await page
        .locator("main")
        .getByText(/public-testing-note\.ttl/)
        .click({
          timeout: 60 * 1000,
        });
      await expect(page).toHaveTitle(/Testing note/);
    });
  });

  test.describe("note editor", async () => {
    test("can be opened from the sidebar", async ({ page }) => {
      await page.getByText(/Testing note/).click();
      await expect(page).toHaveTitle(/Testing note/);
    });

    test("is rendered correctly according to permissions", async ({ page }) => {
      await page.getByText(/Testing note/).click();
      await expect(page.getByText("source")).toBeVisible({
        timeout: 60 * 1000,
      });
      await expect(page.getByText("delete")).toBeVisible({
        timeout: 60 * 1000,
      });
      await expect(page.getByText("share")).toBeVisible({ timeout: 60 * 1000 });
    });
  });
});
